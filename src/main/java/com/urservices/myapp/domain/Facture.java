package com.urservices.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import com.urservices.myapp.domain.enumeration.Etat;

/**
 * A Facture.
 */
@Entity
@Table(name = "facture")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Facture extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "numero_facture")
    private String numeroFacture;

    @NotNull
    @Column(name = "date_facturation", nullable = false)
    private LocalDate dateFacturation;

    @NotNull
    @Min(value = 0L)
    @Column(name = "montant_facure", nullable = false)
    private Long montantFacure;

    @Min(value = 0L)
    @Column(name = "net_a_payer")
    private Long netAPayer;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private Etat etat;

    @ManyToOne
    @JsonIgnoreProperties("factures")
    private Operation operation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroFacture() {
        return numeroFacture;
    }

    public Facture numeroFacture(String numeroFacture) {
        this.numeroFacture = numeroFacture;
        return this;
    }

    public void setNumeroFacture(String numeroFacture) {
        this.numeroFacture = numeroFacture;
    }

    public LocalDate getDateFacturation() {
        return dateFacturation;
    }

    public Facture dateFacturation(LocalDate dateFacturation) {
        this.dateFacturation = dateFacturation;
        return this;
    }

    public void setDateFacturation(LocalDate dateFacturation) {
        this.dateFacturation = dateFacturation;
    }

    public Long getMontantFacure() {
        return montantFacure;
    }

    public Facture montantFacure(Long montantFacure) {
        this.montantFacure = montantFacure;
        return this;
    }

    public void setMontantFacure(Long montantFacure) {
        this.montantFacure = montantFacure;
    }

    public Long getNetAPayer() {
        return netAPayer;
    }

    public Facture netAPayer(Long netAPayer) {
        this.netAPayer = netAPayer;
        return this;
    }

    public void setNetAPayer(Long netAPayer) {
        this.netAPayer = netAPayer;
    }

    public Etat getEtat() {
        return etat;
    }

    public Facture etat(Etat etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Operation getOperation() {
        return operation;
    }

    public Facture operation(Operation operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Facture)) {
            return false;
        }
        return id != null && id.equals(((Facture) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Facture{" +
            "id=" + getId() +
            ", numeroFacture='" + getNumeroFacture() + "'" +
            ", dateFacturation='" + getDateFacturation() + "'" +
            ", montantFacure=" + getMontantFacure() +
            ", netAPayer=" + getNetAPayer() +
            ", etat='" + getEtat() + "'" +
            "}";
    }
}
