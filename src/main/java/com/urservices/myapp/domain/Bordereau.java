package com.urservices.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import com.urservices.myapp.domain.enumeration.TypeBordereau;

/**
 * A Bordereau.
 */
@Entity
@Table(name = "bordereau")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Bordereau extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "numero_bordereau")
    private String numeroBordereau;

    @NotNull
    @Column(name = "date_bordereau", nullable = false)
    private LocalDate dateBordereau;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeBordereau type;

    @ManyToOne
    @JsonIgnoreProperties("bordereaus")
    private Operation operation;

    @ManyToOne
    @JsonIgnoreProperties("bordereaus")
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties("bordereaus")
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroBordereau() {
        return numeroBordereau;
    }

    public Bordereau numeroBordereau(String numeroBordereau) {
        this.numeroBordereau = numeroBordereau;
        return this;
    }

    public void setNumeroBordereau(String numeroBordereau) {
        this.numeroBordereau = numeroBordereau;
    }

    public LocalDate getDateBordereau() {
        return dateBordereau;
    }

    public Bordereau dateBordereau(LocalDate dateBordereau) {
        this.dateBordereau = dateBordereau;
        return this;
    }

    public void setDateBordereau(LocalDate dateBordereau) {
        this.dateBordereau = dateBordereau;
    }

    public TypeBordereau getType() {
        return type;
    }

    public Bordereau type(TypeBordereau type) {
        this.type = type;
        return this;
    }

    public void setType(TypeBordereau type) {
        this.type = type;
    }

    public Operation getOperation() {
        return operation;
    }

    public Bordereau operation(Operation operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Client getClient() {
        return client;
    }

    public Bordereau client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employe getEmploye() {
        return employe;
    }

    public Bordereau employe(Employe employe) {
        this.employe = employe;
        return this;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bordereau)) {
            return false;
        }
        return id != null && id.equals(((Bordereau) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Bordereau{" +
            "id=" + getId() +
            ", numeroBordereau='" + getNumeroBordereau() + "'" +
            ", dateBordereau='" + getDateBordereau() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
