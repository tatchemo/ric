package com.urservices.myapp.domain.enumeration;

/**
 * The TypeBordereau enumeration.
 */
public enum TypeBordereau {
    Approvionnement, Livraison, Retour
}
