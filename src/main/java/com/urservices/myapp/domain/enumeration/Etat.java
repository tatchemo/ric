package com.urservices.myapp.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    Brouillon, EnAttente, EnCours, Terminer, Payer
}
