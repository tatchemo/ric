package com.urservices.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A LigneOperation.
 */
@Entity
@Table(name = "ligne_operation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LigneOperation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "quantite", nullable = false)
    private Long quantite;

    @NotNull
    @Column(name = "p_u", nullable = false)
    private Long pU;

    @Column(name = "montant_ligne_operation")
    private Long montantLigneOperation;

    @Column(name = "deleted")
    private Boolean deleted;

    @ManyToOne
    @JsonIgnoreProperties("ligneOperations")
    private Produit produit;

    @ManyToOne
    @JsonIgnoreProperties("ligneOperations")
    private Operation operation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuantite() {
        return quantite;
    }

    public LigneOperation quantite(Long quantite) {
        this.quantite = quantite;
        return this;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public Long getpU() {
        return pU;
    }

    public LigneOperation pU(Long pU) {
        this.pU = pU;
        return this;
    }

    public void setpU(Long pU) {
        this.pU = pU;
    }

    public Long getMontantLigneOperation() {
        return montantLigneOperation;
    }

    public LigneOperation montantLigneOperation(Long montantLigneOperation) {
        this.montantLigneOperation = montantLigneOperation;
        return this;
    }

    public void setMontantLigneOperation(Long montantLigneOperation) {
        this.montantLigneOperation = montantLigneOperation;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public LigneOperation deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Produit getProduit() {
        return produit;
    }

    public LigneOperation produit(Produit produit) {
        this.produit = produit;
        return this;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Operation getOperation() {
        return operation;
    }

    public LigneOperation operation(Operation operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LigneOperation)) {
            return false;
        }
        return id != null && id.equals(((LigneOperation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "LigneOperation{" +
            "id=" + getId() +
            ", quantite=" + getQuantite() +
            ", pU=" + getpU() +
            ", montantLigneOperation=" + getMontantLigneOperation() +
            ", deleted='" + isDeleted() + "'" +
            "}";
    }
}
