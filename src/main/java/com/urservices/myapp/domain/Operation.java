package com.urservices.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import com.urservices.myapp.domain.enumeration.Etat;

/**
 * A Operation.
 */
@Entity
@Table(name = "operation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Operation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero_operation", nullable = false)
    private String numeroOperation;

    @NotNull
    @Column(name = "date_operation", nullable = false)
    private LocalDate dateOperation;

    @NotNull
    @Column(name = "montant_total", nullable = false)
    private Long montantTotal;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "returned")
    private Boolean returned;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private Etat etat;

    @ManyToOne
    @JsonIgnoreProperties("operations")
    private TypeOperation typeOperation;

    @ManyToOne
    @JsonIgnoreProperties("operations")
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties("operations")
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroOperation() {
        return numeroOperation;
    }

    public Operation numeroOperation(String numeroOperation) {
        this.numeroOperation = numeroOperation;
        return this;
    }

    public void setNumeroOperation(String numeroOperation) {
        this.numeroOperation = numeroOperation;
    }

    public LocalDate getDateOperation() {
        return dateOperation;
    }

    public Operation dateOperation(LocalDate dateOperation) {
        this.dateOperation = dateOperation;
        return this;
    }

    public void setDateOperation(LocalDate dateOperation) {
        this.dateOperation = dateOperation;
    }

    public Long getMontantTotal() {
        return montantTotal;
    }

    public Operation montantTotal(Long montantTotal) {
        this.montantTotal = montantTotal;
        return this;
    }

    public void setMontantTotal(Long montantTotal) {
        this.montantTotal = montantTotal;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Operation deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isReturned() {
        return returned;
    }

    public Operation returned(Boolean returned) {
        this.returned = returned;
        return this;
    }

    public void setReturned(Boolean returned) {
        this.returned = returned;
    }

    public Etat getEtat() {
        return etat;
    }

    public Operation etat(Etat etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public TypeOperation getTypeOperation() {
        return typeOperation;
    }

    public Operation typeOperation(TypeOperation typeOperation) {
        this.typeOperation = typeOperation;
        return this;
    }

    public void setTypeOperation(TypeOperation typeOperation) {
        this.typeOperation = typeOperation;
    }

    public Client getClient() {
        return client;
    }

    public Operation client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employe getEmploye() {
        return employe;
    }

    public Operation employe(Employe employe) {
        this.employe = employe;
        return this;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Operation)) {
            return false;
        }
        return id != null && id.equals(((Operation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Operation{" +
            "id=" + getId() +
            ", numeroOperation='" + getNumeroOperation() + "'" +
            ", dateOperation='" + getDateOperation() + "'" +
            ", montantTotal=" + getMontantTotal() +
            ", deleted='" + isDeleted() + "'" +
            ", returned='" + isReturned() + "'" +
            ", etat='" + getEtat() + "'" +
            "}";
    }
}
