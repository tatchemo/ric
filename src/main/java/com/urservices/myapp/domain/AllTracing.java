package com.urservices.myapp.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A AllTracing.
 */
@Entity
@Table(name = "all_tracing")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AllTracing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(min = 5, max = 250)
    @Column(name = "log_description", length = 30)
    private String logDescription;

    @NotNull
    @Column(name = "entity_logged", nullable = false)
    private String entityLogged;

    @NotNull
    @Column(name = "log_date", nullable = false)
    private Instant logDate;

    @NotNull
    @Column(name = "log_generate_by", nullable = false)
    private String logGenerateBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogDescription() {
        return logDescription;
    }

    public AllTracing logDescription(String logDescription) {
        this.logDescription = logDescription;
        return this;
    }

    public void setLogDescription(String logDescription) {
        this.logDescription = logDescription;
    }

    public String getEntityLogged() {
        return entityLogged;
    }

    public AllTracing entityLogged(String entityLogged) {
        this.entityLogged = entityLogged;
        return this;
    }

    public void setEntityLogged(String entityLogged) {
        this.entityLogged = entityLogged;
    }

    public Instant getLogDate() {
        return logDate;
    }

    public AllTracing logDate(Instant logDate) {
        this.logDate = logDate;
        return this;
    }

    public void setLogDate(Instant logDate) {
        this.logDate = logDate;
    }

    public String getLogGenerateBy() {
        return logGenerateBy;
    }

    public AllTracing logGenerateBy(String logGenerateBy) {
        this.logGenerateBy = logGenerateBy;
        return this;
    }

    public void setLogGenerateBy(String logGenerateBy) {
        this.logGenerateBy = logGenerateBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AllTracing)) {
            return false;
        }
        return id != null && id.equals(((AllTracing) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AllTracing{" +
            "id=" + getId() +
            ", logDescription='" + getLogDescription() + "'" +
            ", entityLogged='" + getEntityLogged() + "'" +
            ", logDate='" + getLogDate() + "'" +
            ", logGenerateBy='" + getLogGenerateBy() + "'" +
            "}";
    }
}
