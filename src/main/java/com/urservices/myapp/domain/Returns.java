package com.urservices.myapp.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Returns.
 */
@Entity
@Table(name = "returns")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Returns extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero_retour", nullable = false)
    private String numeroRetour;

    @NotNull
    @Column(name = "motif", nullable = false)
    private String motif;

    @Column(name = "commentaire")
    private String commentaire;

    @ManyToOne
    @JsonIgnoreProperties("returns")
    private Operation operation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroRetour() {
        return numeroRetour;
    }

    public Returns numeroRetour(String numeroRetour) {
        this.numeroRetour = numeroRetour;
        return this;
    }

    public void setNumeroRetour(String numeroRetour) {
        this.numeroRetour = numeroRetour;
    }

    public String getMotif() {
        return motif;
    }

    public Returns motif(String motif) {
        this.motif = motif;
        return this;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public Returns commentaire(String commentaire) {
        this.commentaire = commentaire;
        return this;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Operation getOperation() {
        return operation;
    }

    public Returns operation(Operation operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Returns)) {
            return false;
        }
        return id != null && id.equals(((Returns) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Returns{" +
            "id=" + getId() +
            ", numeroRetour='" + getNumeroRetour() + "'" +
            ", motif='" + getMotif() + "'" +
            ", commentaire='" + getCommentaire() + "'" +
            "}";
    }
}
