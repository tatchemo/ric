package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Facture;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Facture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FactureRepository extends JpaRepository<Facture, Long>, QuerydslPredicateExecutor<Facture> {

    @Query("SELECT COUNT(f) FROM Facture f")
    int getNomberOfAllFacture();

    @Query("SELECT f FROM Facture f WHERE f.operation.id = :id")
    Facture getFactureOfOperation(@Param("id") long id);

    @Query("SELECT f.lastModifiedBy FROM Facture f WHERE f.id =:id")
    String getTheLastModifiedBy(@Param("id") long id);

}
