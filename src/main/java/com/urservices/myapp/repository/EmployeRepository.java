package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Employe;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Employe entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeRepository extends JpaRepository<Employe, Long>, QuerydslPredicateExecutor<Employe> {

    @Query("SELECT e FROM Employe e WHERE e.user.id = :id")
    Employe getEmployeOfUser(@Param("id") long id);

    @Query("SELECT e FROM Employe e WHERE e.id = :id")
    Employe getEmployeOfOperation(@Param("id") long id);

}
