package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Produit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



/**
 * Spring Data  repository for the Produit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

    @Query("SELECT p FROM Produit p ORDER BY p.designation asc")
    Page<Produit> findAllAsc(Pageable pageable);

    @Query("SELECT p FROM Produit p WHERE p.quantiteStock > 0 ORDER BY p.designation asc")
    Page<Produit> findAllCanSellAsc(Pageable pageable);

    @Query("SELECT p.lastModifiedBy FROM Produit p WHERE p.id =:id")
    String getTheLastModifiedBy(@Param("id") long id);
}
