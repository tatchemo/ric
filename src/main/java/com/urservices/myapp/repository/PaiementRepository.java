package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Paiement;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Paiement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaiementRepository extends JpaRepository<Paiement, Long> {

    @Query("SELECT p FROM Paiement p WHERE p.facture.id  = :id")
    List<Paiement> getAllPaimentOfFacture(@Param("id") long id);
}
