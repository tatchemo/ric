package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Client entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientRepository extends JpaRepository<Client, Long>, QuerydslPredicateExecutor<Client> {

    @Query("SELECT c.lastModifiedBy FROM Client c WHERE c.id =:id")
    String getTheLastModifiedBy(@Param("id") long id);

    @Query("SELECT c FROM Client c WHERE c.deleted = false AND c.fournisseur = false ")
    Page<Client> findAllCLient(Pageable pageable);

    @Query("SELECT c FROM Client c WHERE c.deleted = false AND c.fournisseur = true ")
    Page<Client> findAllFournisseur(Pageable pageable);


}
