package com.urservices.myapp.repository;

import com.urservices.myapp.domain.AllTracing;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AllTracing entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AllTracingRepository extends JpaRepository<AllTracing, Long> {

}
