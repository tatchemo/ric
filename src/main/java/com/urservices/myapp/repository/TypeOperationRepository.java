package com.urservices.myapp.repository;

import com.urservices.myapp.domain.TypeOperation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TypeOperation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeOperationRepository extends JpaRepository<TypeOperation, Long> {

    @Query("SELECT t FROM TypeOperation t WHERE t.libelle LIKE 'RETURN'")
    TypeOperation getReturn();
}
