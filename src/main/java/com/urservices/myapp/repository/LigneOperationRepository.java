package com.urservices.myapp.repository;

import com.urservices.myapp.domain.LigneOperation;
import com.urservices.myapp.service.dto.LigneOperationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the LigneOperation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LigneOperationRepository extends JpaRepository<LigneOperation, Long>, QuerydslPredicateExecutor<LigneOperation> {

    @Query("SELECT lo FROM LigneOperation lo WHERE lo.deleted = false AND lo.operation.id = :id")
    List<LigneOperation> findByOperation_Id(@Param("id") long id);

    @Query("SELECT lo FROM LigneOperation lo WHERE lo.deleted = false AND lo.operation.id = :id")
    List<LigneOperationDTO> findByOperation_IdForPrint(@Param("id") long id);

    @Query("SELECT l FROM LigneOperation l WHERE l.deleted = false")
    Page<LigneOperation> findAllNotDeleted(Pageable pageable);

    @Query("SELECT l FROM LigneOperation l WHERE l.deleted = false AND l.produit.id = :id")
    List<LigneOperation> findAllOfProduit(@Param("id") long id);

    @Query("SELECT l FROM LigneOperation l WHERE l.deleted = false AND l.operation.id = :id")
    List<LigneOperation> findAllOfFactureOperation(@Param("id") long id);
}
