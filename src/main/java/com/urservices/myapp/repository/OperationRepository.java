package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Employe;
import com.urservices.myapp.domain.Operation;
import com.urservices.myapp.service.dto.OperationDTO;
import org.hibernate.sql.Select;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Operation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OperationRepository extends JpaRepository<Operation, Long>, QuerydslPredicateExecutor<Operation> {
    @Query("SELECT o FROM Operation o WHERE o.deleted = false")
    Page<Operation> findAllNotDeleted(Pageable pageable);

    @Query("SELECT o FROM Operation o WHERE o.deleted = false AND o.typeOperation.id = 1")
    Page<Operation> findAllNotDeletedAppro(Pageable pageable);

    @Query("SELECT o FROM Operation o WHERE o.deleted = false AND o.typeOperation.id = 2")
    Page<Operation> findAllNotDeletedSell(Pageable pageable);

    @Query("select COUNT(o) FROM Operation o WHERE o.typeOperation.libelle like 'APPRO'")
    int getNumberOfLigneBuy();

    @Query("select COUNT(o) FROM Operation o WHERE o.typeOperation.libelle like 'SELL'")
    int getNumberOfLigneSell();

    @Query("select COUNT(o) FROM Operation o WHERE o.typeOperation.libelle like 'RETURN'")
    int getNumberOfLigneReturn();

    @Query("SELECT o.lastModifiedBy FROM Operation o WHERE o.id =:id")
    String getTheLastModifiedBy(@Param("id") long id);

    @Query("SELECT o.employe FROM Operation o, Employe e WHERE o.id =:id AND e.id = o.employe.id")
    Employe getOfOperation(@Param("id") long id);

}
