package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Returns;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Returns entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReturnsRepository extends JpaRepository<Returns, Long> {

}
