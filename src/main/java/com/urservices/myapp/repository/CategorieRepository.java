package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Categorie;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Categorie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorieRepository extends JpaRepository<Categorie, Long> {

    @Query("SELECT c.lastModifiedBy FROM Categorie c WHERE c.id =:id")
    String getTheLastModifiedBy(@Param("id") long id);

}
