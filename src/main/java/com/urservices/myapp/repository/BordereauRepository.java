package com.urservices.myapp.repository;

import com.urservices.myapp.domain.Bordereau;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Bordereau entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BordereauRepository extends JpaRepository<Bordereau, Long>, QuerydslPredicateExecutor<Bordereau> {

    @Query("SELECT COUNT(b) FROM Bordereau b WHERE b.type = 'Approvionnement'")
    int getNumberAppro();

    @Query("SELECT COUNT(b) FROM Bordereau b WHERE b.type = 'Livraison'")
    int getNumberLivraison();

    @Query("SELECT b FROM Bordereau b WHERE b.operation.id = :id")
    Bordereau getBordereauOfOperation(@Param("id") long id);
}
