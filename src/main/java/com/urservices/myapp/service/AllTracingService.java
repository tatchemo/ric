package com.urservices.myapp.service;

import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.service.dto.AllTracingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.AllTracing}.
 */
public interface AllTracingService {

    /**
     * Save a allTracing.
     *
     * @param allTracing the entity to save.
     * @return the persisted entity.
     */
    AllTracing save(AllTracing allTracing);

    /**
     * Get all the allTracings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AllTracing> findAll(Pageable pageable);


    /**
     * Get the "id" allTracing.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AllTracing> findOne(Long id);

    /**
     * Delete the "id" allTracing.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
