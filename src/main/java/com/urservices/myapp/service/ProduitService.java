package com.urservices.myapp.service;

import com.urservices.myapp.domain.Produit;
import com.urservices.myapp.service.dto.ProduitDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.Produit}.
 */
public interface ProduitService {

    /**
     * Save a produit.
     *
     * @param produit the entity to save.
     * @return the persisted entity.
     */
    Produit save(Produit produit);

    /**
     * Save a produit.
     *
     * @param produitDTO the entity to save.
     * @return the persisted entity.
     */
    ProduitDTO saveDto(ProduitDTO produitDTO);

    /**
     * Get all the produits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Produit> findAll(Pageable pageable);

    /**
     * Get all the produits can be sell.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Produit> findAllCanSell(Pageable pageable);


    /**
     * Get the "id" produit.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Produit> findOne(Long id);

    Optional<ProduitDTO> findCompletId(Long id);

    /**
     * Delete the "id" produit.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
