package com.urservices.myapp.service;

import com.urservices.myapp.domain.TypeOperation;
import com.urservices.myapp.service.dto.TypeOperationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.TypeOperation}.
 */
public interface TypeOperationService {

    /**
     * Save a typeOperation.
     *
     * @param typeOperationDTO the entity to save.
     * @return the persisted entity.
     */
    TypeOperation save(TypeOperation typeOperation);

    /**
     * Get all the typeOperations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TypeOperation> findAll(Pageable pageable);


    /**
     * Get the "id" typeOperation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TypeOperation> findOne(Long id);

    /**
     * Delete the "id" typeOperation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
