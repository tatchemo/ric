package com.urservices.myapp.service.impl;

import com.urservices.myapp.service.AllTracingService;
import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.repository.AllTracingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AllTracing}.
 */
@Service
@Transactional
public class AllTracingServiceImpl implements AllTracingService {

    private final Logger log = LoggerFactory.getLogger(AllTracingServiceImpl.class);

    private final AllTracingRepository allTracingRepository;

    public AllTracingServiceImpl(AllTracingRepository allTracingRepository) {
        this.allTracingRepository = allTracingRepository;
    }

    /**
     * Save a allTracing.
     *
     * @param allTracing the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AllTracing save(AllTracing allTracing) {
        log.debug("Request to save AllTracing : {}", allTracing);
        return allTracingRepository.save(allTracing);
    }

    /**
     * Get all the allTracings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AllTracing> findAll(Pageable pageable) {
        log.debug("Request to get all AllTracings");
        return allTracingRepository.findAll(pageable);
    }


    /**
     * Get one allTracing by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AllTracing> findOne(Long id) {
        log.debug("Request to get AllTracing : {}", id);
        return allTracingRepository.findById(id);
    }

    /**
     * Delete the allTracing by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AllTracing : {}", id);
        allTracingRepository.deleteById(id);
    }
}
