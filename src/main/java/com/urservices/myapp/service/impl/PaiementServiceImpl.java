package com.urservices.myapp.service.impl;

import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.domain.Employe;
import com.urservices.myapp.domain.Facture;
import com.urservices.myapp.domain.enumeration.Etat;
import com.urservices.myapp.repository.*;
import com.urservices.myapp.service.PaiementService;
import com.urservices.myapp.domain.Paiement;
import com.urservices.myapp.service.dto.PaiementDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Paiement}.
 */
@Service
@Transactional
public class PaiementServiceImpl implements PaiementService {

    private final Logger log = LoggerFactory.getLogger(PaiementServiceImpl.class);

    private final PaiementRepository paiementRepository;
    private final FactureRepository factureRepository;
    private final AllTracingRepository allTracingRepository;
    private final EmployeRepository employeRepository;
    private final UserRepository userRepository;


    public PaiementServiceImpl(
        PaiementRepository paiementRepository,
        FactureRepository factureRepository,
        AllTracingRepository allTracingRepository,
        EmployeRepository employeRepository,
        UserRepository userRepository
        ) {
        this.paiementRepository = paiementRepository;
        this.factureRepository = factureRepository;
        this.allTracingRepository = allTracingRepository;
        this.employeRepository = employeRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a paiement.
     *
     * @param paiement the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Paiement save(Paiement paiement) {
        log.debug("Request to save Paiement : {}", paiement);
        Facture facture = paiement.getFacture();
        AllTracing tracing = new AllTracing();
        List<AllTracing> tracings = new ArrayList<>();
        tracing.setLogDate(Instant.now());
        tracing.setLogDescription("");
        facture.setNetAPayer(facture.getNetAPayer() - paiement.getMontantPaiement());
        if (facture.getNetAPayer() == 0) {
            facture.setEtat(Etat.Payer);
            facture = factureRepository.save(facture);
            tracing.setLogGenerateBy(factureRepository.getTheLastModifiedBy(facture.getId()));
            tracing.setEntityLogged("Facture");
            tracing.setLogDescription("Mise a jour de la Facture [" + facture.getNumeroFacture() + "] : " +
                "Facture::Etat : [" + Etat.Terminer + "] ==> [" + facture.getEtat() + "] .");
            tracings.add(allTracingRepository.save(tracing));
        }
        facture = factureRepository.save(facture);
        Employe em = employeRepository.getEmployeOfUser(userRepository.getUserLogin(
            factureRepository.getTheLastModifiedBy(facture.getId())));
        paiement.setEmploye(em);
        paiement = paiementRepository.save(paiement);
        tracing.setEntityLogged("Paiement");
        tracing.setLogGenerateBy(paiement.getCreatedBy());
        tracing.setLogDescription("Paiement de ["+ paiement.getMontantPaiement() +"] sur la facture ["
            + facture.getNumeroFacture() + "].");
        tracings.add(allTracingRepository.save(tracing));
        allTracingRepository.saveAll(tracings);
        paiement.setFacture(facture);
        return paiement;
    }

    @Override
    public PaiementDTO saveDTO(PaiementDTO paiementDTO) {
        Paiement paiement = paiementDTO.getPaiement();
        Facture facture = paiementDTO.getFacture();
        PaiementDTO result = new PaiementDTO();
        /** Traitement Paiement pour Facture **/
        facture.setNetAPayer(facture.getNetAPayer() - paiement.getMontantPaiement());
        if (facture.getNetAPayer() == 0) {
            facture.setEtat(Etat.Payer);
            facture = factureRepository.save(facture);
        }
        if (paiement.getId() == null) {
            paiement = paiementRepository.save(paiement);
        }
        result.setPaiement(paiementRepository.save(paiement));
        return result;
    }

    /**
     * Get all the paiements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Paiement> findAll(Pageable pageable) {
        log.debug("Request to get all Paiements");
        return paiementRepository.findAll(pageable);
    }


    /**
     * Get one paiement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Paiement> findOne(Long id) {
        log.debug("Request to get Paiement : {}", id);
        return paiementRepository.findById(id);
    }

    /**
     * Delete the paiement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Paiement : {}", id);
        paiementRepository.deleteById(id);
    }
}
