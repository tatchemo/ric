package com.urservices.myapp.service.impl;

import com.urservices.myapp.domain.LigneOperation;
import com.urservices.myapp.repository.LigneOperationRepository;
import com.urservices.myapp.service.BordereauService;
import com.urservices.myapp.domain.Bordereau;
import com.urservices.myapp.repository.BordereauRepository;
import com.urservices.myapp.service.dto.BordereauDTO;
import com.urservices.myapp.service.dto.LigneOpSimpleDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Bordereau}.
 */
@Service
@Transactional
public class BordereauServiceImpl implements BordereauService {

    private final Logger log = LoggerFactory.getLogger(BordereauServiceImpl.class);

    private final BordereauRepository bordereauRepository;
    private final LigneOperationRepository ligneOperationRepository;


    public BordereauServiceImpl(
        BordereauRepository bordereauRepository,
        LigneOperationRepository ligneOperationRepository
    ) {
        this.bordereauRepository = bordereauRepository;
        this.ligneOperationRepository = ligneOperationRepository;
    }

    /**
     * Save a bordereau.
     *
     * @param bordereau the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Bordereau save(Bordereau bordereau) {
        log.debug("Request to save Bordereau : {}", bordereau);
        return bordereauRepository.save(bordereau);
    }

    /**
     * Get all the bordereaus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Bordereau> findAll(Pageable pageable) {
        log.debug("Request to get all Bordereaus");
        return bordereauRepository.findAll(pageable);
    }


    /**
     * Get one bordereau by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Bordereau> findOne(Long id) {
        log.debug("Request to get Bordereau : {}", id);
        return bordereauRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BordereauDTO> findOneWithLo(Long id) {
        log.debug("Request to get Bordereau : {}", id);
        BordereauDTO result = new BordereauDTO();
        List<LigneOperation> ligneOperations ;
        Bordereau bordereau = bordereauRepository.getOne(id);
        ligneOperations = ligneOperationRepository.findAllOfFactureOperation(bordereau.getOperation().getId());
        List<LigneOpSimpleDTO> designations = new ArrayList<>();
        LigneOpSimpleDTO designation = new LigneOpSimpleDTO();
        for (int i = 0; i<ligneOperations.size(); i++) {
            designation.setId(ligneOperations.get(i).getId());
            designation.setDesignation(ligneOperations.get(i).getProduit().getDesignation());
            designation.setQuantite(ligneOperations.get(i).getQuantite());
            designation.setpU(ligneOperations.get(i).getpU());
            designation.setMontantLigneOperation(ligneOperations.get(i).getMontantLigneOperation());
            designations.add(i, designation);
            designation = new LigneOpSimpleDTO();
        }
        result.setLigneOpSimpleDTOS(designations);
        result.setLigneOperations(ligneOperations);
        result.setBordereau(bordereau);
        return Optional.of(result);
    }

    /**
     * Delete the bordereau by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bordereau : {}", id);
        bordereauRepository.deleteById(id);
    }
}
