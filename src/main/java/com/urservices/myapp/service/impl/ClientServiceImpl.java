package com.urservices.myapp.service.impl;

import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.repository.AllTracingRepository;
import com.urservices.myapp.service.ClientService;
import com.urservices.myapp.domain.Client;
import com.urservices.myapp.repository.ClientRepository;
import com.urservices.myapp.service.dto.ClientDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Client}.
 */
@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    private final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    private final ClientRepository clientRepository;
    private final AllTracingRepository allTracingRepository;


    public ClientServiceImpl(
        ClientRepository clientRepository,
        AllTracingRepository allTracingRepository
    ) {
        this.clientRepository = clientRepository;
        this.allTracingRepository = allTracingRepository;
    }

    /**
     * Save a client.
     *
     * @param client the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Client save(Client client) {
        log.debug("Request to save Client : {}", client);
        return clientRepository.save(client);

    }
    /**
     * Save a client.
     *
     * @param clientDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClientDTO saveDto(ClientDTO clientDTO) {
        log.debug("Request to save Client : {}", clientDTO);
        ClientDTO result = new ClientDTO();
        Client t = new Client();
        if (clientDTO.getId() != null) {
            t = clientRepository.getOne(clientDTO.getId());
        }
        Client client;
        AllTracing tracing = new AllTracing();
        String p = "";
        tracing.setLogDate(Instant.now());
        tracing.setEntityLogged("Client");
        if (clientDTO.getId() == null) {
            client = clientRepository.save(clientDTO.getClient());
            tracing.setLogGenerateBy(client.getCreatedBy());
            tracing.setLogDescription("Creation du Client [" + client.getNom() + " " + client.getPrenom() + "]");
        } else {
            if (!clientDTO.getNom().equals(t.getNom())) {
                if (p != "") { p = p + ", "; }
                p = p + "Client::categorie : [" + t.getNom() + "] ==> [" + clientDTO.getNom() + "]";
            }
            if (!clientDTO.getPrenom().equals(t.getPrenom())) {
                if (p != "") { p = p + ", "; }
                p = p + "Client::Designation : [" + t.getPrenom() + "] ==> [" + clientDTO.getPrenom() + "]";
            }
            if (!clientDTO.getTelephone().equals(t.getTelephone())) {
                if (p != "") { p = p + ", "; }
                p = p + "Client::QuantiteMinimale : [" + t.getTelephone() + "] ==> [" + clientDTO.getTelephone() + "]";
            }
            if (clientDTO.isFournisseur() != t.isFournisseur()) {
                if (p != "") { p = p + ", "; }
                p = p + "Client::Fournisseur : [" + t.isFournisseur() + "] ==> [" + clientDTO.isFournisseur() + "]";
            }
            tracing.setLogDescription("Modification du Client: " + p + ".");
            client = clientRepository.save(clientDTO.getClient());
            tracing.setLogGenerateBy(clientRepository.getTheLastModifiedBy(client.getId()));
        }
        tracing = allTracingRepository.save(tracing);
        result.setClient(client);
        result.setTracing(tracing);
        return result;

    }

    /**
     * Get all the clients.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Client> findAll(Pageable pageable) {
        log.debug("Request to get all Clients");
        return clientRepository.findAll(pageable);
    }

    /**
     * Get all the clients Client.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Client> findAllClient(Pageable pageable) {
        log.debug("Request to get all Clients");
        return clientRepository.findAllCLient(pageable);
    }
    /**
     * Get all the clients Client.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Client> findAllFournisseur(Pageable pageable) {
        log.debug("Request to get all Clients");
        return clientRepository.findAllFournisseur(pageable);
    }


    /**
     * Get one client by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Client> findOne(Long id) {
        log.debug("Request to get Client : {}", id);
        return clientRepository.findById(id);
    }

    /**
     * Delete the client by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Client : {}", id);
        Client client;
        client = clientRepository.getOne(id);
        client.setDeleted(true);
        clientRepository.save(client);
//        clientRepository.deleteById(id);
    }
}
