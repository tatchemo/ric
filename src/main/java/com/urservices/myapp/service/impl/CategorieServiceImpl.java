package com.urservices.myapp.service.impl;

import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.repository.AllTracingRepository;
import com.urservices.myapp.service.CategorieService;
import com.urservices.myapp.domain.Categorie;
import com.urservices.myapp.repository.CategorieRepository;
import com.urservices.myapp.service.dto.CategorieDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Categorie}.
 */
@Service
@Transactional
public class CategorieServiceImpl implements CategorieService {

    private final Logger log = LoggerFactory.getLogger(CategorieServiceImpl.class);

    private final CategorieRepository categorieRepository;
    private final AllTracingRepository allTracingRepository;


    public CategorieServiceImpl(
        CategorieRepository categorieRepository,
        AllTracingRepository allTracingRepository
    ) {
        this.categorieRepository = categorieRepository;
        this.allTracingRepository = allTracingRepository;
    }

    /**
     * Save a categorie.
     *
     * @param categorie the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Categorie save(Categorie categorie) {
        log.debug("Request to save Categorie : {}", categorie);
        return categorieRepository.save(categorie);
    }
    /**
     * Save a categorie.
     *
     * @param categorieDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CategorieDTO saveDto(CategorieDTO categorieDTO) {
        log.debug("Request to save Categorie : {}", categorieDTO);
        CategorieDTO result = new CategorieDTO();
        Categorie t = new Categorie();
       if (categorieDTO.getId() != null) {
           t = categorieRepository.getOne(categorieDTO.getId());
       }
        Categorie categorie = categorieRepository.save(categorieDTO.getCategorie());
        AllTracing tracing = new AllTracing();
        tracing.setLogDate(Instant.now());
        tracing.setEntityLogged("Categorie");
        if (categorieDTO.getId() == null) {
            tracing.setLogGenerateBy(categorie.getCreatedBy());
            tracing.setLogDescription("Creation de la categorie [" + categorie.getLibelle() + "]");
        } else {
            tracing.setLogGenerateBy(categorieRepository.getTheLastModifiedBy(categorie.getId()));
            tracing.setLogDescription("Modification de categorie::Libelle : [" + t.getLibelle() + "] ==> [" + categorie.getLibelle() + "]");
        }
        tracing = allTracingRepository.save(tracing);
        result.setTracing(tracing);
        result.setCategorie(categorie);
        return result;
    }

    /**
     * Get all the categories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Categorie> findAll(Pageable pageable) {
        log.debug("Request to get all Categories");
        return categorieRepository.findAll(pageable);
    }


    /**
     * Get one categorie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Categorie> findOne(Long id) {
        log.debug("Request to get Categorie : {}", id);
        return categorieRepository.findById(id);
    }

    /**
     * Delete the categorie by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Categorie : {}", id);
        categorieRepository.deleteById(id);
    }
}
