package com.urservices.myapp.service.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.urservices.myapp.domain.*;
import com.urservices.myapp.domain.enumeration.Etat;
import com.urservices.myapp.domain.enumeration.TypeBordereau;
import com.urservices.myapp.repository.*;
import com.urservices.myapp.service.OperationService;
import com.urservices.myapp.service.dto.OperationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Operation}.
 */
@Service
@Transactional
public class OperationServiceImpl implements OperationService {

    private final Logger log = LoggerFactory.getLogger(OperationServiceImpl.class);

    private final OperationRepository operationRepository;
    private final LigneOperationRepository ligneOperationRepository;
    private final BordereauRepository bordereauRepository;
    private final FactureRepository factureRepository;
    private final ReturnsRepository returnsRepository;
    private final TypeOperationRepository typeOperationRepository;
    private final ProduitRepository produitRepository;
    private final AllTracingRepository allTracingRepository;
    private final EmployeRepository employeRepository;
    private final UserRepository userRepository;


    public OperationServiceImpl(
        OperationRepository operationRepository,
        LigneOperationRepository ligneOperationRepository,
        BordereauRepository bordereauRepository,
        FactureRepository factureRepository,
        ReturnsRepository returnsRepository,
        TypeOperationRepository typeOperationRepository,
        ProduitRepository produitRepository,
        AllTracingRepository allTracingRepository,
        EmployeRepository employeRepository,
        UserRepository userRepository
    ) {
        this.operationRepository = operationRepository;
        this.ligneOperationRepository = ligneOperationRepository;
        this.bordereauRepository = bordereauRepository;
        this.factureRepository = factureRepository;
        this.returnsRepository = returnsRepository;
        this.typeOperationRepository = typeOperationRepository;
        this.produitRepository = produitRepository;
        this.allTracingRepository = allTracingRepository;
        this.employeRepository = employeRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a operation.
     *
     * @param operation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Operation save(Operation operation) {
        log.debug("Request to save Operation : {}", operation);
        return operationRepository.save(operation);
    }

    @Override
    public OperationDTO saveDTO(OperationDTO operationDTO) {
        Operation operation = operationDTO.getOperation();

        String generateNumeroOperation;
        String operationT = "";
        int nberLigne = 0;
        Bordereau bordereau = new Bordereau();
        String nberLigneString = "";
        String zeroBefore = "";
        int j = 0;
        if (operation.getTypeOperation().getLibelle().equals("APPRO")) {
            nberLigne = operationRepository.getNumberOfLigneBuy();
            nberLigne++;
            nberLigneString = nberLigne + "";
            j = 5 - nberLigneString.length();
            for (int i = 0; i < j; i++) {
                zeroBefore = zeroBefore + "0";
            }
            operationT = "d'Approvisionnement";
        }

        if (operation.getTypeOperation().getLibelle().equals("SELL")) {
            nberLigne = operationRepository.getNumberOfLigneSell();
            nberLigne++;
            nberLigneString = nberLigne + "";
            j = 5 - nberLigneString.length();
            for (int i = 0; i < j; i++) {
                zeroBefore = zeroBefore + "0";
            }
            operationT = "de Vente";
        }
        generateNumeroOperation = "OP/"
            + operation.getTypeOperation().getLibelle() + "/"
            + operation.getDateOperation().getChronology().dateNow().getYear()
            + "-" + zeroBefore + "" + nberLigne;
        /** Fin generation numeroOperation */

        operation.setNumeroOperation(generateNumeroOperation);
        operation = operationRepository.save(operation);
        //* now getting current user and set employe of operation **/
        Employe em = employeRepository.getEmployeOfUser(
            userRepository.getUserLogin(operation.getCreatedBy()));
        operation.setEmploye(em);
        operation = operationRepository.save(operation);

        List<LigneOperation> ligneOperations = new ArrayList<>();
        for (LigneOperation ligneOperation : operationDTO.getLigneOperations()) {
            ligneOperation.setOperation(operation);
            ligneOperations.add(ligneOperationRepository.save(ligneOperation));
        }

        List<AllTracing> tracings = new ArrayList<>();
        AllTracing tracing = new AllTracing();
            tracing.setLogDate(Instant.now());
            tracing.setEntityLogged("Operation");
            tracing.setLogGenerateBy(operation.getCreatedBy());
            tracing.setLogDescription("Creation de la operation " + operationT + " [" + operation.getNumeroOperation() + "]");
        tracings.add(allTracingRepository.save(tracing));

        OperationDTO result = operationDTO;
        /* result adding */
        result.setId(operation.getId());
        result.setLigneOperations(ligneOperations);
        result.setBordereau(bordereau);
        result.setTracings(tracings);

        return result;
    }

    @Override
    public OperationDTO updateDTO(OperationDTO operationDTO) {
        Operation o = operationRepository.getOne(operationDTO.getId());
        OperationDTO result = operationDTO;
        Operation operation;
        Operation operationRe = new Operation();
        List<LigneOperation> ligneOperations = new ArrayList<>();
        List<Produit> produits = new ArrayList<>();

        String generateNumeroOperation;
        String generateNumeroBordereau;
        String generateNumeroFacture;
        int nberLigne = 0;
        int nberFacture = 0;
        Bordereau bordereau = new Bordereau();
        Facture facture = new Facture();
        Returns returns = new Returns();
        String nberLigneString = "";
        String nberFactureString = "";
        String zeroBefore = "";
        int j = 0;

        /* tracing Initialisation and default value */
        List<AllTracing> tracings = new ArrayList<>();
        AllTracing tracing = new AllTracing();
        String p = "";
        boolean hasChange = false;
        tracing.setLogDate(Instant.now());
        tracing.setEntityLogged("Operation");
        /* tracing for operation update */

        if (o.getEmploye() != null && operationDTO.getEmploye() != null) {
            if (!o.getEmploye().equals(operationDTO.getEmploye())) {
                if (p != "") { p = p + ", \n"; }
                p = p + "Operation::Employe : " +
                    "[" + o.getEmploye().getNom() + " " + o.getEmploye().getPrenom() + "] " +
                    "==> [" + operationDTO.getEmploye().getNom() + " " + o.getEmploye().getPrenom() + "]";
                hasChange = true;
            }
        }
        if (!o.getDateOperation().equals(operationDTO.getDateOperation())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "Operation::DateOperation : [" + o.getDateOperation() + "] ==> [" + operationDTO.getDateOperation() + "]";
            hasChange = true;
        }
        if (!o.getMontantTotal().equals(operationDTO.getMontantTotal())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "Operation::MontantTotal : [" + o.getMontantTotal() + "] ==> [" + operationDTO.getMontantTotal() + "]";
            hasChange = true;
        }
        if (!o.getClient().equals(operationDTO.getClient())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "Operation::Client : " +
                "[" + o.getClient().getNom() + " "+ o.getClient().getPrenom() + "] " +
                "==> [" + operationDTO.getClient().getNom() + " " + operationDTO.getClient().getPrenom() + "]";
            hasChange = true;
        }
        operation = operationRepository.save(operationDTO.getOperation());
        String lM = operationRepository.getTheLastModifiedBy(operation.getId());
        //* now getting current user and set employe of operation **/
        Employe em = employeRepository.getEmployeOfUser(
            userRepository.getUserLogin(lM));
        operation.setEmploye(em);
        operation = operationRepository.save(operation);
        tracing.setLogGenerateBy(operation.getLastModifiedBy());
        tracing.setLogGenerateBy(lM);
        if (hasChange) {
            tracing.setLogDescription("Mise a jour de l'operation [" + operation.getNumeroOperation() + "] : \n" +
                ""+ p + ".");
            //* Tracing save **/
            tracings.add(allTracingRepository.save(tracing));
            //****/
        }

        /*End of tracing for operation update */
        if (operation.getEtat().equals(Etat.Terminer)) {
            tracing.setId(null);
            tracing.setLogDescription("Confirmation de l'operation " + operationDTO.getNumeroOperation() + " " +
                "==> [Etat]= Terminer .");
            //* Tracing save **/
            tracings.add(allTracingRepository.save(tracing));
            //****/
            if (operation.getTypeOperation().getLibelle().equals("APPRO")) {
                nberLigne = bordereauRepository.getNumberAppro();
                nberLigne++;
                nberLigneString = nberLigne + "";
                j = 5 - nberLigneString.length();
                for (int i = 0; i < j; i++) {
                    zeroBefore = zeroBefore + "0";
                }
                /** generation d'un bordereau de d'approvionnement */
                generateNumeroBordereau = "BOR/"
                    + operation.getTypeOperation().getLibelle()
                    + "-" + zeroBefore + nberLigne;
                bordereau.setOperation(operation);
                bordereau.setNumeroBordereau(generateNumeroBordereau);
                bordereau.setClient(operation.getClient());
                bordereau.setDateBordereau(operation.getDateOperation().getChronology().dateNow());
                bordereau.setEmploye(operation.getEmploye());
                bordereau.setType(TypeBordereau.Approvionnement);
                bordereau = bordereauRepository.save(bordereau);
                /** Incrementation de la quantite des produits */
                Produit prod;
                for (LigneOperation ligneOperation : operationDTO.getLigneOperations()) {
                    ligneOperation.setOperation(operation);
                    prod = ligneOperation.getProduit();
                    Long a;
                    a = prod.getQuantiteStock() + ligneOperation.getQuantite();
                    prod.setQuantiteStock(a);
                    produits.add(produitRepository.save(prod));
                    ligneOperations.add(ligneOperationRepository.save(ligneOperation));
                }
                /* tracing for bordereau update */
                tracing.setEntityLogged("Bordereau");
                tracing.setLogDescription("Creation du Bordereau D'approvisionnement [" + bordereau.getNumeroBordereau() + "]");
                //* Tracing save **/
                tracings.add(allTracingRepository.save(tracing));
                //****/

                /*End of tracing for bordereau update */
                result.setProduits(produits);
                result.setLigneOperations(ligneOperations);
            }


            if (operation.getTypeOperation().getLibelle().equals("SELL")) {
                nberLigne = bordereauRepository.getNumberLivraison();
                nberLigne++;
                nberLigneString = nberLigne + "";
                j = 5 - nberLigneString.length();
                for (int i = 0; i < j; i++) {
                    zeroBefore = zeroBefore + "0";
                }
                /* generation d'un bordereau de livraison **/
                generateNumeroBordereau = "BOR/"
                    + operation.getTypeOperation().getLibelle()
                    + "-" + zeroBefore + nberLigne;
                bordereau.setOperation(operation);
                bordereau.setNumeroBordereau(generateNumeroBordereau);
                bordereau.setClient(operation.getClient());
                bordereau.setDateBordereau(operation.getDateOperation().getChronology().dateNow());
                bordereau.setEmploye(operation.getEmploye());
                bordereau.setType(TypeBordereau.Livraison);
                bordereau = bordereauRepository.save(bordereau);

                /* Incrementation de la quantite des produits */
                Produit prod;
                for (LigneOperation ligneOperation : operationDTO.getLigneOperations()) {
                    ligneOperation.setOperation(operation);
                    prod = ligneOperation.getProduit();
                    Long a;
                    a = prod.getQuantiteStock() - ligneOperation.getQuantite();
                    prod.setQuantiteStock(a);
                    produits.add(produitRepository.save(prod));
                    ligneOperations.add(ligneOperationRepository.save(ligneOperation));
                }
                /* tracing for bordereau creation */
                tracing.setEntityLogged("Bordereau");
                tracing.setLogDescription("Creation du Bordereau de livraison [" + bordereau.getNumeroBordereau() + "]");
                //* Tracing save **/
                tracings.add(allTracingRepository.save(tracing));
                //****/
                /*End of tracing for bordereau creation */
                /* result adding */
                result.setProduits(produits);
                result.setLigneOperations(ligneOperations);

                /* Generation d'une Facture brouiilon de l'operation **/
                nberFacture = factureRepository.getNomberOfAllFacture();
                nberFacture++;
                nberFactureString = nberFacture + "";
                zeroBefore = "";
                j = 6 - nberFactureString.length();
                for (int i = 0; i < j; i++) {
                    zeroBefore = zeroBefore + "0";
                }
                generateNumeroFacture = "VTE/" + operation.getDateOperation().getChronology().dateNow().getYear()
                    + "-" + zeroBefore + nberFacture;
                facture.setOperation(operation);
                facture.setDateFacturation(operation.getDateOperation().getChronology().dateNow());
                facture.setEtat(Etat.Terminer);
                facture.setMontantFacure(operation.getMontantTotal());
                facture.setNetAPayer(operation.getMontantTotal());
                facture.setNumeroFacture(generateNumeroFacture);
                facture = factureRepository.save(facture);
                /* tracing for facture creation */
                tracing.setEntityLogged("Facture");
                tracing.setLogDescription("Creation de la facture [" + facture.getNumeroFacture() + "]");
                //* Tracing save **/
                tracings.add(allTracingRepository.save(tracing));
                //****/
                /*End of tracing for facture creation */
                /* result adding */
                result.setFacture(facture);
            }


            if (operation.isReturned().equals(true)) {
                nberLigne = operationRepository.getNumberOfLigneReturn();
                nberLigne++;
                nberLigneString = nberLigne + "";
                j = 4 - nberLigneString.length();
                for (int i = 0; i < j; i++) {
                    zeroBefore = zeroBefore + "0";
                }

                /** generation d'un bordereau de Retour **/
                generateNumeroBordereau = "BOR/"
                    + operation.getTypeOperation().getLibelle()
                    + "-" + zeroBefore + nberLigne;
                bordereau.setNumeroBordereau(generateNumeroBordereau);
                bordereau.setClient(operation.getClient());
                bordereau.setDateBordereau(operation.getDateOperation().getChronology().dateNow());
                bordereau.setEmploye(operation.getEmploye());
                bordereau.setType(TypeBordereau.Retour);
                bordereau = bordereauRepository.save(bordereau);

                /** Generation de l'operation de retour */
                nberLigne = operationRepository.getNumberOfLigneReturn();
                nberLigne++;
                nberLigneString = nberLigne + "";
                j = 5 - nberLigneString.length();
                for (int i = 0; i < j; i++) {
                    zeroBefore = zeroBefore + "0";
                }
                generateNumeroOperation = "OP/"
                    + operation.getTypeOperation().getLibelle() + "/"
                    + operation.getDateOperation().getChronology().dateNow().getYear()
                    + "-" + zeroBefore + "" + nberLigne;
                operationRe.setNumeroOperation(generateNumeroOperation);
                operationRe.setDateOperation(operation.getDateOperation().getChronology().dateNow());
                operationRe.setEtat(Etat.Terminer);
                operationRe.setClient(operation.getClient());
                operationRe.setMontantTotal(operation.getMontantTotal());
                operationRe.setDeleted(false);
                operationRe.setEmploye(operation.getEmploye());
                operationRe.setTypeOperation(typeOperationRepository.getReturn());
                operationRe.setReturned(false);
                operationRe.setId(null);
                operationRe = operationRepository.save(operationRe);
                result.setOperationRe(operationRe);
                /** generation du return */
                /** Incrementation de la quantite des produits */
                Produit prod;
                for (LigneOperation ligneOperation : operationDTO.getLigneOperations()) {
                    ligneOperation.setOperation(operationRe);
                    prod = ligneOperation.getProduit();
                    prod.setQuantiteStock(prod.getQuantiteStock() + ligneOperation.getQuantite());
                    produits.add(produitRepository.save(prod));
                    ligneOperations.add(ligneOperationRepository.save(ligneOperation));
                }
                returns.setNumeroRetour(operationRe.getNumeroOperation());
                /* result adding */
                returns.setOperation(operation);
                returns = returnsRepository.save(returns);
                /* result adding */
                result.setReturns(returns);
                result.setProduits(produits);
                result.setLigneOperations(ligneOperations);

            }
            /* result adding */
            result.setBordereau(bordereau);
        } else {
            for (LigneOperation ligneOperation : operationDTO.getLigneOperations()) {
                ligneOperation.setOperation(operation);
                ligneOperations.add(ligneOperationRepository.save(ligneOperation));
            }
            result.setLigneOperations(ligneOperations);
        }

        /* result adding */
        result.setTracings(tracings);
        result.setId(operation.getId());

        return result;
    }

    /**
     * Get all the operations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Operation> findAll(Pageable pageable) {
        log.debug("Request to get all Operations");
        return operationRepository.findAllNotDeleted(pageable);
    }

    @Override
    @Transactional
    public Page<Operation> findAll (Boolean deleted, Pageable pageable) {
        BooleanExpression predicat = null;
        QOperation operation = QOperation.operation;
        predicat = operation.deleted.eq(false);

        return operationRepository.findAll(predicat, pageable);
    }

    @Override
    @Transactional
    public Page<Operation> findAllAppro (Pageable pageable) {
        return operationRepository.findAllNotDeletedAppro(pageable);
    }

    @Override
    @Transactional
    public Page<Operation> findAllSell (Pageable pageable) {
        return operationRepository.findAllNotDeletedSell(pageable);
    }

    /**
     * Get one operation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Operation> findOne(Long id) {
        log.debug("Request to get Operation : {}", id);
        return operationRepository.findById(id);
    }

    @Override
    public Optional<OperationDTO> findCompletId(Long id) {
        log.debug("Request to get OperationDTO : {}", id);
        OperationDTO result = new OperationDTO();
        Operation operation = operationRepository.getOne(id);
        Facture facture = factureRepository.getFactureOfOperation(operation.getId());
        Bordereau bordereau = bordereauRepository.getBordereauOfOperation(operation.getId());
        result.setOperation(operation);
        result.setBordereau(bordereau);
        result.setFacture(facture);
        result.setLigneOperations(ligneOperationRepository.findByOperation_Id(id));
        return Optional.of(result);
    }


    /**
     * Delete the operation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Operation : {}", id);
        operationRepository.deleteById(id);
    }
}
