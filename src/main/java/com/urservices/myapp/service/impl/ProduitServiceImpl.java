package com.urservices.myapp.service.impl;

import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.repository.AllTracingRepository;
import com.urservices.myapp.repository.LigneOperationRepository;
import com.urservices.myapp.service.ProduitService;
import com.urservices.myapp.domain.Produit;
import com.urservices.myapp.repository.ProduitRepository;
import com.urservices.myapp.service.dto.ProduitDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Produit}.
 */
@Service
@Transactional
public class ProduitServiceImpl implements ProduitService {

    private final Logger log = LoggerFactory.getLogger(ProduitServiceImpl.class);

    private final ProduitRepository produitRepository;
    private final LigneOperationRepository ligneOperationRepository;
    private final AllTracingRepository allTracingRepository;


    public ProduitServiceImpl(
        ProduitRepository produitRepository,
        LigneOperationRepository ligneOperationRepository,
        AllTracingRepository allTracingRepository

    ) {
        this.produitRepository = produitRepository;
        this.ligneOperationRepository = ligneOperationRepository;
        this.allTracingRepository = allTracingRepository;
    }

    /**
     * Save a produit.
     *
     * @param produit the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Produit save(Produit produit) {
        log.debug("Request to save Produit : {}", produit);
        return produitRepository.save(produit);
    }
    /**
     * Save a produit.
     *
     * @param produitDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProduitDTO saveDto(ProduitDTO produitDTO) {
        log.debug("Request to save Produit : {}", produitDTO);
        ProduitDTO result =  new ProduitDTO();
        Produit prod = new Produit();
        if (produitDTO.getId() != null) {
            prod = produitRepository.getOne(produitDTO.getId());
        }
        AllTracing tracing = new AllTracing();
        String p = "";
        tracing.setLogDate(Instant.now());
        tracing.setEntityLogged("Produit");
        boolean hasChange = false;
        if (produitDTO.getId() == null) {
            prod = produitRepository.save(produitDTO.getProduit());
            tracing.setLogGenerateBy(prod.getCreatedBy());
            tracing.setLogDescription("Creation de la produit [" + prod.getDesignation() + "]");
        } else {
            if (!prod.getCategorie().equals(produitDTO.getCategorie())) {
                if (p != "") { p = p + ", \n"; }
                p = p + "Produit::categorie : [" + prod.getCategorie().getLibelle() + "] ==> [" + produitDTO.getCategorie().getLibelle() + "]";
                hasChange = true;
            }
            if (!prod.getDesignation().equals(produitDTO.getDesignation())) {
                if (p != "") { p = p + ", "; }
                p = p + "Produit::Designation : [" + prod.getDesignation() + "] ==> [" + produitDTO.getDesignation() + "]";
                hasChange = true;
            }
            if (!prod.getQuantiteMinimale().equals(produitDTO.getQuantiteMinimale())) {
                if (p != "") { p = p + ", "; }
                p = p + "Produit::QuantiteMinimale : [" + prod.getQuantiteMinimale() + "] ==> [" + produitDTO.getQuantiteMinimale() + "]";
                hasChange = true;
            }
            if (hasChange) {
                tracing.setLogDescription("Mise a jour du produit  [" + prod.getDesignation() + "] : \n" + ""+ p + ".");
            }

            prod = produitRepository.save(produitDTO.getProduit());
            tracing.setLogGenerateBy(produitRepository.getTheLastModifiedBy(prod.getId()));
        }
        tracing = allTracingRepository.save(tracing);
        result.setTracing(tracing);
        result.setProduit(prod);
        return result;
    }

    /**
     * Get all the produits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Produit> findAll(Pageable pageable) {
        log.debug("Request to get all Produits");
        return produitRepository.findAllAsc(pageable);
    }

    /**
     * Get all the produits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Produit> findAllCanSell(Pageable pageable) {
        log.debug("Request to get all Produits");
        return produitRepository.findAllCanSellAsc(pageable);
    }


    /**
     * Get one produit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Produit> findOne(Long id) {
        log.debug("Request to get Produit : {}", id);
        return produitRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ProduitDTO> findCompletId(Long id) {
        ProduitDTO result = new ProduitDTO();
        Produit produit;
        log.debug("Request to get Produit : {}", id);
        produit = produitRepository.getOne(id);
        result.setProduit(produit);
        result.setLigneOperations(ligneOperationRepository.findAllOfProduit(produit.getId()));
        return Optional.of(result);
    }

    /**
     * Delete the produit by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Produit : {}", id);
        produitRepository.deleteById(id);
    }
}
