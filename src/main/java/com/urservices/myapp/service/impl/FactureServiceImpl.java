package com.urservices.myapp.service.impl;

import com.urservices.myapp.domain.LigneOperation;
import com.urservices.myapp.domain.Operation;
import com.urservices.myapp.domain.Paiement;
import com.urservices.myapp.repository.*;
import com.urservices.myapp.service.EmployeService;
import com.urservices.myapp.service.FactureService;
import com.urservices.myapp.domain.Facture;
import com.urservices.myapp.service.OperationService;
import com.urservices.myapp.service.dto.FactureDTO;
import com.urservices.myapp.service.dto.LigneOpSimpleDTO;
import com.urservices.myapp.service.dto.LigneOperationDTO;
import com.urservices.myapp.service.dto.OperationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link Facture}.
 */
@Service
@Transactional
public class FactureServiceImpl implements FactureService {

    private final Logger log = LoggerFactory.getLogger(FactureServiceImpl.class);

    private final FactureRepository factureRepository;
    private final OperationRepository operationRepository;
    private final EmployeRepository employeRepository;
    private final LigneOperationRepository ligneOperationRepository;
    private final PaiementRepository paiementRepository;


    public FactureServiceImpl(
        FactureRepository factureRepository,
        OperationRepository operationRepository,
        EmployeRepository employeRepository,
        LigneOperationRepository ligneOperationRepository,
        PaiementRepository paiementRepository
        ) {
        this.factureRepository = factureRepository;
        this.operationRepository = operationRepository;
        this.employeRepository = employeRepository;
        this.ligneOperationRepository = ligneOperationRepository;
        this.paiementRepository = paiementRepository;
    }

    /**
     * Save a facture.
     *
     * @param facture the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Facture save(Facture facture) {
        log.debug("Request to save Facture : {}", facture);
        return factureRepository.save(facture);
    }

    /**
     * Get all the factures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Facture> findAll(Pageable pageable) {
        log.debug("Request to get all Factures");
        return factureRepository.findAll(pageable);
    }


    /**
     * Get one facture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Facture> findOne(Long id) {
        log.debug("Request to get Facture : {}", id);
        return factureRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FactureDTO> findCompletElement(Long id) {
        FactureDTO result = new FactureDTO();
        Operation operation;
        List<LigneOperation> ligneOperations;
        List<Paiement> paiements ;
        Facture facture = factureRepository.getOne(id);
        paiements = paiementRepository.getAllPaimentOfFacture(id);
        operation = operationRepository.getOne(facture.getOperation().getId());
        ligneOperations = ligneOperationRepository.findByOperation_Id(operation.getId());
        List<LigneOpSimpleDTO> ligneOpSimpleDTOS = new ArrayList<>();
        LigneOpSimpleDTO ligneOpSimpleDTO = new LigneOpSimpleDTO();
        for (int i = 0; i<ligneOperations.size(); i++) {
            ligneOpSimpleDTO.setId(ligneOperations.get(i).getId());
            ligneOpSimpleDTO.setDesignation(ligneOperations.get(i).getProduit().getDesignation());
            ligneOpSimpleDTO.setQuantite(ligneOperations.get(i).getQuantite());
            ligneOpSimpleDTO.setpU(ligneOperations.get(i).getpU());
            ligneOpSimpleDTO.setMontantLigneOperation(ligneOperations.get(i).getMontantLigneOperation());
            ligneOpSimpleDTOS.add(i, ligneOpSimpleDTO);
            ligneOpSimpleDTO = new LigneOpSimpleDTO();
        }
        result.setFacture(facture);
        result.setPaiements(paiements);
        result.setOperation(operation);
        result.setEmploye(operationRepository.getOfOperation(operation.getId()));
        result.setLigneOperations(ligneOperations);
        result.setLigneOpSimpleDTOS(ligneOpSimpleDTOS);
        return Optional.of(result);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Facture> findOneForPrint (Long id) {
        System.out.println("****************************************");
        System.out.println("TESTING EXECTION FONCTION IN ServiceImpl");
        return factureRepository.findById(id);
    }

    /**
     * Delete the facture by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Facture : {}", id);
        factureRepository.deleteById(id);
    }
}
