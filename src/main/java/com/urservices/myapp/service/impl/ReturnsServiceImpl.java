package com.urservices.myapp.service.impl;

import com.urservices.myapp.service.ReturnsService;
import com.urservices.myapp.domain.Returns;
import com.urservices.myapp.repository.ReturnsRepository;
import com.urservices.myapp.service.dto.ReturnsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Returns}.
 */
@Service
@Transactional
public class ReturnsServiceImpl implements ReturnsService {

    private final Logger log = LoggerFactory.getLogger(ReturnsServiceImpl.class);

    private final ReturnsRepository returnsRepository;


    public ReturnsServiceImpl(ReturnsRepository returnsRepository) {
        this.returnsRepository = returnsRepository;
    }

    /**
     * Save a returns.
     *
     * @param returns the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Returns save(Returns returns) {
        log.debug("Request to save Returns : {}", returns);
        return returnsRepository.save(returns);
    }

    /**
     * Get all the returns.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Returns> findAll(Pageable pageable) {
        log.debug("Request to get all Returns");
        return returnsRepository.findAll(pageable);
    }


    /**
     * Get one returns by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Returns> findOne(Long id) {
        log.debug("Request to get Returns : {}", id);
        return returnsRepository.findById(id);
    }

    /**
     * Delete the returns by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Returns : {}", id);
        returnsRepository.deleteById(id);
    }
}
