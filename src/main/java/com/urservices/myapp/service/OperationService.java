package com.urservices.myapp.service;

import com.urservices.myapp.domain.Operation;
import com.urservices.myapp.service.dto.OperationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.Operation}.
 */
public interface OperationService {

    /**
     * Save a operation.
     *
     * @param operation the entity to save.
     * @return the persisted entity.
     */
    Operation save(Operation operation);

    OperationDTO saveDTO(OperationDTO operationDTO);

    OperationDTO updateDTO(OperationDTO operationDTO);

    /**
     * Get all the operations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Operation> findAll(Pageable pageable);

    Page<Operation> findAll(Boolean deleted, Pageable pageable);

    Page<Operation> findAllSell(Pageable pageable);

    Page<Operation> findAllAppro(Pageable pageable);


    /**
     * Get the "id" operation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Operation> findOne(Long id);


    Optional<OperationDTO> findCompletId(Long id);

    /**
     * Delete the "id" operation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
