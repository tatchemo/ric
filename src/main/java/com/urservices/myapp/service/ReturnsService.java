package com.urservices.myapp.service;

import com.urservices.myapp.domain.Returns;
import com.urservices.myapp.service.dto.ReturnsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.Returns}.
 */
public interface ReturnsService {

    /**
     * Save a returns.
     *
     * @param returns the entity to save.
     * @return the persisted entity.
     */
    Returns save(Returns returns);

    /**
     * Get all the returns.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Returns> findAll(Pageable pageable);


    /**
     * Get the "id" returns.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Returns> findOne(Long id);

    /**
     * Delete the "id" returns.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
