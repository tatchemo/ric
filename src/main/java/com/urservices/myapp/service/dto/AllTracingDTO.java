package com.urservices.myapp.service.dto;
import java.time.Instant;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.AllTracing} entity.
 */
public class AllTracingDTO implements Serializable {

    private Long id;

    @Size(min = 5, max = 250)
    private String logDescription;

    @NotNull
    private String entityLogged;

    @NotNull
    private Instant logDate;

    @NotNull
    private String logGenerateBy;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogDescription() {
        return logDescription;
    }

    public void setLogDescription(String logDescription) {
        this.logDescription = logDescription;
    }

    public String getEntityLogged() {
        return entityLogged;
    }

    public void setEntityLogged(String entityLogged) {
        this.entityLogged = entityLogged;
    }

    public Instant getLogDate() {
        return logDate;
    }

    public void setLogDate(Instant logDate) {
        this.logDate = logDate;
    }

    public String getLogGenerateBy() {
        return logGenerateBy;
    }

    public void setLogGenerateBy(String logGenerateBy) {
        this.logGenerateBy = logGenerateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AllTracingDTO allTracingDTO = (AllTracingDTO) o;
        if (allTracingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), allTracingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AllTracingDTO{" +
            "id=" + getId() +
            ", logDescription='" + getLogDescription() + "'" +
            ", entityLogged='" + getEntityLogged() + "'" +
            ", logDate='" + getLogDate() + "'" +
            ", logGenerateBy='" + getLogGenerateBy() + "'" +
            "}";
    }
}
