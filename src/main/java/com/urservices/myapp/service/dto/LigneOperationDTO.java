package com.urservices.myapp.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.LigneOperation} entity.
 */
public class LigneOperationDTO implements Serializable {

    private Long id;
    @NotNull
    private Long quantite;
    @NotNull
    private Long pU;
    private Long montantLigneOperation;
    private String designation;

    public LigneOperationDTO() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuantite() {
        return quantite;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public Long getpU() {
        return pU;
    }

    public void setpU(Long pU) {
        this.pU = pU;
    }

    public Long getMontantLigneOperation() {
        return montantLigneOperation;
    }

    public void setMontantLigneOperation(Long montantLigneOperation) {
        this.montantLigneOperation = montantLigneOperation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LigneOperationDTO ligneOperationDTO = (LigneOperationDTO) o;
        if (ligneOperationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ligneOperationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LigneOperationDTO{" +
            "id=" + getId() +
            ", quantite=" + getQuantite() +
            ", pU=" + getpU() +
            ", montantLigneOperation=" + getMontantLigneOperation() +
            ", produitId=" + getDesignation() +
            "}";
    }
}
