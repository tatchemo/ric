package com.urservices.myapp.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.urservices.myapp.domain.*;
import com.urservices.myapp.domain.enumeration.Etat;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Operation} entity.
 */
public class OperationDTO implements Serializable {

    private Long id;

    @NotNull
    private String numeroOperation;
    @NotNull
    private LocalDate dateOperation;
    @NotNull
    private Long montantTotal;
    private Boolean deleted;
    private Boolean returned;
    private Etat etat;
    private TypeOperation typeOperation;
    private Client client;
    private Employe employe;
    private List<LigneOperation> ligneOperations;
    private Bordereau bordereau;
    private Facture facture;
    private Operation operationRe;
    private Returns returns;
    private List<Produit> produits;
    private List<AllTracing> tracings;

    public  OperationDTO() {}

    public Operation getOperation() {
        Operation result = new Operation();
        if (id!=null) {
            result.setId(this.getId());
        }
        result.setNumeroOperation(numeroOperation);
        result.setDateOperation(dateOperation);
        result.setMontantTotal(montantTotal);
        result.setDeleted(deleted);
        result.setReturned(returned);
        result.setEtat(etat);
        result.setTypeOperation(this.getTypeOperation());
        result.setClient(this.getClient());
        result.setEmploye(this.getEmploye());
        return result;
    }

    public void setOperation(Operation operation) {
        this.setId(operation.getId());
        this.setNumeroOperation(operation.getNumeroOperation());
        this.setDateOperation(operation.getDateOperation());
        this.setMontantTotal(operation.getMontantTotal());
        this.setDeleted(operation.isDeleted());
        this.setReturned(operation.isReturned());
        this.setEtat(operation.getEtat());
        this.setTypeOperation(operation.getTypeOperation());
        this.setClient(operation.getClient());
        this.setEmploye(operation.getEmploye());
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNumeroOperation() { return numeroOperation; }
    public void setNumeroOperation(String numeroOperation) { this.numeroOperation = numeroOperation; }

    public LocalDate getDateOperation() {return dateOperation; }
    public void setDateOperation(LocalDate dateOperation) { this.dateOperation = dateOperation; }

    public Long getMontantTotal() { return montantTotal; }
    public void setMontantTotal(Long montantTotal) { this.montantTotal = montantTotal; }

    public Boolean isDeleted() { return deleted; }
    public void setDeleted(Boolean deleted) { this.deleted = deleted; }

    public Boolean isReturned() { return returned; }
    public void setReturned(Boolean returned) { this.returned = returned; }

    public Etat getEtat() { return etat; }
    public void setEtat(Etat etat) { this.etat = etat; }

    public TypeOperation getTypeOperation() { return typeOperation; }
    public void setTypeOperation(TypeOperation typeOperation) { this.typeOperation = typeOperation; }

    public Client getClient() { return client; }
    public void setClient(Client client) { this.client = client; }

    public Employe getEmploye() { return employe; }
    public void setEmploye(Employe employe) { this.employe = employe; }

    public List<LigneOperation> getLigneOperations() { return ligneOperations; }
    public void setLigneOperations(List<LigneOperation> ligneOperations) { this.ligneOperations = ligneOperations; }

    public Bordereau getBordereau() { return bordereau; }
    public void setBordereau(Bordereau bordereau) { this.bordereau = bordereau; }

    public Facture getFacture() { return facture; }
    public void setFacture(Facture facture) { this.facture = facture; }

    public Operation getOperationRe() { return operationRe; }
    public void setOperationRe(Operation operationRe) { this.operationRe = operationRe; }

    public Returns getReturns() { return returns; }
    public void setReturns(Returns returns) { this.returns = returns; }

    public List<Produit> getProduits() { return produits; }
    public void setProduits(List<Produit> produits) { this.produits = produits; }

    public List<AllTracing> getTracings() { return tracings; }
    public void setTracings(List<AllTracing> tracings) { this.tracings = tracings; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OperationDTO operationDTO = (OperationDTO) o;
        if (operationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), operationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OperationDTO{" +
            "id=" + getId() +
            ", numeroOperation='" + getNumeroOperation() + "'" +
            ", dateOperation='" + getDateOperation() + "'" +
            ", montantTotal=" + getMontantTotal() +
            ", deleted='" + isDeleted() + "'" +
            ", returned='" + isReturned() + "'" +
            ", etat='" + getEtat() + "'" +
            ", typeOperation=" + getTypeOperation() +
            ", client=" + getClient() +
            ", employe=" + getEmploye() +
            "}";
    }
}
