package com.urservices.myapp.service.dto;
import com.urservices.myapp.domain.*;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Paiement} entity.
 */
public class PaiementDTO extends AbstractAuditingEntity implements Serializable {

    private Long id;

    @NotNull
    private LocalDate datePaiement;

    @NotNull
    @Min(value = 0L)
    private Long montantPaiement;
    private Facture facture;
    private Client client;
    private Employe employe;
    private AllTracing tracing;

    public PaiementDTO() {}

    public void setPaiement(Paiement paiement) {
        this.setDatePaiement(paiement.getDatePaiement());
        this.setMontantPaiement(paiement.getMontantPaiement());
        this.setFacture(paiement.getFacture());
        this.setClient(paiement.getClient());
        this.setEmploye(paiement.getEmploye());
    }

    public Paiement getPaiement() {
        Paiement result = new Paiement();
        if (id!=null) {
            result.setId(this.getId());
        }
        result.setDatePaiement(datePaiement);
        result.setMontantPaiement(montantPaiement);
        result.setFacture(this.getFacture());
        result.setClient(this.getClient());
        result.setEmploye(this.getEmploye());
        return result;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDatePaiement() {
        return datePaiement;
    }
    public void setDatePaiement(LocalDate datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Long getMontantPaiement() {
        return montantPaiement;
    }
    public void setMontantPaiement(Long montantPaiement) {
        this.montantPaiement = montantPaiement;
    }

    public Facture getFacture() {
        return facture;
    }
    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    public Employe getEmploye() {
        return employe;
    }
    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public AllTracing getTracing() { return tracing; }
    public void setTracing(AllTracing tracing) { this.tracing = tracing; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaiementDTO paiementDTO = (PaiementDTO) o;
        if (paiementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), paiementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PaiementDTO{" +
            "id=" + getId() +
            ", datePaiement=" + getDatePaiement() +
            ", montantPaiement=" + getMontantPaiement() +
            ", facture=" + getFacture() +
            ", client=" + getClient() +
            ", employe=" + getEmploye() +
            "}";
    }
}
