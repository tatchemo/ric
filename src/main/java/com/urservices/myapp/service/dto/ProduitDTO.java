package com.urservices.myapp.service.dto;
import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.domain.Categorie;
import com.urservices.myapp.domain.LigneOperation;
import com.urservices.myapp.domain.Produit;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Produit} entity.
 */
public class ProduitDTO implements Serializable {

    private Long id;

    @NotNull
    private String designation;

    @NotNull
    private Long quantiteStock;

    private Long quantiteMinimale;

    private Categorie categorie;

    private List<LigneOperation> ligneOperations;

    private AllTracing tracing;

    public ProduitDTO() {}

    public void setProduit(Produit produit) {
        this.setId(produit.getId());
        this.setDesignation(produit.getDesignation());
        this.setQuantiteMinimale(produit.getQuantiteMinimale());
        this.setQuantiteStock(produit.getQuantiteStock());
        this.setCategorie(produit.getCategorie());
    }

    public Produit getProduit() {
        Produit result = new Produit();
        if (id != null) {
            result.setId(this.getId());
        }
        result.setDesignation(designation);
        result.setCategorie(this.getCategorie());
        result.setQuantiteMinimale(quantiteMinimale);
        result.setQuantiteStock(quantiteStock);
        return result;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantiteStock() {
        return quantiteStock;
    }
    public void setQuantiteStock(Long quantiteStock) {
        this.quantiteStock = quantiteStock;
    }

    public Long getQuantiteMinimale() {
        return quantiteMinimale;
    }
    public void setQuantiteMinimale(Long quantiteMinimale) {
        this.quantiteMinimale = quantiteMinimale;
    }

    public Categorie getCategorie() {
        return categorie;
    }
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public List<LigneOperation> getLigneOperations() { return ligneOperations; }
    public void setLigneOperations(List<LigneOperation> ligneOperations) { this.ligneOperations = ligneOperations; }

    public AllTracing getTracing() { return tracing; }
    public void setTracing(AllTracing tracing) { this.tracing = tracing; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProduitDTO produitDTO = (ProduitDTO) o;
        if (produitDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), produitDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProduitDTO{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", quantiteStock=" + getQuantiteStock() +
            ", quantiteMinimale=" + getQuantiteMinimale() +
            ", categorie=" + getCategorie() +
            "}";
    }
}
