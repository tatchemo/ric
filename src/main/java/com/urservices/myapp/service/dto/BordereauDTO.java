package com.urservices.myapp.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.urservices.myapp.domain.*;
import com.urservices.myapp.domain.enumeration.TypeBordereau;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Bordereau} entity.
 */
public class BordereauDTO implements Serializable {

    private Long id;
    private String numeroBordereau;

    @NotNull
    private LocalDate dateBordereau;

    private TypeBordereau type;
    private Operation operation;
    private Client client;
    private Employe employe;
    private List<LigneOperation> ligneOperations;
    private List<LigneOpSimpleDTO> ligneOpSimpleDTOS;

    public BordereauDTO() {}

    public void setBordereau(Bordereau bordereau) {
        this.setId(bordereau.getId());
        this.setNumeroBordereau(bordereau.getNumeroBordereau());
        this.setDateBordereau(bordereau.getDateBordereau());
        this.setClient(bordereau.getClient());
        this.setType(bordereau.getType());
        this.setOperation(bordereau.getOperation());
        this.setEmploye(bordereau.getEmploye());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroBordereau() {
        return numeroBordereau;
    }
    public void setNumeroBordereau(String numeroBordereau) {
        this.numeroBordereau = numeroBordereau;
    }

    public LocalDate getDateBordereau() {
        return dateBordereau;
    }
    public void setDateBordereau(LocalDate dateBordereau) {
        this.dateBordereau = dateBordereau;
    }

    public TypeBordereau getType() {
        return type;
    }
    public void setType(TypeBordereau type) {
        this.type = type;
    }

    public Operation getOperation() {
        return operation;
    }
    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    public Employe getEmploye() {
        return employe;
    }
    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public List<LigneOperation> getLigneOperations() { return ligneOperations; }
    public void setLigneOperations(List<LigneOperation> ligneOperations) { this.ligneOperations = ligneOperations; }

    public List<LigneOpSimpleDTO> getLigneOpSimpleDTOS() { return ligneOpSimpleDTOS; }

    public void setLigneOpSimpleDTOS(List<LigneOpSimpleDTO> ligneOpSimpleDTOS) { this.ligneOpSimpleDTOS = ligneOpSimpleDTOS; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BordereauDTO bordereauDTO = (BordereauDTO) o;
        if (bordereauDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bordereauDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BordereauDTO{" +
            "id=" + getId() +
            ", numeroBordereau='" + getNumeroBordereau() + "'" +
            ", dateBordereau='" + getDateBordereau() + "'" +
            ", type='" + getType() + "'" +
            ", operation=" + getOperation() +
            ", client=" + getClient() +
            ", employe=" + getEmploye() +
            "}";
    }
}
