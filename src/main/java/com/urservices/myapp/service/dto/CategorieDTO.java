package com.urservices.myapp.service.dto;
import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.domain.Categorie;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Categorie} entity.
 */
public class CategorieDTO implements Serializable {

    private Long id;

    @NotNull
    private String libelle;

    private AllTracing tracing;

    public CategorieDTO() {}

    public Categorie getCategorie() {
        Categorie result = new Categorie();
        if (id!=null) {
            result.setId(this.getId());
        }
        result.setLibelle(libelle);
        return result;
    }

    public void setCategorie(Categorie categorie) {
        this.setId(categorie.getId());
        this.setLibelle(categorie.getLibelle());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public AllTracing getTracing() { return tracing; }

    public void setTracing(AllTracing tracing) { this.tracing = tracing; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CategorieDTO categorieDTO = (CategorieDTO) o;
        if (categorieDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), categorieDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CategorieDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
