package com.urservices.myapp.service.dto;
import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.domain.Client;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Client} entity.
 */
public class ClientDTO implements Serializable {

    private Long id;
    @NotNull
    private String nom;
    @NotNull
    private String prenom;
    private Long telephone;
    private Boolean deleted;
    private Boolean fournisseur;
    private AllTracing tracing;

    public ClientDTO() {}

    public void setClient(Client client) {
        this.setId(client.getId());
        this.setNom(client.getNom());
        this.setPrenom(client.getPrenom());
        this.setFournisseur(client.isFournisseur());
        this.setTelephone(client.getTelephone());
        this.setDeleted(client.isDeleted());
    }

    public Client getClient() {
        Client result = new Client();
        if (id!=null) {
            result.setId(this.getId());
        }
        result.setNom(nom);
        result.setPrenom(prenom);
        result.setFournisseur(fournisseur);
        result.setTelephone(telephone);
        result.setDeleted(deleted);
        return result;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNom() { return nom; }
    public void setNom(String nom) { this.nom = nom; }

    public String getPrenom() { return prenom; }
    public void setPrenom(String prenom) { this.prenom = prenom; }

    public Long getTelephone() { return telephone; }
    public void setTelephone(Long telephone) { this.telephone = telephone; }

    public Boolean isDeleted() { return deleted; }
    public void setDeleted(Boolean deleted) { this.deleted = deleted; }

    public Boolean isFournisseur() { return fournisseur; }
    public void setFournisseur(Boolean fournisseur) { this.fournisseur = fournisseur; }

    public AllTracing getTracing() { return tracing; }
    public void setTracing(AllTracing tracing) { this.tracing = tracing; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientDTO clientDTO = (ClientDTO) o;
        if (clientDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", telephone=" + getTelephone() +
            ", fournisseur='" + isFournisseur() + "'" +
            ", deleted='" + isDeleted() + "'" +
            "}";
    }
}
