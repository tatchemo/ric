package com.urservices.myapp.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.urservices.myapp.domain.*;
import com.urservices.myapp.domain.enumeration.Etat;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Facture} entity.
 */
public class FactureDTO implements Serializable {

    private Long id;

    private String numeroFacture;

    @NotNull
    private LocalDate dateFacturation;

    @NotNull
    @Min(value = 0L)
    private Long montantFacure;

    @Min(value = 0L)
    private Long netAPayer;
    private Etat etat;
    private Operation operation;
    private Employe employe;
    private List<Paiement> paiements;
    private List<LigneOperation> ligneOperations;
    private List<LigneOpSimpleDTO> ligneOpSimpleDTOS;

    public FactureDTO() {}

    public void setFacture(Facture facture) {
        this.setId(facture.getId());
        this.setNumeroFacture(facture.getNumeroFacture());
        this.setMontantFacure(facture.getMontantFacure());
        this.setDateFacturation(facture.getDateFacturation());
        this.setEtat(facture.getEtat());
        this.setNetAPayer(facture.getNetAPayer());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroFacture() {
        return numeroFacture;
    }
    public void setNumeroFacture(String numeroFacture) {
        this.numeroFacture = numeroFacture;
    }

    public LocalDate getDateFacturation() {
        return dateFacturation;
    }
    public void setDateFacturation(LocalDate dateFacturation) {
        this.dateFacturation = dateFacturation;
    }

    public Long getMontantFacure() {
        return montantFacure;
    }
    public void setMontantFacure(Long montantFacure) {
        this.montantFacure = montantFacure;
    }

    public Long getNetAPayer() {
        return netAPayer;
    }
    public void setNetAPayer(Long netAPayer) { this.netAPayer = netAPayer; }

    public Etat getEtat() {
        return etat;
    }
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Operation getOperation() {
        return operation;
    }
    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public List<Paiement> getPaiements() { return paiements; }
    public void setPaiements(List<Paiement> paiements) { this.paiements = paiements; }

    public List<LigneOperation> getLigneOperations() { return ligneOperations; }
    public void setLigneOperations(List<LigneOperation> ligneOperations) { this.ligneOperations = ligneOperations; }

    public List<LigneOpSimpleDTO> getLigneOpSimpleDTOS() { return ligneOpSimpleDTOS; }
    public void setLigneOpSimpleDTOS(List<LigneOpSimpleDTO> ligneOpSimpleDTOS) { this.ligneOpSimpleDTOS = ligneOpSimpleDTOS; }

    public Employe getEmploye() { return employe; }

    public void setEmploye(Employe employe) { this.employe = employe; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FactureDTO factureDTO = (FactureDTO) o;
        if (factureDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), factureDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FactureDTO{" +
            "id=" + getId() +
            ", numeroFacture='" + getNumeroFacture() + "'" +
            ", dateFacturation='" + getDateFacturation() + "'" +
            ", montantFacure=" + getMontantFacure() +
            ", netAPayer=" + getNetAPayer() +
            ", etat='" + getEtat() + "'" +
            ", operation=" + getOperation() +
            "}";
    }
}
