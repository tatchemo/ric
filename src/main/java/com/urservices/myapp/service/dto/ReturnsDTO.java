package com.urservices.myapp.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.urservices.myapp.domain.Returns} entity.
 */
public class ReturnsDTO implements Serializable {

    private Long id;

    @NotNull
    private String numeroRetour;

    @NotNull
    private String motif;

    private String commentaire;


    private Long operationId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroRetour() {
        return numeroRetour;
    }

    public void setNumeroRetour(String numeroRetour) {
        this.numeroRetour = numeroRetour;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReturnsDTO returnsDTO = (ReturnsDTO) o;
        if (returnsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), returnsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReturnsDTO{" +
            "id=" + getId() +
            ", numeroRetour='" + getNumeroRetour() + "'" +
            ", motif='" + getMotif() + "'" +
            ", commentaire='" + getCommentaire() + "'" +
            ", operationId=" + getOperationId() +
            "}";
    }
}
