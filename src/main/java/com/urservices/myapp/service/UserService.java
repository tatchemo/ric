package com.urservices.myapp.service;

import com.urservices.myapp.config.Constants;
import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.domain.Authority;
import com.urservices.myapp.domain.Employe;
import com.urservices.myapp.domain.User;
import com.urservices.myapp.repository.*;
import com.urservices.myapp.security.AuthoritiesConstants;
import com.urservices.myapp.security.SecurityUtils;
import com.urservices.myapp.service.dto.UserDTO;

import io.github.jhipster.security.RandomUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final EmployeRepository employeRepository;
    private final PasswordEncoder passwordEncoder;
    private final PersistentTokenRepository persistentTokenRepository;
    private final AuthorityRepository authorityRepository;
    private final CacheManager cacheManager;
    private final AllTracingRepository allTracingRepository;

    public UserService(
        UserRepository userRepository,
        EmployeRepository employeRepository,
        PasswordEncoder passwordEncoder,
        PersistentTokenRepository persistentTokenRepository,
        AuthorityRepository authorityRepository,
        CacheManager cacheManager,
        AllTracingRepository allTracingRepository
    ) {
        this.userRepository = userRepository;
        this.employeRepository = employeRepository;
        this.passwordEncoder = passwordEncoder;
        this.persistentTokenRepository = persistentTokenRepository;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.allTracingRepository = allTracingRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                this.clearUserCaches(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                this.clearUserCaches(user);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                this.clearUserCaches(user);
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {
        userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new UsernameAlreadyUsedException();
            }
        });
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            newUser.setEmail(userDTO.getEmail().toLowerCase());
        }
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser){
        if (existingUser.getActivated()) {
             return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public UserDTO createUser(UserDTO userDTO) {
        User user = new User();
        Employe employe = new Employe();
        UserDTO result = new UserDTO();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
//        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        employe.setNom(userDTO.getFirstName());
        employe.setPrenom(userDTO.getLastName());
        employe.setTelephone(userDTO.getTelephone());
        employe.setAgence(userDTO.getAgence());
        employe.setPoste(userDTO.getPoste());
        employe.setCni(userDTO.getCni());
        employe.setDeleted(userDTO.getDeleted());
        employe.setUser(user);
        user = userRepository.save(user);
        AllTracing tracing = new AllTracing();
        tracing.setLogDate(Instant.now());
        /* Tracing creation User */
        tracing.setEntityLogged("User");
        tracing.setLogGenerateBy(user.getCreatedBy());
        tracing.setLogDescription("Creation du compte d'utilisateur :  [" + user.getLogin() + "]");
        tracing = allTracingRepository.save(tracing);
        /* result adding */
        result.setUserDTO(user);
        result.setEmploye(employeRepository.save(employe));
        result.setTracing(tracing);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        log.debug("Created Information for Employe: {}", employe);
        return result;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName  last name of user.
     * @param email     email id of user.
     * @param langKey   language key.
     * @param imageUrl  image URL of user.
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                if (email != null) {
	                user.setEmail(email.toLowerCase());
                }
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        log.debug("Update Information for UserDTO: {}", userDTO);
        UserDTO result = new UserDTO();
        UserDTO oldUser = new UserDTO();
        User user = userRepository.getOne(userDTO.getId());
        System.out.println("****************************");
        System.out.println("Testing Already existing User value :: " + user);
        Employe employe = employeRepository.getEmployeOfUser(userDTO.getId());
        oldUser.setUserDTO(user);
        oldUser.setEmploye(employe);
        oldUser.setPoste(employe.getPoste());
        oldUser.setCni(employe.getCni());
        oldUser.setTelephone(employe.getTelephone());
        oldUser.setAgence(employe.getAgence());
//        oldUser.setPassword(user.getPassword());
        System.out.println("****************************");
        System.out.println("Testing OldUser value :: " + oldUser);
        String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(encryptedPassword);
        System.out.println("****************************");
        System.out.println("Testing new password value :: " + userDTO.getPassword());
        System.out.println("Testing encryptedPassword value :: " + user.getPassword());
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        user.setImageUrl(userDTO.getImageUrl());
        user.setActivated(userDTO.isActivated());
        user.setLangKey(userDTO.getLangKey());
        Set<Authority> managedAuthorities = user.getAuthorities();
        managedAuthorities.clear();
        userDTO.getAuthorities().stream()
            .map(authorityRepository::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .forEach(managedAuthorities::add);
        employe.setNom(userDTO.getFirstName());
        employe.setPrenom(userDTO.getLastName());
        employe.setTelephone(userDTO.getTelephone());
        employe.setAgence(userDTO.getAgence());
        employe.setPoste(userDTO.getPoste());
        employe.setCni(userDTO.getCni());
        employe.setDeleted(userDTO.getDeleted());
        employe.setUser(user);


        AllTracing tracing = new AllTracing();
        String p = "";
        boolean hasChange = false;
        tracing.setLogDate(Instant.now());

        /* tracing for User update */
        tracing.setEntityLogged("User");

        if (!oldUser.getEmploye().getNom().equals(employe.getNom())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Employe:::Nom : " + "[" + oldUser.getEmploye().getNom() + "] "+ "==> [" + employe.getNom() + "]";
            hasChange = true;
        }
        if (!oldUser.getEmploye().getPrenom().equals(employe.getPrenom())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Employe:::Prenom : " + "[" + oldUser.getEmploye().getPrenom() + "] "+ "==> [" + employe.getPrenom() + "]";
            hasChange = true;
        }
        if (!oldUser.getAgence().equals(employe.getAgence())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Employe:::Agence : " + "[" + oldUser.getEmploye().getAgence().getNom() + "] " + "==> [" + employe.getAgence().getNom() + "]";
            hasChange = true;
        }
        if (!oldUser.getPoste().equals(employe.getPoste())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Employe:::Agence : " + "[" + oldUser.getEmploye().getPoste().getLibelle() + "] " + "==> [" + employe.getPoste().getLibelle() + "]";
            hasChange = true;
        }
        if (!oldUser.getCni().equals(employe.getCni())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Employe:::Cni : " + "[" + oldUser.getEmploye().getCni() + "] " + "==> [" + employe.getCni() + "]";
            hasChange = true;
        }
        if (!oldUser.getTelephone().equals(employe.getTelephone())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Employe:::Telephone : " + "[" + oldUser.getEmploye().getTelephone() + "] " + "==> [" + employe.getTelephone() + "]";
            hasChange = true;
        }
        if (!oldUser.getUser().getAuthorities().equals(user.getAuthorities())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::Authorities : " + "[" + oldUser.getUser().getAuthorities().toString() + "] " + "==> [" + user.getAuthorities().toString() + "]";
            hasChange = true;
        }
        if (!oldUser.getUser().getLangKey().equals(user.getLangKey())) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::LangKey : " + "[" + oldUser.getUser().getLangKey() + "] " + "==> [" + user.getLangKey() + "]";
            hasChange = true;
        }
        if (oldUser.getUser().getActivated() != user.getActivated()) {
            if (p != "") { p = p + ", \n"; }
            p = p + "User::LangKey : " + "[" + oldUser.getUser().getActivated() + "] " + "==> [" + user.getActivated() + "]";
            hasChange = true;
        }
        user = userRepository.save(user);

        tracing.setLogGenerateBy(userRepository.getTheLastModifiedBy(user.getId()));
        if (hasChange) {
            tracing.setLogDescription("Mise a jour de l'operation [" + user.getLogin() + "] : \n" +
                ""+ p + ".");
            //* Tracing save **/
            tracing = allTracingRepository.save(tracing);
            //****/
        }
        result.setUserDTO(user);
        result.setTracing(tracing);
        System.out.println("****************************");
        System.out.println("Testing result.getUser() value :: " + result.getUser());
        result.setEmploye(employeRepository.save(employe));
        this.clearUserCaches(user);
        log.debug("Changed Information for User: {}", user);
        log.debug("Created Information for Employe: {}", employe);

        return Optional.of(result);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String currentEncryptedPassword = user.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(encryptedPassword);
                this.clearUserCaches(user);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<UserDTO> getUserWithAuthoritiesByLogin(String login) {
        UserDTO result = new UserDTO();
        User user = userRepository.findOneWithAuthoritiesByLoginUser(login);
        Employe employe = employeRepository.getEmployeOfUser(user.getId());
        result.setUserDTO(user);
        result.setEmploye(employe);
        result.setPoste(employe.getPoste());
        result.setCni(employe.getCni());
        result.setTelephone(employe.getTelephone());
        result.setAgence(employe.getAgence());
        return Optional.of(result);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(user -> {
                log.debug("Deleting not activated user {}", user.getLogin());
                userRepository.delete(user);
                this.clearUserCaches(user);
            });
    }

    /**
     * Gets a list of all the authorities.
     * @return a list of all the authorities.
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }


    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }
}
