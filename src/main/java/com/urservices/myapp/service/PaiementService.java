package com.urservices.myapp.service;

import com.urservices.myapp.domain.Paiement;
import com.urservices.myapp.service.dto.PaiementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.Paiement}.
 */
public interface PaiementService {

    /**
     * Save a paiement.
     *
     * @param paiement the entity to save.
     * @return the persisted entity.
     */
    Paiement save(Paiement paiement);

    PaiementDTO saveDTO(PaiementDTO paiementDTO);

    /**
     * Get all the paiements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Paiement> findAll(Pageable pageable);


    /**
     * Get the "id" paiement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Paiement> findOne(Long id);

    /**
     * Delete the "id" paiement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
