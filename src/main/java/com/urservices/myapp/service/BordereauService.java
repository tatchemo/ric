package com.urservices.myapp.service;

import com.urservices.myapp.domain.Bordereau;
import com.urservices.myapp.service.dto.BordereauDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.urservices.myapp.domain.Bordereau}.
 */
public interface BordereauService {

    /**
     * Save a bordereau.
     *
     * @param bordereau the entity to save.
     * @return the persisted entity.
     */
    Bordereau save(Bordereau bordereau);

    /**
     * Get all the bordereaus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Bordereau> findAll(Pageable pageable);


    /**
     * Get the "id" bordereau.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Bordereau> findOne(Long id);


    Optional<BordereauDTO> findOneWithLo(Long id);

    /**
     * Delete the "id" bordereau.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
