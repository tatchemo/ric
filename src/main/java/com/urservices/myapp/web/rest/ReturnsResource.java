package com.urservices.myapp.web.rest;

import com.urservices.myapp.domain.Returns;
import com.urservices.myapp.service.ReturnsService;
import com.urservices.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.urservices.myapp.domain.Returns}.
 */
@RestController
@RequestMapping("/api")
public class ReturnsResource {

    private final Logger log = LoggerFactory.getLogger(ReturnsResource.class);

    private static final String ENTITY_NAME = "returns";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReturnsService returnsService;

    public ReturnsResource(ReturnsService returnsService) {
        this.returnsService = returnsService;
    }

    /**
     * {@code POST  /returns} : Create a new returns.
     *
     * @param returns the returns to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new returns, or with status {@code 400 (Bad Request)} if the returns has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/returns")
    public ResponseEntity<Returns> createReturns(@Valid @RequestBody Returns returns) throws URISyntaxException {
        log.debug("REST request to save Returns : {}", returns);
        if (returns.getId() != null) {
            throw new BadRequestAlertException("A new returns cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Returns result = returnsService.save(returns);
        return ResponseEntity.created(new URI("/api/returns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /returns} : Updates an existing returns.
     *
     * @param returns the returns to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated returns,
     * or with status {@code 400 (Bad Request)} if the returns is not valid,
     * or with status {@code 500 (Internal Server Error)} if the returns couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/returns")
    public ResponseEntity<Returns> updateReturns(@Valid @RequestBody Returns returns) throws URISyntaxException {
        log.debug("REST request to update Returns : {}", returns);
        if (returns.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Returns result = returnsService.save(returns);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, returns.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /returns} : get all the returns.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of returns in body.
     */
    @GetMapping("/returns")
    public ResponseEntity<List<Returns>> getAllReturns(Pageable pageable) {
        log.debug("REST request to get a page of Returns");
        Page<Returns> page = returnsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /returns/:id} : get the "id" returns.
     *
     * @param id the id of the returns to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the returns, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/returns/{id}")
    public ResponseEntity<Returns> getReturns(@PathVariable Long id) {
        log.debug("REST request to get Returns : {}", id);
        Optional<Returns> returns = returnsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(returns);
    }

    /**
     * {@code DELETE  /returns/:id} : delete the "id" returns.
     *
     * @param id the id of the returns to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/returns/{id}")
    public ResponseEntity<Void> deleteReturns(@PathVariable Long id) {
        log.debug("REST request to delete Returns : {}", id);
        returnsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
