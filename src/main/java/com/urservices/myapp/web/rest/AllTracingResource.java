package com.urservices.myapp.web.rest;

import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.service.AllTracingService;
import com.urservices.myapp.web.rest.errors.BadRequestAlertException;
import com.urservices.myapp.service.dto.AllTracingDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.urservices.myapp.domain.AllTracing}.
 */
@RestController
@RequestMapping("/api")
public class AllTracingResource {

    private final Logger log = LoggerFactory.getLogger(AllTracingResource.class);

    private static final String ENTITY_NAME = "allTracing";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AllTracingService allTracingService;

    public AllTracingResource(AllTracingService allTracingService) {
        this.allTracingService = allTracingService;
    }

    /**
     * {@code POST  /all-tracings} : Create a new allTracing.
     *
     * @param allTracing the allTracing to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new allTracing, or with status {@code 400 (Bad Request)} if the allTracing has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/all-tracings")
    public ResponseEntity<AllTracing> createAllTracing(@Valid @RequestBody AllTracing allTracing) throws URISyntaxException {
        log.debug("REST request to save AllTracing : {}", allTracing);
        if (allTracing.getId() != null) {
            throw new BadRequestAlertException("A new allTracing cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AllTracing result = allTracingService.save(allTracing);
        return ResponseEntity.created(new URI("/api/all-tracings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /all-tracings} : Updates an existing allTracing.
     *
     * @param allTracing the allTracing to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated allTracing,
     * or with status {@code 400 (Bad Request)} if the allTracing is not valid,
     * or with status {@code 500 (Internal Server Error)} if the allTracing couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/all-tracings")
    public ResponseEntity<AllTracing> updateAllTracing(@Valid @RequestBody AllTracing allTracing) throws URISyntaxException {
        log.debug("REST request to update AllTracing : {}", allTracing);
        if (allTracing.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AllTracing result = allTracingService.save(allTracing);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, allTracing.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /all-tracings} : get all the allTracings.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of allTracings in body.
     */
    @GetMapping("/all-tracings")
    public ResponseEntity<List<AllTracing>> getAllAllTracings(Pageable pageable) {
        log.debug("REST request to get a page of AllTracings");
        Page<AllTracing> page = allTracingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /all-tracings/:id} : get the "id" allTracing.
     *
     * @param id the id of the allTracing to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the allTracing, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/all-tracings/{id}")
    public ResponseEntity<AllTracing> getAllTracing(@PathVariable Long id) {
        log.debug("REST request to get AllTracing : {}", id);
        Optional<AllTracing> allTracing = allTracingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(allTracing);
    }

    /**
     * {@code DELETE  /all-tracings/:id} : delete the "id" allTracing.
     *
     * @param id the id of the allTracing to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/all-tracings/{id}")
    public ResponseEntity<Void> deleteAllTracing(@PathVariable Long id) {
        log.debug("REST request to delete AllTracing : {}", id);
        allTracingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
