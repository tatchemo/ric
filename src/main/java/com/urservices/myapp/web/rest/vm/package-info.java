/**
 * View Models used by Spring MVC REST controllers.
 */
package com.urservices.myapp.web.rest.vm;
