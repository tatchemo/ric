import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { LANGUAGES } from 'app/core/language/language.constants';
import { User } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IPoste } from 'app/shared/model/poste.model';
import { IAgence } from 'app/shared/model/agence.model';
import { map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { PosteService } from 'app/entities/poste/poste.service';
import { AgenceService } from 'app/entities/agence/agence.service';
import { EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from 'app/shared/constants/error.constants';

type SelectableEntity = IPoste | IAgence;

@Component({
  selector: 'jhi-user-mgmt-update',
  templateUrl: './user-management-update.component.html'
})
export class UserManagementUpdateComponent implements OnInit {
  user!: User;
  languages = LANGUAGES;
  authorities: string[] = [];
  authoritiesView: any[] = [];
  // authBtnChooseState: string[] = [];
  authBtnChooseState = {
    admin: 'no',
    user: 'no',
    sell: 'no',
    appro: 'no',
    return: 'no',
    edite: 'no',
    add: 'no',
    delete: 'no',
    paie: 'no',
    facture: 'no',
    sup: 'no'
  };
  postes: IPoste[] = [];
  agences: IAgence[] = [];
  isSaving = false;
  doNotMatch = false;
  error = false;
  errorEmailExists = false;
  errorUserExists = false;
  success = false;

  editForm = this.fb.group({
    id: [],
    login: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('^[_.@A-Za-z0-9-]*')]],
    password: ['', [Validators.required, Validators.minLength(5), Validators.pattern('^[_.@A-Za-z0-9-]*')]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    firstName: ['', [Validators.maxLength(50)]],
    lastName: ['', [Validators.maxLength(50)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    telephone: [],
    cni: ['', Validators.required],
    deleted: [],
    poste: ['', Validators.required],
    agence: ['', Validators.required],
    activated: [],
    langKey: ['', Validators.required],
    authorities: ['', Validators.required]
  });

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    protected posteService: PosteService,
    protected agenceService: AgenceService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(({ user }) => {
      if (user) {
        this.user = user;
        if (this.user.id === undefined) {
          this.user.activated = true;
        }
        this.updateForm(user);
      }
    });

    this.userService.authorities().subscribe(authorities => {
      this.authorities = authorities;
    });
    this.posteService
      .query()
      .pipe(
        map((res: HttpResponse<IPoste[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IPoste[]) => (this.postes = resBody));

    this.agenceService
      .query()
      .pipe(
        map((res: HttpResponse<IAgence[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IAgence[]) => (this.agences = resBody));
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    this.doNotMatch = false;
    this.error = false;
    this.errorEmailExists = false;
    this.errorUserExists = false;

    const password = this.editForm.get(['password'])!.value;
    if (password !== this.editForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
    } else {
      this.updateUser(this.user);
      if (this.user.id !== undefined) {
        this.userService.update(this.user).subscribe(
          () => this.onSaveSuccess(),
          response => this.processError(response)
        );
      } else {
        this.userService.create(this.user).subscribe(
          () => this.onSaveSuccess(),
          response => this.processError(response)
        );
      }
    }
  }

  private updateForm(user: User): void {
    this.editForm.patchValue({
      id: user.id,
      login: user.login,
      password: user.password,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      telephone: user.telephone,
      cni: user.cni,
      deleted: user.deleted = false,
      poste: user.poste,
      agence: user.agence,
      activated: user.activated,
      langKey: user.langKey,
      authorities: user.authorities
    });
  }

  private updateUser(user: User): void {
    user.login = this.editForm.get(['login'])!.value;
    user.password = this.editForm.get(['password'])!.value;
    user.firstName = this.editForm.get(['firstName'])!.value;
    user.lastName = this.editForm.get(['lastName'])!.value;
    user.email = this.editForm.get(['email'])!.value;
    user.telephone = this.editForm.get(['telephone'])!.value;
    user.cni = this.editForm.get(['cni'])!.value;
    user.deleted = this.editForm.get(['deleted'])!.value;
    user.poste = this.editForm.get(['poste'])!.value;
    user.agence = this.editForm.get(['agence'])!.value;
    user.activated = this.editForm.get(['activated'])!.value;
    user.langKey = this.editForm.get(['langKey'])!.value;
    user.authorities = this.editForm.get(['authorities'])!.value;
  }

  private onSaveSuccess(): void {
    this.isSaving = false;
    this.success = true;
    this.previousState();
  }

  private onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  private processError(response: HttpErrorResponse): void {
    if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
      this.errorUserExists = true;
    } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
      this.errorEmailExists = true;
    } else {
      this.error = true;
      this.isSaving = false;
    }
  }

  changeBtnAdminState(): void {
    if (this.authBtnChooseState.admin === 'no') {
      this.authBtnChooseState.admin = 'yes';
      if (this.authoritiesView.length > 0) {
        this.authoritiesView.push()[this.authoritiesView.length - 1] = this.authorities[0];
      } else {
        this.authoritiesView[0] = this.authorities[0];
      }
    } else {
      this.authBtnChooseState.admin = 'no';
      if (this.authoritiesView.length > 0) {
        this.authoritiesView.push()[this.authoritiesView.length - 1] = '';
      } else {
        this.authoritiesView = [];
      }
    }
  }
}
