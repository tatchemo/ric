import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RicSharedModule } from 'app/shared/shared.module';
import { OperationComponent } from './operation.component';
import { OperationDetailComponent } from './operation-detail.component';
import { OperationUpdateComponent } from './operation-update.component';
import { OperationDeleteDialogComponent } from './operation-delete-dialog.component';
import { operationRoute } from './operation.route';
import { OperationApprovionnementComponent } from 'app/entities/operation/Operation-Approvionnement/Operation-Approvionnement.component';
import { OperationSellComponent } from 'app/entities/operation/Operation-sell/Operation-sell.component';
import { MaterialModule } from 'app/entities/material-module';
import { MatNativeDateModule } from '@angular/material/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

@NgModule({
  imports: [RicSharedModule, MaterialModule, MatNativeDateModule, RouterModule.forChild(operationRoute)],
  declarations: [
    OperationComponent,
    OperationSellComponent,
    OperationDetailComponent,
    OperationUpdateComponent,
    OperationDeleteDialogComponent,
    OperationApprovionnementComponent
  ],
  entryComponents: [OperationDeleteDialogComponent],
  providers: [{ provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } }]
})
export class RicOperationModule {}
