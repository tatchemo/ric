import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from 'app/entities/operation/operation.service';
import { Etat } from 'app/shared/model/enumerations/etat.model';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-operation-detail',
  templateUrl: './operation-detail.component.html'
})
export class OperationDetailComponent implements OnInit {
  operation!: IOperation;
  isSaving = false;
  isEdition!: boolean;
  editForm!: FormGroup;
  ligneOperation!: FormArray;
  draft: any = 'Brouillon';
  editDraft!: any;

  constructor(protected activatedRoute: ActivatedRoute, protected operationService: OperationService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.editForm = this.fb.group({
      id: new FormControl(),
      numeroOperation: new FormControl(null),
      dateOperation: new FormControl(null, Validators.required),
      montantTotal: new FormControl(0),
      deleted: new FormControl(),
      returned: new FormControl(),
      etat: new FormControl(Etat.Brouillon),
      typeOperation: new FormControl(),
      client: new FormControl(),
      employe: new FormControl(),
      ligneOperations: this.fb.array([])
    });
    this.activatedRoute.data.subscribe(({ operation }) => {
      this.operation = operation;
      this.isEdition = true;
      this.editDraft = this.operation.etat + '';
      if (operation.ligneOperations.length !== 0) {
        this.addManyLigneOperation(operation.ligneOperations.length);
      }
      this.editForm.patchValue(Object.assign({}, this.operation));
    });
  }

  iniLigneOperation(): FormGroup {
    return this.fb.group({
      id: new FormControl(),
      quantite: new FormControl(1, Validators.required),
      pU: new FormControl(0, Validators.required),
      montantLigneOperation: new FormControl(0),
      deleted: new FormControl(false),
      produit: new FormControl(),
      operation: new FormControl()
    });
  }

  addLigneOperation(): void {
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    this.ligneOperation.push(this.iniLigneOperation());
  }

  addManyLigneOperation(size: number): void {
    for (let i = 0; i < size; i++) {
      this.addLigneOperation();
    }
  }

  save(): void {
    this.isSaving = true;
    this.editForm.patchValue({ etat: Etat.Terminer });
    this.subscribeToSaveResponse(this.operationService.updateDto(this.editForm.value));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOperation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  previousState(): void {
    window.history.back();
    window.history.forward();
  }
}
