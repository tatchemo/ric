import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from './operation.service';
import { ITypeOperation } from 'app/shared/model/type-operation.model';
import { TypeOperationService } from 'app/entities/type-operation/type-operation.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IEmploye } from 'app/shared/model/employe.model';
import { EmployeService } from 'app/entities/employe/employe.service';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IProduit } from 'app/shared/model/produit.model';
import { Etat } from 'app/shared/model/enumerations/etat.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

type SelectableEntity = ITypeOperation | IClient | IEmploye;

@Component({
  selector: 'jhi-operation-update',
  templateUrl: './operation-update.component.html',
  styleUrls: ['operation.scss']
})
export class OperationUpdateComponent implements OnInit {
  isSaving = false;
  isEdition!: boolean;
  typeoperations: ITypeOperation[] = [];

  clients: IClient[] = [];
  clientsClient: IClient[] = [];
  clientsFournisseur: IClient[] = [];
  ligneOperation!: FormArray;
  employes: IEmploye[] = [];
  produits: IProduit[] = [];
  produitsCanSell: IProduit[] = [];
  operation!: IOperation;
  deletedLigne: string[] = [];
  bsNiv1: number[] = [];
  montantLigneOperationI: number[] = [];
  qteLigneOperationI: number[] = [];
  mT!: number;

  dateOperationDp: any;
  disableTak!: ITypeOperation;
  editForm!: FormGroup;
  closeResult!: string;

  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];

  constructor(
    protected operationService: OperationService,
    protected typeOperationService: TypeOperationService,
    protected clientService: ClientService,
    protected employeService: EmployeService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.typeOperationService
      .query()
      .pipe(
        map((res: HttpResponse<ITypeOperation[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: ITypeOperation[]) => (this.typeoperations = resBody));

    this.clientService
      .queryFournisseur()
      .pipe(
        map((res: HttpResponse<IClient[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IClient[]) => (this.clientsFournisseur = resBody));

    this.clientService
      .queryClient()
      .pipe(
        map((res: HttpResponse<IClient[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IClient[]) => (this.clientsClient = resBody));

    this.employeService
      .query()
      .pipe(
        map((res: HttpResponse<IEmploye[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IEmploye[]) => (this.employes = resBody));

    this.produitService
      .query()
      .pipe(
        map((res: HttpResponse<IProduit[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IProduit[]) => (this.produits = resBody));

    this.produitService
      .queryCanSell()
      .pipe(
        map((res: HttpResponse<IProduit[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IProduit[]) => (this.produitsCanSell = resBody));

    this.editForm = this.fb.group({
      id: new FormControl(),
      numeroOperation: new FormControl(null),
      dateOperation: new FormControl(new Date().toISOString(), Validators.required),
      montantTotal: new FormControl(0),
      deleted: new FormControl(),
      returned: new FormControl(),
      etat: new FormControl(Etat.Brouillon),
      typeOperation: new FormControl(),
      client: new FormControl('', Validators.required),
      employe: new FormControl(''),
      ligneOperations: this.fb.array([])
    });

    this.activatedRoute.data.subscribe(({ operation }) => {
      this.operation = operation;
      if (this.operation.id !== null && this.operation.id !== undefined) {
        this.isEdition = true;
        if (operation.ligneOperations.length !== 0) {
          this.addManyLigneOperation(operation.ligneOperations.length);
          for (let i = 0; i < operation.ligneOperations.length; i++) {
            this.bsNiv1[i] = 0;
          }
        }
      } else {
        this.isEdition = false;
        this.mT = this.operation.montantTotal = 0;
        this.operation.etat = Etat.Brouillon;
        this.operation.numeroOperation = '0';
        if (this.produits.length !== 0) {
          this.addLigneOperation();
        }
      }
      // console.log('Testing operation value :=> ', this.operation);
      this.editForm.patchValue(Object.assign({}, this.operation));
    });
  }

  isAppro(): void {
    this.editForm.patchValue({ typeOperation: this.typeoperations[0] });
    this.disableTak = this.typeoperations[0];
  }

  isSell(): void {
    this.editForm.patchValue({ typeOperation: this.typeoperations[1] });
    this.disableTak = this.typeoperations[1];
  }

  iniLigneOperation(): FormGroup {
    return this.fb.group({
      id: new FormControl(),
      quantite: new FormControl(1, Validators.min(1)),
      pU: new FormControl(null, Validators.required),
      montantLigneOperation: new FormControl(0),
      deleted: new FormControl(false),
      produit: new FormControl(null, Validators.required),
      operation: new FormControl()
    });
  }

  addLigneOperation(): void {
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    this.ligneOperation.push(this.iniLigneOperation());
  }

  addManyLigneOperation(size: number): void {
    for (let i = 0; i < size; i++) {
      this.addLigneOperation();
    }
  }

  changeBsNiv1(i: number): void {
    if (this.bsNiv1[i] === 0) {
      this.bsNiv1[i] = 1;
    } else {
      this.bsNiv1[i] = 0;
    }
  }

  removeLigneOperationNoNull(i: number): void {
    this.ligneOperation.at(i).patchValue({ deleted: true });
    this.deletedLigne[i] = 'yes';
    this.bsNiv1[i] = 0;
    this.getMontantTotal();
  }

  removeLigneOperationNull(i: number): void {
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    this.ligneOperation.removeAt(i);
  }

  confirmLigne(i: number): void {
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    this.montantLigneOperationI[i] = this.ligneOperation.at(i).value.quantite * this.ligneOperation.at(i).value.pU;
    this.ligneOperation.at(i).patchValue({ montantLigneOperation: this.montantLigneOperationI[i] });
  }

  maxQuantityTest(i: number): void {
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    if (this.ligneOperation.at(i).value.produit.quantiteStock < this.ligneOperation.at(i).value.quantite) {
      // this.montantLigneOperationI[i] = this.ligneOperation.at(i).value.produit.quantiteStock;
      this.ligneOperation.at(i).patchValue({ quantite: this.ligneOperation.at(i).value.produit.quantiteStock });
    }
    this.minQuantityTest(i);
  }

  minQuantityTest(i: number): void {
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    if (this.ligneOperation.at(i).value.quantite < 1) {
      // this.montantLigneOperationI[i] = this.ligneOperation.at(i).value.produit.quantiteStock;
      this.ligneOperation.at(i).patchValue({ quantite: 1 });
    }
  }

  getMontantTotal(): void {
    this.mT = 0;
    this.ligneOperation = this.editForm.get(['ligneOperations']) as FormArray;
    for (let i = 0; i < this.ligneOperation.length; i++) {
      this.confirmLigne(i);
      if (this.ligneOperation.at(i).value.deleted === false) {
        this.mT = this.mT + this.ligneOperation.at(i).value.montantLigneOperation;
      }
    }
    this.editForm.patchValue({ montantTotal: this.mT, netAPayer: this.mT });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    this.getMontantTotal();
    if (this.editForm.value.id !== undefined && this.editForm.value.id !== null && this.editForm.value.id !== '') {
      this.subscribeToSaveResponse(this.operationService.updateDto(this.editForm.value));
    } else {
      this.subscribeToSaveResponse(this.operationService.create(this.editForm.value));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOperation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
  trackByDesignation(index: number, produitOption: IProduit): any {
    return produitOption.designation;
  }
}
