import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOperation, Operation } from 'app/shared/model/operation.model';
import { OperationService } from './operation.service';
import { OperationComponent } from './operation.component';
import { OperationDetailComponent } from './operation-detail.component';
import { OperationUpdateComponent } from './operation-update.component';
import { OperationApprovionnementComponent } from 'app/entities/operation/Operation-Approvionnement/Operation-Approvionnement.component';
import { OperationSellComponent } from 'app/entities/operation/Operation-sell/Operation-sell.component';

@Injectable({ providedIn: 'root' })
export class OperationResolve implements Resolve<IOperation> {
  constructor(private service: OperationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOperation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.findComplet(id).pipe(
        flatMap((operation: HttpResponse<Operation>) => {
          if (operation.body) {
            return of(operation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Operation());
  }
}

export const operationRoute: Routes = [
  {
    path: '',
    component: OperationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'ricApp.operation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'appro',
    component: OperationApprovionnementComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_APPRO'],
      defaultSort: 'id,asc',
      pageTitle: 'ricApp.operation.home.titleAppro'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'sell',
    component: OperationSellComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_SELL'],
      defaultSort: 'id,asc',
      pageTitle: 'ricApp.operation.home.titleSell'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OperationDetailComponent,
    resolve: {
      operation: OperationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_APPRO', 'ROLE_SELL'],
      pageTitle: 'ricApp.operation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OperationUpdateComponent,
    resolve: {
      operation: OperationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_APPRO', 'ROLE_SELL'],
      pageTitle: 'ricApp.operation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'newAppro',
    component: OperationApprovionnementComponent,
    resolve: {
      operation: OperationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_APPRO'],
      pageTitle: 'ricApp.operation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'newSell',
    component: OperationSellComponent,
    resolve: {
      operation: OperationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_SELL'],
      pageTitle: 'ricApp.operation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OperationUpdateComponent,
    resolve: {
      operation: OperationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_SUPP', 'ROLE_APPRO', 'ROLE_SELL'],
      pageTitle: 'ricApp.operation.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
