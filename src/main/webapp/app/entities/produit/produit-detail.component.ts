import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProduit } from 'app/shared/model/produit.model';
import { ILigneOperation } from 'app/shared/model/ligne-operation.model';

@Component({
  selector: 'jhi-produit-detail',
  templateUrl: './produit-detail.component.html'
})
export class ProduitDetailComponent implements OnInit {
  produit: IProduit | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ produit }) => {
      this.produit = produit;
    });
  }

  trackId(index: number, item: ILigneOperation): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  previousState(): void {
    window.history.back();
  }
}
