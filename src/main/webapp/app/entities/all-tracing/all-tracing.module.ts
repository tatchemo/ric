import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RicSharedModule } from 'app/shared/shared.module';
import { AllTracingComponent } from './all-tracing.component';
import { AllTracingDetailComponent } from './all-tracing-detail.component';
import { AllTracingUpdateComponent } from './all-tracing-update.component';
import { AllTracingDeleteDialogComponent } from './all-tracing-delete-dialog.component';
import { allTracingRoute } from './all-tracing.route';

@NgModule({
  imports: [RicSharedModule, RouterModule.forChild(allTracingRoute)],
  declarations: [AllTracingComponent, AllTracingDetailComponent, AllTracingUpdateComponent, AllTracingDeleteDialogComponent],
  entryComponents: [AllTracingDeleteDialogComponent]
})
export class RicAllTracingModule {}
