import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAllTracing, AllTracing } from 'app/shared/model/all-tracing.model';
import { AllTracingService } from './all-tracing.service';
import { AllTracingComponent } from './all-tracing.component';
import { AllTracingDetailComponent } from './all-tracing-detail.component';
import { AllTracingUpdateComponent } from './all-tracing-update.component';

@Injectable({ providedIn: 'root' })
export class AllTracingResolve implements Resolve<IAllTracing> {
  constructor(private service: AllTracingService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAllTracing> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((allTracing: HttpResponse<AllTracing>) => {
          if (allTracing.body) {
            return of(allTracing.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AllTracing());
  }
}

export const allTracingRoute: Routes = [
  {
    path: '',
    component: AllTracingComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'ricApp.allTracing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AllTracingDetailComponent,
    resolve: {
      allTracing: AllTracingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ricApp.allTracing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AllTracingUpdateComponent,
    resolve: {
      allTracing: AllTracingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ricApp.allTracing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AllTracingUpdateComponent,
    resolve: {
      allTracing: AllTracingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ricApp.allTracing.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
