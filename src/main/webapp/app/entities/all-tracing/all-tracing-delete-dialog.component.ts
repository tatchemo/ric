import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAllTracing } from 'app/shared/model/all-tracing.model';
import { AllTracingService } from './all-tracing.service';

@Component({
  templateUrl: './all-tracing-delete-dialog.component.html'
})
export class AllTracingDeleteDialogComponent {
  allTracing?: IAllTracing;

  constructor(
    protected allTracingService: AllTracingService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.allTracingService.delete(id).subscribe(() => {
      this.eventManager.broadcast('allTracingListModification');
      this.activeModal.close();
    });
  }
}
