import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAllTracing } from 'app/shared/model/all-tracing.model';

@Component({
  selector: 'jhi-all-tracing-detail',
  templateUrl: './all-tracing-detail.component.html'
})
export class AllTracingDetailComponent implements OnInit {
  allTracing: IAllTracing | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ allTracing }) => {
      this.allTracing = allTracing;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
