import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAllTracing, AllTracing } from 'app/shared/model/all-tracing.model';
import { AllTracingService } from './all-tracing.service';

@Component({
  selector: 'jhi-all-tracing-update',
  templateUrl: './all-tracing-update.component.html'
})
export class AllTracingUpdateComponent implements OnInit {
  isSaving = false;
  logDateDp: any;

  editForm = this.fb.group({
    id: [],
    logDescription: [null, [Validators.minLength(5), Validators.maxLength(30)]],
    entityLogged: [null, [Validators.required]],
    logDate: [null, [Validators.required]],
    logGenerateBy: [null, [Validators.required]]
  });

  constructor(protected allTracingService: AllTracingService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ allTracing }) => {
      this.updateForm(allTracing);
    });
  }

  updateForm(allTracing: IAllTracing): void {
    this.editForm.patchValue({
      id: allTracing.id,
      logDescription: allTracing.logDescription,
      entityLogged: allTracing.entityLogged,
      logDate: allTracing.logDate,
      logGenerateBy: allTracing.logGenerateBy
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const allTracing = this.createFromForm();
    if (allTracing.id !== undefined) {
      this.subscribeToSaveResponse(this.allTracingService.update(allTracing));
    } else {
      this.subscribeToSaveResponse(this.allTracingService.create(allTracing));
    }
  }

  private createFromForm(): IAllTracing {
    return {
      ...new AllTracing(),
      id: this.editForm.get(['id'])!.value,
      logDescription: this.editForm.get(['logDescription'])!.value,
      entityLogged: this.editForm.get(['entityLogged'])!.value,
      logDate: this.editForm.get(['logDate'])!.value,
      logGenerateBy: this.editForm.get(['logGenerateBy'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAllTracing>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
