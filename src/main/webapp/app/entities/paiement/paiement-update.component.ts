import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IPaiement, Paiement } from 'app/shared/model/paiement.model';
import { PaiementService } from './paiement.service';
import { IFacture } from 'app/shared/model/facture.model';
import { FactureService } from 'app/entities/facture/facture.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IEmploye } from 'app/shared/model/employe.model';
import { EmployeService } from 'app/entities/employe/employe.service';

type SelectableEntity = IFacture | IClient | IEmploye;

@Component({
  selector: 'jhi-paiement-update',
  templateUrl: './paiement-update.component.html'
})
export class PaiementUpdateComponent implements OnInit {
  isSaving = false;

  factures: IFacture[] = [];

  clients: IClient[] = [];

  employes: IEmploye[] = [];

  editForm = this.fb.group({
    id: [],
    datePaiement: [null, [Validators.required]],
    montantPaiement: [null, [Validators.required, Validators.min(0)]],
    facture: [],
    client: [],
    employe: []
  });

  constructor(
    protected paiementService: PaiementService,
    protected factureService: FactureService,
    protected clientService: ClientService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paiement }) => {
      this.updateForm(paiement);

      this.factureService
        .query()
        .pipe(
          map((res: HttpResponse<IFacture[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IFacture[]) => (this.factures = resBody));

      this.clientService
        .query()
        .pipe(
          map((res: HttpResponse<IClient[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IClient[]) => (this.clients = resBody));

      this.employeService
        .query()
        .pipe(
          map((res: HttpResponse<IEmploye[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IEmploye[]) => (this.employes = resBody));
    });
  }

  updateForm(paiement: IPaiement): void {
    this.editForm.patchValue({
      id: paiement.id,
      datePaiement: paiement.datePaiement,
      montantPaiement: paiement.montantPaiement,
      facture: paiement.facture,
      client: paiement.client,
      employe: paiement.employe
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const paiement = this.createFromForm();
    if (paiement.id !== undefined) {
      this.subscribeToSaveResponse(this.paiementService.update(paiement));
    } else {
      this.subscribeToSaveResponse(this.paiementService.create(paiement));
    }
  }

  private createFromForm(): IPaiement {
    return {
      ...new Paiement(),
      id: this.editForm.get(['id'])!.value,
      datePaiement: this.editForm.get(['datePaiement'])!.value,
      montantPaiement: this.editForm.get(['montantPaiement'])!.value,
      facture: this.editForm.get(['facture'])!.value,
      client: this.editForm.get(['client'])!.value,
      employe: this.editForm.get(['employe'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaiement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
