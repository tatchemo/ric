import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.RicClientModule)
      },
      {
        path: 'poste',
        loadChildren: () => import('./poste/poste.module').then(m => m.RicPosteModule)
      },
      {
        path: 'agence',
        loadChildren: () => import('./agence/agence.module').then(m => m.RicAgenceModule)
      },
      {
        path: 'employe',
        loadChildren: () => import('./employe/employe.module').then(m => m.RicEmployeModule)
      },
      {
        path: 'categorie',
        loadChildren: () => import('./categorie/categorie.module').then(m => m.RicCategorieModule)
      },
      {
        path: 'produit',
        loadChildren: () => import('./produit/produit.module').then(m => m.RicProduitModule)
      },
      {
        path: 'ligne-operation',
        loadChildren: () => import('./ligne-operation/ligne-operation.module').then(m => m.RicLigneOperationModule)
      },
      {
        path: 'type-operation',
        loadChildren: () => import('./type-operation/type-operation.module').then(m => m.RicTypeOperationModule)
      },
      {
        path: 'operation',
        loadChildren: () => import('./operation/operation.module').then(m => m.RicOperationModule)
      },
      {
        path: 'returns',
        loadChildren: () => import('./returns/returns.module').then(m => m.RicReturnsModule)
      },
      {
        path: 'paiement',
        loadChildren: () => import('./paiement/paiement.module').then(m => m.RicPaiementModule)
      },
      {
        path: 'facture',
        loadChildren: () => import('./facture/facture.module').then(m => m.RicFactureModule)
      },
      {
        path: 'bordereau',
        loadChildren: () => import('./bordereau/bordereau.module').then(m => m.RicBordereauModule)
      },
      {
        path: 'all-tracing',
        loadChildren: () => import('./all-tracing/all-tracing.module').then(m => m.RicAllTracingModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class RicEntityModule {}
