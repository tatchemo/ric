import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IReturns } from 'app/shared/model/returns.model';

@Component({
  selector: 'jhi-returns-detail',
  templateUrl: './returns-detail.component.html'
})
export class ReturnsDetailComponent implements OnInit {
  returns: IReturns | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ returns }) => {
      this.returns = returns;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
