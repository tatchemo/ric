import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IReturns, Returns } from 'app/shared/model/returns.model';
import { ReturnsService } from './returns.service';
import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from 'app/entities/operation/operation.service';

@Component({
  selector: 'jhi-returns-update',
  templateUrl: './returns-update.component.html'
})
export class ReturnsUpdateComponent implements OnInit {
  isSaving = false;

  operations: IOperation[] = [];

  editForm = this.fb.group({
    id: [],
    numeroRetour: [null, [Validators.required]],
    motif: [null, [Validators.required]],
    commentaire: [],
    operationId: []
  });

  constructor(
    protected returnsService: ReturnsService,
    protected operationService: OperationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ returns }) => {
      this.updateForm(returns);

      this.operationService
        .query()
        .pipe(
          map((res: HttpResponse<IOperation[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IOperation[]) => (this.operations = resBody));
    });
  }

  updateForm(returns: IReturns): void {
    this.editForm.patchValue({
      id: returns.id,
      numeroRetour: returns.numeroRetour,
      motif: returns.motif,
      commentaire: returns.commentaire,
      operationId: returns.operation
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const returns = this.createFromForm();
    if (returns.id !== undefined) {
      this.subscribeToSaveResponse(this.returnsService.update(returns));
    } else {
      this.subscribeToSaveResponse(this.returnsService.create(returns));
    }
  }

  private createFromForm(): IReturns {
    return {
      ...new Returns(),
      id: this.editForm.get(['id'])!.value,
      numeroRetour: this.editForm.get(['numeroRetour'])!.value,
      motif: this.editForm.get(['motif'])!.value,
      commentaire: this.editForm.get(['commentaire'])!.value,
      operation: this.editForm.get(['operationId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReturns>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOperation): any {
    return item.id;
  }
}
