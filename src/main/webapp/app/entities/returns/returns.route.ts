import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IReturns, Returns } from 'app/shared/model/returns.model';
import { ReturnsService } from './returns.service';
import { ReturnsComponent } from './returns.component';
import { ReturnsDetailComponent } from './returns-detail.component';
import { ReturnsUpdateComponent } from './returns-update.component';

@Injectable({ providedIn: 'root' })
export class ReturnsResolve implements Resolve<IReturns> {
  constructor(private service: ReturnsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IReturns> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((returns: HttpResponse<Returns>) => {
          if (returns.body) {
            return of(returns.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Returns());
  }
}

export const returnsRoute: Routes = [
  {
    path: '',
    component: ReturnsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'ricApp.returns.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ReturnsDetailComponent,
    resolve: {
      returns: ReturnsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ricApp.returns.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ReturnsUpdateComponent,
    resolve: {
      returns: ReturnsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ricApp.returns.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ReturnsUpdateComponent,
    resolve: {
      returns: ReturnsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ricApp.returns.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
