import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IReturns } from 'app/shared/model/returns.model';
import { ReturnsService } from './returns.service';

@Component({
  templateUrl: './returns-delete-dialog.component.html'
})
export class ReturnsDeleteDialogComponent {
  returns?: IReturns;

  constructor(protected returnsService: ReturnsService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.returnsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('returnsListModification');
      this.activeModal.close();
    });
  }
}
