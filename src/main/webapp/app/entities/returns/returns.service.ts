import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IReturns } from 'app/shared/model/returns.model';

type EntityResponseType = HttpResponse<IReturns>;
type EntityArrayResponseType = HttpResponse<IReturns[]>;

@Injectable({ providedIn: 'root' })
export class ReturnsService {
  public resourceUrl = SERVER_API_URL + 'api/returns';

  constructor(protected http: HttpClient) {}

  create(returns: IReturns): Observable<EntityResponseType> {
    return this.http.post<IReturns>(this.resourceUrl, returns, { observe: 'response' });
  }

  update(returns: IReturns): Observable<EntityResponseType> {
    return this.http.put<IReturns>(this.resourceUrl, returns, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReturns>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReturns[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
