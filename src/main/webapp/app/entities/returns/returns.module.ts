import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RicSharedModule } from 'app/shared/shared.module';
import { ReturnsComponent } from './returns.component';
import { ReturnsDetailComponent } from './returns-detail.component';
import { ReturnsUpdateComponent } from './returns-update.component';
import { ReturnsDeleteDialogComponent } from './returns-delete-dialog.component';
import { returnsRoute } from './returns.route';

@NgModule({
  imports: [RicSharedModule, RouterModule.forChild(returnsRoute)],
  declarations: [ReturnsComponent, ReturnsDetailComponent, ReturnsUpdateComponent, ReturnsDeleteDialogComponent],
  entryComponents: [ReturnsDeleteDialogComponent]
})
export class RicReturnsModule {}
