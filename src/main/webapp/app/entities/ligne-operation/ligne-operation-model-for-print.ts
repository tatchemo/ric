import { IProduit } from 'app/shared/model/produit.model';
import { IOperation } from 'app/shared/model/operation.model';

export interface ILigneOperationModelForPrint {
  id?: number;
  quantite?: number;
  pU?: number;
  montantLigneOperation?: number;
  designation?: string;
}

export class LigneOperationModelForPrint implements ILigneOperationModelForPrint {
  constructor(
    public id?: number,
    public quantite?: number,
    public pU?: number,
    public montantLigneOperation?: number,
    public designation?: string
  ) {}
}
