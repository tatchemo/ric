import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ILigneOperation, LigneOperation } from 'app/shared/model/ligne-operation.model';
import { LigneOperationService } from './ligne-operation.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from 'app/entities/operation/operation.service';

type SelectableEntity = IProduit | IOperation;

@Component({
  selector: 'jhi-ligne-operation-update',
  templateUrl: './ligne-operation-update.component.html'
})
export class LigneOperationUpdateComponent implements OnInit {
  isSaving = false;

  produits: IProduit[] = [];

  operations: IOperation[] = [];

  editForm = this.fb.group({
    id: [],
    quantite: [null, [Validators.required]],
    pU: [null, [Validators.required]],
    montantLigneOperation: [],
    deleted: [],
    produit: [],
    operation: []
  });

  constructor(
    protected ligneOperationService: LigneOperationService,
    protected produitService: ProduitService,
    protected operationService: OperationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ligneOperation }) => {
      this.updateForm(ligneOperation);

      this.produitService
        .query()
        .pipe(
          map((res: HttpResponse<IProduit[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IProduit[]) => (this.produits = resBody));

      this.operationService
        .query()
        .pipe(
          map((res: HttpResponse<IOperation[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IOperation[]) => (this.operations = resBody));
    });
  }

  updateForm(ligneOperation: ILigneOperation): void {
    this.editForm.patchValue({
      id: ligneOperation.id,
      quantite: ligneOperation.quantite,
      pU: ligneOperation.pU,
      montantLigneOperation: ligneOperation.montantLigneOperation,
      deleted: ligneOperation.deleted,
      produit: ligneOperation.produit,
      operation: ligneOperation.operation
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ligneOperation = this.createFromForm();
    if (ligneOperation.id !== undefined) {
      this.subscribeToSaveResponse(this.ligneOperationService.update(ligneOperation));
    } else {
      this.subscribeToSaveResponse(this.ligneOperationService.create(ligneOperation));
    }
  }

  private createFromForm(): ILigneOperation {
    return {
      ...new LigneOperation(),
      id: this.editForm.get(['id'])!.value,
      quantite: this.editForm.get(['quantite'])!.value,
      pU: this.editForm.get(['pU'])!.value,
      montantLigneOperation: this.editForm.get(['montantLigneOperation'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      produit: this.editForm.get(['produit'])!.value,
      operation: this.editForm.get(['operation'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILigneOperation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
