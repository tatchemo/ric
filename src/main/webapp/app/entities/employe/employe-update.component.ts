import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IEmploye, Employe } from 'app/shared/model/employe.model';
import { EmployeService } from './employe.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IPoste } from 'app/shared/model/poste.model';
import { PosteService } from 'app/entities/poste/poste.service';
import { IAgence } from 'app/shared/model/agence.model';
import { AgenceService } from 'app/entities/agence/agence.service';

type SelectableEntity = IUser | IPoste | IAgence;

@Component({
  selector: 'jhi-employe-update',
  templateUrl: './employe-update.component.html'
})
export class EmployeUpdateComponent implements OnInit {
  isSaving = false;

  users: IUser[] = [];

  postes: IPoste[] = [];

  agences: IAgence[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    prenom: [null, [Validators.required]],
    telephone: [null, [Validators.required]],
    cni: [null, [Validators.required]],
    deleted: [],
    user: [],
    poste: [],
    agence: []
  });

  constructor(
    protected employeService: EmployeService,
    protected userService: UserService,
    protected posteService: PosteService,
    protected agenceService: AgenceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ employe }) => {
      this.updateForm(employe);

      this.userService
        .query()
        .pipe(
          map((res: HttpResponse<IUser[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IUser[]) => (this.users = resBody));

      this.posteService
        .query()
        .pipe(
          map((res: HttpResponse<IPoste[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IPoste[]) => (this.postes = resBody));

      this.agenceService
        .query()
        .pipe(
          map((res: HttpResponse<IAgence[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IAgence[]) => (this.agences = resBody));
    });
  }

  updateForm(employe: IEmploye): void {
    this.editForm.patchValue({
      id: employe.id,
      nom: employe.nom,
      prenom: employe.prenom,
      telephone: employe.telephone,
      cni: employe.cni,
      deleted: employe.deleted,
      userId: employe.user,
      posteId: employe.poste,
      agenceId: employe.agence
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const employe = this.createFromForm();
    if (employe.id !== undefined) {
      this.subscribeToSaveResponse(this.employeService.update(employe));
    } else {
      this.subscribeToSaveResponse(this.employeService.create(employe));
    }
  }

  private createFromForm(): IEmploye {
    return {
      ...new Employe(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      cni: this.editForm.get(['cni'])!.value,
      deleted: this.editForm.get(['deleted'])!.value,
      user: this.editForm.get(['user'])!.value,
      poste: this.editForm.get(['poste'])!.value,
      agence: this.editForm.get(['agence'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmploye>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
