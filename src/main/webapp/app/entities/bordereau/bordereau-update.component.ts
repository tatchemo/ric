import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IBordereau, Bordereau } from 'app/shared/model/bordereau.model';
import { BordereauService } from './bordereau.service';
import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from 'app/entities/operation/operation.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IEmploye } from 'app/shared/model/employe.model';
import { EmployeService } from 'app/entities/employe/employe.service';

type SelectableEntity = IOperation | IClient | IEmploye;

@Component({
  selector: 'jhi-bordereau-update',
  templateUrl: './bordereau-update.component.html'
})
export class BordereauUpdateComponent implements OnInit {
  isSaving = false;

  operations: IOperation[] = [];

  clients: IClient[] = [];

  employes: IEmploye[] = [];
  dateBordereauDp: any;

  editForm = this.fb.group({
    id: [],
    numeroBordereau: [],
    dateBordereau: [null, [Validators.required]],
    type: [],
    operationId: [],
    clientId: [],
    employeId: []
  });

  constructor(
    protected bordereauService: BordereauService,
    protected operationService: OperationService,
    protected clientService: ClientService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bordereau }) => {
      this.updateForm(bordereau);

      this.operationService
        .query()
        .pipe(
          map((res: HttpResponse<IOperation[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IOperation[]) => (this.operations = resBody));

      this.clientService
        .query()
        .pipe(
          map((res: HttpResponse<IClient[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IClient[]) => (this.clients = resBody));

      this.employeService
        .query()
        .pipe(
          map((res: HttpResponse<IEmploye[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IEmploye[]) => (this.employes = resBody));
    });
  }

  updateForm(bordereau: IBordereau): void {
    this.editForm.patchValue({
      id: bordereau.id,
      numeroBordereau: bordereau.numeroBordereau,
      dateBordereau: bordereau.dateBordereau,
      type: bordereau.type,
      operationId: bordereau.operation,
      clientId: bordereau.client,
      employeId: bordereau.employe
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bordereau = this.createFromForm();
    if (bordereau.id !== undefined) {
      this.subscribeToSaveResponse(this.bordereauService.update(bordereau));
    } else {
      this.subscribeToSaveResponse(this.bordereauService.create(bordereau));
    }
  }

  private createFromForm(): IBordereau {
    return {
      ...new Bordereau(),
      id: this.editForm.get(['id'])!.value,
      numeroBordereau: this.editForm.get(['numeroBordereau'])!.value,
      dateBordereau: this.editForm.get(['dateBordereau'])!.value,
      type: this.editForm.get(['type'])!.value,
      operation: this.editForm.get(['operationId'])!.value,
      client: this.editForm.get(['clientId'])!.value,
      employe: this.editForm.get(['employeId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBordereau>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
