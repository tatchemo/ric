import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Bordereau, IBordereau } from 'app/shared/model/bordereau.model';
import { formatNumber } from '@angular/common';
import { ILigneOpSimpleDTOSModel, LigneOpSimpleDTOSModel } from 'app/shared/model/LigneOpSimpleDTOS.model';
import { IClient } from 'app/shared/model/client.model';
import { PrinterService } from 'app/entities/script.service';
import { TypeBordereau } from 'app/shared/model/enumerations/type-bordereau.model';

@Component({
  selector: 'jhi-bordereau-detail',
  templateUrl: './bordereau-detail.component.html'
})
export class BordereauDetailComponent extends PrinterService implements OnInit {
  bordereau: IBordereau | null = null;
  bordereauPrint = new Bordereau();
  ligneOpSimpleDTOS!: ILigneOpSimpleDTOSModel[];
  dateBordereau!: any;
  client!: IClient;

  constructor(protected activatedRoute: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bordereau }) => {
      this.bordereau = bordereau;
    });
    if (this.bordereau) {
      this.bordereauPrint = this.bordereau;
      if (this.bordereauPrint.dateBordereau !== undefined) {
        this.dateBordereau = this.bordereauPrint.dateBordereau.format('DD-MM-YYYY');
      }
      if (this.bordereauPrint.ligneOpSimpleDTOS !== undefined) {
        this.ligneOpSimpleDTOS = this.bordereauPrint.ligneOpSimpleDTOS;
      }
      if (this.bordereauPrint.client !== undefined) {
        this.client = this.bordereauPrint.client;
      }
    }
  }

  getDocumentDefinition(): any {
    // localStorage.setItem('facurePrint', JSON.stringify(this.facturePrint));
    return {
      content: [
        { text: 'R.I.C', decoration: 'underline', bold: true, fontSize: 23, alignment: 'center', margin: [0, 0, 20, 25] },
        {
          columns: [
            [
              this.getFournisseurAjust(this.changeTypeTypeBordereauUndefineToTypeBordereau(this.bordereauPrint.type) + ''),
              { text: 'Nom : ' + this.client.nom, margin: [1, 1, 1, 5] },
              { text: 'Prenom : ' + this.client.prenom, margin: [1, 1, 1, 5] },
              { text: 'Tel : ' + this.client.telephone, margin: [1, 1, 1, 5] },
              { text: 'Date : ' + this.dateBordereau, margin: [1, 1, 1, 15] },
              { text: ' ', margin: [1, 1, 1, 15] },
              {
                text: 'Bordereau ' + this.bordereauPrint.type + ' N* ' + this.bordereauPrint.numeroBordereau,
                color: 'black',
                fontSize: 21,
                bold: true,
                margin: [1, 1, 1, 5],
                decoration: 'underline'
              },
              { text: ' ', margin: [1, 1, 1, 15] }
            ]
          ]
        },
        this.getLigneOperationObject(this.ligneOpSimpleDTOS),
        { text: ' ', margin: [1, 1, 1, 25] },
        {
          columns: [
            { text: 'RECEPTIONEUR', bold: true, alignment: 'left', fontSize: 16, widths: ['*'] },
            { text: 'LIVREURE', bold: true, alignment: 'right', fontSize: 16, widths: ['*'] }
          ]
        }
      ],
      styles: {
        header: { fontSize: 18, bold: true, margin: [0, 20, 0, 10], decoration: 'underline' },
        name: { fontSize: 16, bold: true },
        jobTitle: { fontSize: 14, bold: true, italics: true },
        tableHeader: { alignment: 'center', bold: true },
        number: { alignment: 'right' }
      },
      info: { title: 'Bordereau ' + this.bordereauPrint.type + ' ' + this.bordereauPrint.numeroBordereau, author: 'RIC' }
    };
  }

  getFournisseurAjust(type: string): any {
    if (type !== 'Approvionnement') {
      return { text: 'CLIENT', color: 'black', bold: true, margin: [1, 1, 1, 5], decoration: 'underline' };
    } else {
      return { text: 'FOURNISSEUR', color: 'black', bold: true, margin: [1, 1, 1, 5], decoration: 'underline' };
    }
  }

  getLigneOperationObject(ligneOpSimpleDTOs: LigneOpSimpleDTOSModel[]): any {
    const dd = {
      table: {
        widths: [400, '*'],
        body: [
          [
            {
              text: 'Designation',
              style: 'tableHeader'
            },
            {
              text: 'Quantite',
              style: 'tableHeader'
            }
          ],
          ...ligneOpSimpleDTOs.map(ed => {
            return [
              {
                text: ed.designation
              },
              {
                text: formatNumber(this.changeTypeNumberUndefineToNumber(ed.quantite), 'fr-FR', '.0-5'),
                style: 'number'
              }
            ];
          })
        ]
      }
    };
    return dd;
  }

  generatePdf(): void {
    const documentDefinition = this.getDocumentDefinition();
    this.pdfMake.createPdf(documentDefinition).download('Bordereau_' + this.bordereauPrint.numeroBordereau + '.pdf');
  }

  changeTypeNumberUndefineToNumber(numb: number | undefined): number {
    let a = 0;
    if (numb !== undefined) {
      a = numb;
    }
    return a;
  }
  changeTypeTypeBordereauUndefineToTypeBordereau(typeBor: TypeBordereau | undefined): TypeBordereau {
    let a = 0;
    if (typeBor !== undefined) {
      a = typeBor;
    }
    return a;
  }

  previousState(): void {
    window.history.back();
  }
}
