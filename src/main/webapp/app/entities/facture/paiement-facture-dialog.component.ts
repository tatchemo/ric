import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { IFacture } from 'app/shared/model/facture.model';
import { FormBuilder, Validators } from '@angular/forms';
import { IPaiement, Paiement } from 'app/shared/model/paiement.model';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { PaiementService } from 'app/entities/paiement/paiement.service';
import { IClient } from 'app/shared/model/client.model';
import { map } from 'rxjs/operators';
import { IEmploye } from 'app/shared/model/employe.model';
import { EmployeService } from 'app/entities/employe/employe.service';
import { DatePipe } from '@angular/common';

type SelectableEntity = IClient | IEmploye;

@Component({
  templateUrl: './paiement-facture-dialog.component.html',
  providers: [DatePipe]
})
export class PaiementFactureDialogComponent implements OnInit {
  facture?: IFacture;
  client?: IClient;
  isSaving = false;
  employes: IEmploye[] = [];
  location!: Location;

  editForm = this.fb.group({
    id: [],
    datePaiement: ['', [Validators.required]],
    montantPaiement: [null, [Validators.required, Validators.min(0)]],
    facture: [],
    client: [],
    employe: []
  });

  constructor(
    public activeModal: NgbActiveModal,
    protected paiementService: PaiementService,
    protected employeService: EmployeService,
    protected eventManager: JhiEventManager,
    private datePipe: DatePipe,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.facture !== undefined) {
      this.editForm.patchValue({ facture: this.facture });
      if (this.facture.operation !== undefined) {
        this.client = this.facture.operation.client;
        this.editForm.patchValue({ client: this.client });
      }
      // this.editForm.patchValue({ datePaiement: this.datePipe.transform(this.currentDate, 'yyyy-MM-dd') });
    }
    this.employeService
      .query()
      .pipe(
        map((res: HttpResponse<IEmploye[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IEmploye[]) => (this.employes = resBody));
  }

  updateForm(paiement: IPaiement): void {
    this.editForm.patchValue({
      id: paiement.id,
      datePaiement: paiement.datePaiement,
      montantPaiement: paiement.montantPaiement,
      facture: paiement.facture,
      client: paiement.client,
      employe: paiement.employe
    });
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  previousState(): void {
    window.history.go(0);
  }

  confirmPaiement(): void {
    this.isSaving = true;
    const paiement = this.createFromForm();
    if (paiement.id !== undefined && paiement.id != null) {
      this.subscribeToSaveResponse(this.paiementService.update(paiement));
    } else {
      this.subscribeToSaveResponse(this.paiementService.create(paiement));
    }
    this.activeModal.close();
  }

  private createFromForm(): IPaiement {
    return {
      ...new Paiement(),
      id: this.editForm.get(['id'])!.value,
      datePaiement: this.editForm.get(['datePaiement'])!.value,
      montantPaiement: this.editForm.get(['montantPaiement'])!.value,
      facture: this.editForm.get(['facture'])!.value,
      client: this.editForm.get(['client'])!.value,
      employe: this.editForm.get(['employe'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaiement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
