import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Facture, IFacture } from 'app/shared/model/facture.model';
import { PrinterService } from '../script.service';
import { IOperation } from 'app/shared/model/operation.model';
import { IClient } from 'app/shared/model/client.model';
import { IProduit } from 'app/shared/model/produit.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaiementFactureDialogComponent } from 'app/entities/facture/paiement-facture-dialog.component';
import { IPaiement } from 'app/shared/model/paiement.model';
import { Etat } from 'app/shared/model/enumerations/etat.model';
import { FormArray, FormGroup } from '@angular/forms';
import { ILigneOpSimpleDTOSModel, LigneOpSimpleDTOSModel } from 'app/shared/model/LigneOpSimpleDTOS.model';
import { IEmploye } from 'app/shared/model/employe.model';
import { formatNumber } from '@angular/common';

type SelectableEntity = IPaiement | IFacture;

@Component({
  selector: 'jhi-facture-detail',
  templateUrl: './facture-detail.component.html'
})
export class FactureDetailComponent extends PrinterService implements OnInit {
  facture: IFacture | null = null;
  facturePrint = new Facture();
  dateFacture!: any;
  operation!: IOperation;
  client!: IClient;
  employe!: IEmploye;
  prod!: IProduit;
  editForm!: FormGroup;
  ligneOpSimpleDTOS!: ILigneOpSimpleDTOSModel[];
  ligneOperation!: FormArray;
  stateDraft: any = Etat.Brouillon;
  showHide!: boolean;
  showHideText: any = 'ricApp.paiement.detail.showPaiement';

  constructor(protected activatedRoute: ActivatedRoute, protected modalService: NgbModal) {
    super();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ facture }) => {
      this.facture = facture;
    });
    this.showHide = false;
    if (this.facture) {
      this.facturePrint = this.facture;
      if (this.facturePrint.dateFacturation !== undefined) {
        this.dateFacture = this.facturePrint.dateFacturation.format('DD-MM-YYYY');
      }
      if (this.facturePrint.ligneOpSimpleDTOS !== undefined) {
        this.ligneOpSimpleDTOS = this.facturePrint.ligneOpSimpleDTOS;
      }
      if (this.facturePrint.operation !== undefined) {
        this.operation = this.facturePrint.operation;
        if (this.operation.client !== undefined) {
          this.client = this.operation.client;
        }
        if (this.facturePrint.employe !== undefined) {
          this.employe = this.facturePrint.employe;
        }
      }
    }
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
  trackIdPaiment(index: number, item: IPaiement): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  paid(facture: IFacture): void {
    const modalRef = this.modalService.open(PaiementFactureDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.facture = facture;
    this.ngOnInit();
  }

  showHidePaiement(): void {
    if (!this.showHide) {
      this.showHide = true;
      this.showHideText = 'ricApp.paiement.detail.hidePaiement';
    } else {
      this.showHide = false;
      this.showHideText = 'ricApp.paiement.detail.showPaiement';
    }
  }

  /**
   *
   */

  getDocumentDefinition(): any {
    // localStorage.setItem('facurePrint', JSON.stringify(this.facturePrint));
    return {
      content: [
        { text: 'R.I.C', decoration: 'underline', bold: true, fontSize: 23, alignment: 'center', margin: [0, 0, 20, 25] },
        {
          columns: [
            [
              { text: 'CLIENT', color: 'black', bold: true, margin: [1, 1, 1, 5], decoration: 'underline' },
              { text: 'Nom : ' + this.client.nom, margin: [1, 1, 1, 5] },
              { text: 'Prenom : ' + this.client.prenom, margin: [1, 1, 1, 5] },
              { text: 'TELEPHONE : ' + this.client.telephone, margin: [1, 1, 1, 5] },
              { text: ' ', margin: [1, 1, 1, 15] },
              { text: 'VENDEUR', color: 'black', bold: true, margin: [1, 1, 1, 5], decoration: 'underline' },
              { text: 'Nom : ' + this.employe.nom + ' ' + this.employe.prenom, margin: [1, 1, 1, 5] },
              { text: ' ', margin: [1, 1, 1, 15] },
              {
                text: 'Facture Commercial N* ' + this.facturePrint.numeroFacture,
                color: 'black',
                fontSize: 23,
                bold: true,
                margin: [1, 1, 1, 5],
                decoration: 'underline'
              },
              { text: ' ', margin: [1, 1, 1, 15] }
            ]
          ]
        },
        this.getLigneOperationObject(this.ligneOpSimpleDTOS),
        {
          table: {
            widths: [360, '*'],
            body: [
              [
                { text: 'Montant Total', bold: true, fontSize: 18, margin: [1, 1, 7, 10] },
                {
                  text: formatNumber(this.changeTypeNumberUndefineToNumber(this.facturePrint.montantFacure), 'fr-FR', '4.0-5'),
                  bold: true,
                  fontSize: 18,
                  style: 'number',
                  margin: [1, 1, 7, 10]
                }
              ],
              [
                { text: 'Net A Payer', bold: true, fontSize: 18, margin: [1, 1, 7, 10] },
                {
                  text: formatNumber(this.changeTypeNumberUndefineToNumber(this.facturePrint.netAPayer), 'fr-FR', '4.0-5'),
                  bold: true,
                  fontSize: 18,
                  style: 'number',
                  margin: [1, 1, 7, 10]
                }
              ]
            ]
          }
        }
      ],
      styles: {
        header: { fontSize: 18, bold: true, margin: [0, 20, 0, 10], decoration: 'underline' },
        name: { fontSize: 16, bold: true },
        jobTitle: { fontSize: 14, bold: true, italics: true },
        tableHeader: { alignment: 'center', bold: true },
        number: { alignment: 'right' }
      },
      info: { title: 'Facture Commercial ' + this.facturePrint.numeroFacture, author: 'RIC' }
    };
  }

  getLigneOperationObject(ligneOpSimpleDTOs: LigneOpSimpleDTOSModel[]): any {
    const dd = {
      table: {
        widths: [300, 50, 60, '*'],
        body: [
          [
            { text: 'Designation', style: 'tableHeader' },
            { text: 'Quantite', style: 'tableHeader' },
            { text: 'PU', style: 'tableHeader' },
            { text: 'Montants', style: 'tableHeader' }
          ],
          ...ligneOpSimpleDTOs.map(ed => {
            return [
              { text: ed.designation },
              { text: formatNumber(this.changeTypeNumberUndefineToNumber(ed.quantite), 'fr-FR', '.0-5'), style: 'number' },
              { text: formatNumber(this.changeTypeNumberUndefineToNumber(ed.pU), 'fr-FR', '4.0-5'), style: 'number' },
              { text: formatNumber(this.changeTypeNumberUndefineToNumber(ed.montantLigneOperation), 'fr-FR', '4.0-5'), style: 'number' }
            ];
          })
        ]
      }
    };
    return dd;
  }

  generatePdf(): void {
    const documentDefinition = this.getDocumentDefinition();
    this.pdfMake.createPdf(documentDefinition).download('Facture_' + this.facturePrint.numeroFacture + '.pdf');
  }

  changeTypeNumberUndefineToNumber(numb: number | undefined): number {
    let a = 0;
    if (numb !== undefined) {
      a = numb;
    }
    return a;
  }

  previousState(): void {
    window.history.back();
  }
}
