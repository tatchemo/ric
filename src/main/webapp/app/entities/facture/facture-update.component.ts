import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IFacture, Facture } from 'app/shared/model/facture.model';
import { FactureService } from './facture.service';
import { IOperation } from 'app/shared/model/operation.model';
import { OperationService } from 'app/entities/operation/operation.service';

@Component({
  selector: 'jhi-facture-update',
  templateUrl: './facture-update.component.html'
})
export class FactureUpdateComponent implements OnInit {
  isSaving = false;

  operations: IOperation[] = [];
  dateFacturationDp: any;

  editForm = this.fb.group({
    id: [],
    numeroFacture: [],
    dateFacturation: [null, [Validators.required]],
    montantFacure: [null, [Validators.required, Validators.min(0)]],
    netAPayer: [null, [Validators.min(0)]],
    etat: [],
    operation: []
  });

  constructor(
    protected factureService: FactureService,
    protected operationService: OperationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ facture }) => {
      this.updateForm(facture);

      this.operationService
        .query()
        .pipe(
          map((res: HttpResponse<IOperation[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IOperation[]) => (this.operations = resBody));
    });
  }

  updateForm(facture: IFacture): void {
    this.editForm.patchValue({
      id: facture.id,
      numeroFacture: facture.numeroFacture,
      dateFacturation: facture.dateFacturation,
      montantFacure: facture.montantFacure,
      netAPayer: facture.netAPayer,
      etat: facture.etat,
      operationId: facture.operation
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const facture = this.createFromForm();
    if (facture.id !== undefined) {
      this.subscribeToSaveResponse(this.factureService.update(facture));
    } else {
      this.subscribeToSaveResponse(this.factureService.create(facture));
    }
  }

  private createFromForm(): IFacture {
    return {
      ...new Facture(),
      id: this.editForm.get(['id'])!.value,
      numeroFacture: this.editForm.get(['numeroFacture'])!.value,
      dateFacturation: this.editForm.get(['dateFacturation'])!.value,
      montantFacure: this.editForm.get(['montantFacure'])!.value,
      netAPayer: this.editForm.get(['netAPayer'])!.value,
      etat: this.editForm.get(['etat'])!.value,
      operation: this.editForm.get(['operationId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFacture>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOperation): any {
    return item.id;
  }
}
