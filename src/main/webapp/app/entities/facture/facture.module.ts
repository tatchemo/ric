import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RicSharedModule } from 'app/shared/shared.module';
import { FactureComponent } from './facture.component';
import { FactureDetailComponent } from './facture-detail.component';
import { FactureUpdateComponent } from './facture-update.component';
import { FactureDeleteDialogComponent } from './facture-delete-dialog.component';
import { PaiementFactureDialogComponent } from 'app/entities/facture/paiement-facture-dialog.component';
import { factureRoute } from './facture.route';

@NgModule({
  imports: [RicSharedModule, RouterModule.forChild(factureRoute)],
  declarations: [
    FactureComponent,
    FactureDetailComponent,
    FactureUpdateComponent,
    FactureDeleteDialogComponent,
    PaiementFactureDialogComponent
  ],
  entryComponents: [FactureDeleteDialogComponent, PaiementFactureDialogComponent]
})
export class RicFactureModule {}
