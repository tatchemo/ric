import { IPoste } from 'app/shared/model/poste.model';
import { IAgence } from 'app/shared/model/agence.model';
import { IEmploye } from 'app/shared/model/employe.model';

export interface IUser {
  id?: any;
  login?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  telephone?: string;
  cni?: string;
  deleted?: boolean;
  poste?: IPoste;
  agence?: IAgence;
  activated?: boolean;
  langKey?: string;
  authorities?: string[];
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  employe?: IEmploye;
}

export class User implements IUser {
  constructor(
    public id?: any,
    public login?: string,
    public password?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public telephone?: string,
    public cni?: string,
    public deleted?: boolean,
    public poste?: IPoste,
    public agence?: IAgence,
    public activated?: boolean,
    public langKey?: string,
    public authorities?: string[],
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date,
    public employe?: IEmploye
  ) {}
}
