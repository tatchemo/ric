import { Moment } from 'moment';
import { TypeBordereau } from 'app/shared/model/enumerations/type-bordereau.model';
import { IOperation } from 'app/shared/model/operation.model';
import { IClient } from 'app/shared/model/client.model';
import { IEmploye } from 'app/shared/model/employe.model';
import { ILigneOperation } from 'app/shared/model/ligne-operation.model';
import { ILigneOpSimpleDTOSModel } from 'app/shared/model/LigneOpSimpleDTOS.model';

export interface IBordereau {
  id?: number;
  numeroBordereau?: string;
  dateBordereau?: Moment;
  type?: TypeBordereau;
  operation?: IOperation;
  client?: IClient;
  employe?: IEmploye;
  ligneOperations?: ILigneOperation[];
  ligneOpSimpleDTOS?: ILigneOpSimpleDTOSModel[];
}

export class Bordereau implements IBordereau {
  constructor(
    public id?: number,
    public numeroBordereau?: string,
    public dateBordereau?: Moment,
    public type?: TypeBordereau,
    public operation?: IOperation,
    public client?: IClient,
    public employe?: IEmploye,
    public ligneOperations?: ILigneOperation[],
    public ligneOpSimpleDTOS?: ILigneOpSimpleDTOSModel[]
  ) {}
}
