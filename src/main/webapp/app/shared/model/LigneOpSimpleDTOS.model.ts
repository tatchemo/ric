export interface ILigneOpSimpleDTOSModel {
  id?: number;
  designation?: string;
  quantite?: number;
  pU?: number;
  montantLigneOperation?: number;
}

export class LigneOpSimpleDTOSModel implements ILigneOpSimpleDTOSModel {
  constructor(
    public id?: number,
    public designation?: string,
    public quantite?: number,
    public pU?: number,
    public montantLigneOperation?: number
  ) {}
}
