import { IProduit } from 'app/shared/model/produit.model';
import { IOperation } from 'app/shared/model/operation.model';

export interface ILigneOperation {
  id?: number;
  quantite?: number;
  pU?: number;
  montantLigneOperation?: number;
  deleted?: boolean;
  produit?: IProduit;
  operation?: IOperation;
}

export class LigneOperation implements ILigneOperation {
  constructor(
    public id?: number,
    public quantite?: number,
    public pU?: number,
    public montantLigneOperation?: number,
    public deleted?: boolean,
    public produit?: IProduit,
    public operation?: IOperation
  ) {
    this.deleted = this.deleted || false;
  }
}
