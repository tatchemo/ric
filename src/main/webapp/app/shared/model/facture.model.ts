import { Moment } from 'moment';
import { Etat } from 'app/shared/model/enumerations/etat.model';
import { IOperation } from 'app/shared/model/operation.model';
import { IPaiement } from 'app/shared/model/paiement.model';
import { ILigneOperation } from 'app/shared/model/ligne-operation.model';
import { ILigneOpSimpleDTOSModel } from 'app/shared/model/LigneOpSimpleDTOS.model';
import { IEmploye } from 'app/shared/model/employe.model';

export interface IFacture {
  id?: number;
  numeroFacture?: string;
  dateFacturation?: Moment;
  montantFacure?: number;
  netAPayer?: number;
  etat?: Etat;
  operation?: IOperation;
  employe?: IEmploye;
  ligneOpSimpleDTOS?: ILigneOpSimpleDTOSModel[];
  paiements?: IPaiement[];
  ligneOperations?: ILigneOperation[];
}

export class Facture implements IFacture {
  constructor(
    public id?: number,
    public numeroFacture?: string,
    public dateFacturation?: Moment,
    public montantFacure?: number,
    public netAPayer?: number,
    public etat?: Etat,
    public ligneOpSimpleDTOS?: ILigneOpSimpleDTOSModel[],
    public operation?: IOperation,
    public employe?: IEmploye,
    public paiements?: IPaiement[],
    public ligneOperations?: ILigneOperation[]
  ) {}
}
