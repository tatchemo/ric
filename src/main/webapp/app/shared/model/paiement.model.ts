import { IFacture } from 'app/shared/model/facture.model';
import { IClient } from 'app/shared/model/client.model';
import { IEmploye } from 'app/shared/model/employe.model';
import { Moment } from 'moment';

export interface IPaiement {
  id?: number;
  datePaiement?: Moment;
  montantPaiement?: number;
  facture?: IFacture;
  client?: IClient;
  employe?: IEmploye;
}

export class Paiement implements IPaiement {
  constructor(
    public id?: number,
    public datePaiement?: Moment,
    public montantPaiement?: number,
    public facture?: IFacture,
    public client?: IClient,
    public employe?: IEmploye
  ) {}
}
