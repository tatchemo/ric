import { Moment } from 'moment';
import { Etat } from 'app/shared/model/enumerations/etat.model';
import { ITypeOperation } from 'app/shared/model/type-operation.model';
import { IClient } from 'app/shared/model/client.model';
import { IEmploye } from 'app/shared/model/employe.model';
import { LigneOperation } from 'app/shared/model/ligne-operation.model';
import { IBordereau } from 'app/shared/model/bordereau.model';
import { IFacture } from 'app/shared/model/facture.model';

export interface IOperation {
  id?: number;
  numeroOperation?: string;
  dateOperation?: Moment;
  montantTotal?: number;
  deleted?: boolean;
  returned?: boolean;
  typeO?: number;
  etat?: Etat;
  typeOperation?: ITypeOperation;
  client?: IClient;
  employe?: IEmploye;
  facture?: IFacture;
  bordereau?: IBordereau;
  ligneOperations?: LigneOperation[];
}

export class Operation implements IOperation {
  constructor(
    public id?: number,
    public numeroOperation?: string,
    public dateOperation?: Moment,
    public montantTotal?: number,
    public deleted?: boolean,
    public returned?: boolean,
    public etat?: Etat,
    public typeOperation?: ITypeOperation,
    public client?: IClient,
    public employe?: IEmploye,
    public facture?: IFacture,
    public bordereau?: IBordereau,
    public ligneOperations?: LigneOperation[]
  ) {
    this.deleted = this.deleted || false;
    this.returned = this.returned || false;
  }
}
