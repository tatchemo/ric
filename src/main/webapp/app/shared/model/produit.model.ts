import { ICategorie } from 'app/shared/model/categorie.model';
import { ILigneOperation } from 'app/shared/model/ligne-operation.model';

export interface IProduit {
  id?: number;
  designation?: string;
  quantiteStock?: number;
  quantiteMinimale?: number;
  categorie?: ICategorie;
  ligneOperations?: ILigneOperation[];
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public designation?: string,
    public quantiteStock?: number,
    public quantiteMinimale?: number,
    public categorie?: ICategorie,
    public ligneOperations?: ILigneOperation[]
  ) {}
}
