export interface ITypeOperation {
  id?: number;
  libelle?: string;
}

export class TypeOperation implements ITypeOperation {
  constructor(public id?: number, public libelle?: string) {}
}
