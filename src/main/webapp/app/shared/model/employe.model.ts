import { IPoste } from 'app/shared/model/poste.model';
import { IUser } from 'app/core/user/user.model';
import { IAgence } from 'app/shared/model/agence.model';

export interface IEmploye {
  id?: number;
  nom?: string;
  prenom?: string;
  telephone?: string;
  cni?: string;
  deleted?: boolean;
  user?: IUser;
  poste?: IPoste;
  agence?: IAgence;
}

export class Employe implements IEmploye {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public telephone?: string,
    public cni?: string,
    public deleted?: boolean,
    public user?: IUser,
    public poste?: IPoste,
    public agence?: IAgence
  ) {
    this.deleted = this.deleted || false;
  }
}
