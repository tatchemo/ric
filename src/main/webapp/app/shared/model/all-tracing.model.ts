import { Moment } from 'moment';

export interface IAllTracing {
  id?: number;
  logDescription?: string;
  entityLogged?: string;
  logDate?: Moment;
  logGenerateBy?: string;
}

export class AllTracing implements IAllTracing {
  constructor(
    public id?: number,
    public logDescription?: string,
    public entityLogged?: string,
    public logDate?: Moment,
    public logGenerateBy?: string
  ) {}
}
