export interface IAgence {
  id?: number;
  nom?: string;
  ville?: string;
  telephone?: string;
  deleted?: boolean;
}

export class Agence implements IAgence {
  constructor(public id?: number, public nom?: string, public ville?: string, public telephone?: string, public deleted?: boolean) {
    this.deleted = this.deleted || false;
  }
}
