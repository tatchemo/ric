export interface IClient {
  id?: number;
  nom?: string;
  prenom?: string;
  telephone?: number;
  fournisseur?: boolean;
  deleted?: boolean;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public telephone?: number,
    public fournisseur?: boolean,
    public deleted?: boolean
  ) {
    this.fournisseur = this.fournisseur || false;
    this.deleted = this.deleted || false;
  }
}
