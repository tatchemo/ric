export const enum Etat {
  Brouillon,
  EnAttente,
  EnCours,
  Terminer,
  Payer
}
