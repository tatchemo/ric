import { ICategorie } from 'app/shared/model/categorie.model';

export interface IReturns {
  id?: number;
  numeroRetour?: string;
  motif?: string;
  commentaire?: string;
  operation?: ICategorie;
}

export class Returns implements IReturns {
  constructor(
    public id?: number,
    public numeroRetour?: string,
    public motif?: string,
    public commentaire?: string,
    public operation?: ICategorie
  ) {}
}
