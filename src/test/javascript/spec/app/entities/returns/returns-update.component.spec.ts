import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { RicTestModule } from '../../../test.module';
import { ReturnsUpdateComponent } from 'app/entities/returns/returns-update.component';
import { ReturnsService } from 'app/entities/returns/returns.service';
import { Returns } from 'app/shared/model/returns.model';

describe('Component Tests', () => {
  describe('Returns Management Update Component', () => {
    let comp: ReturnsUpdateComponent;
    let fixture: ComponentFixture<ReturnsUpdateComponent>;
    let service: ReturnsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RicTestModule],
        declarations: [ReturnsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ReturnsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ReturnsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ReturnsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Returns(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Returns();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
