import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RicTestModule } from '../../../test.module';
import { ReturnsDetailComponent } from 'app/entities/returns/returns-detail.component';
import { Returns } from 'app/shared/model/returns.model';

describe('Component Tests', () => {
  describe('Returns Management Detail Component', () => {
    let comp: ReturnsDetailComponent;
    let fixture: ComponentFixture<ReturnsDetailComponent>;
    const route = ({ data: of({ returns: new Returns(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RicTestModule],
        declarations: [ReturnsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ReturnsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ReturnsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load returns on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.returns).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
