import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RicTestModule } from '../../../test.module';
import { AllTracingDetailComponent } from 'app/entities/all-tracing/all-tracing-detail.component';
import { AllTracing } from 'app/shared/model/all-tracing.model';

describe('Component Tests', () => {
  describe('AllTracing Management Detail Component', () => {
    let comp: AllTracingDetailComponent;
    let fixture: ComponentFixture<AllTracingDetailComponent>;
    const route = ({ data: of({ allTracing: new AllTracing(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RicTestModule],
        declarations: [AllTracingDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AllTracingDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AllTracingDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load allTracing on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.allTracing).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
