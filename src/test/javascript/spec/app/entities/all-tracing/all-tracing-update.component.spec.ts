import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { RicTestModule } from '../../../test.module';
import { AllTracingUpdateComponent } from 'app/entities/all-tracing/all-tracing-update.component';
import { AllTracingService } from 'app/entities/all-tracing/all-tracing.service';
import { AllTracing } from 'app/shared/model/all-tracing.model';

describe('Component Tests', () => {
  describe('AllTracing Management Update Component', () => {
    let comp: AllTracingUpdateComponent;
    let fixture: ComponentFixture<AllTracingUpdateComponent>;
    let service: AllTracingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RicTestModule],
        declarations: [AllTracingUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AllTracingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AllTracingUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AllTracingService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AllTracing(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AllTracing();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
