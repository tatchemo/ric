import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { BordereauService } from 'app/entities/bordereau/bordereau.service';
import { IBordereau, Bordereau } from 'app/shared/model/bordereau.model';
import { TypeBordereau } from 'app/shared/model/enumerations/type-bordereau.model';

describe('Service Tests', () => {
  describe('Bordereau Service', () => {
    let injector: TestBed;
    let service: BordereauService;
    let httpMock: HttpTestingController;
    let elemDefault: IBordereau;
    let expectedResult: IBordereau | IBordereau[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BordereauService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Bordereau(0, 'AAAAAAA', currentDate, TypeBordereau.Approvionnement);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateBordereau: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Bordereau', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateBordereau: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateBordereau: currentDate
          },
          returnedFromService
        );
        service
          .create(new Bordereau())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Bordereau', () => {
        const returnedFromService = Object.assign(
          {
            numeroBordereau: 'BBBBBB',
            dateBordereau: currentDate.format(DATE_FORMAT),
            type: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateBordereau: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Bordereau', () => {
        const returnedFromService = Object.assign(
          {
            numeroBordereau: 'BBBBBB',
            dateBordereau: currentDate.format(DATE_FORMAT),
            type: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateBordereau: currentDate
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Bordereau', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
