import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CategorieComponentsPage, CategorieDeleteDialog, CategorieUpdatePage } from './categorie.page-object';

const expect = chai.expect;

describe('Categorie e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let categorieComponentsPage: CategorieComponentsPage;
  let categorieUpdatePage: CategorieUpdatePage;
  let categorieDeleteDialog: CategorieDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Categories', async () => {
    await navBarPage.goToEntity('categorie');
    categorieComponentsPage = new CategorieComponentsPage();
    await browser.wait(ec.visibilityOf(categorieComponentsPage.title), 5000);
    expect(await categorieComponentsPage.getTitle()).to.eq('ricApp.categorie.home.title');
  });

  it('should load create Categorie page', async () => {
    await categorieComponentsPage.clickOnCreateButton();
    categorieUpdatePage = new CategorieUpdatePage();
    expect(await categorieUpdatePage.getPageTitle()).to.eq('ricApp.categorie.home.createOrEditLabel');
    await categorieUpdatePage.cancel();
  });

  it('should create and save Categories', async () => {
    const nbButtonsBeforeCreate = await categorieComponentsPage.countDeleteButtons();

    await categorieComponentsPage.clickOnCreateButton();
    await promise.all([categorieUpdatePage.setLibelleInput('libelle')]);
    expect(await categorieUpdatePage.getLibelleInput()).to.eq('libelle', 'Expected Libelle value to be equals to libelle');
    await categorieUpdatePage.save();
    expect(await categorieUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await categorieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Categorie', async () => {
    const nbButtonsBeforeDelete = await categorieComponentsPage.countDeleteButtons();
    await categorieComponentsPage.clickOnLastDeleteButton();

    categorieDeleteDialog = new CategorieDeleteDialog();
    expect(await categorieDeleteDialog.getDialogTitle()).to.eq('ricApp.categorie.delete.question');
    await categorieDeleteDialog.clickOnConfirmButton();

    expect(await categorieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
