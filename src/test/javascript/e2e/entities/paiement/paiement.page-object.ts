import { element, by, ElementFinder } from 'protractor';

export class PaiementComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-paiement div table .btn-danger'));
  title = element.all(by.css('jhi-paiement div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class PaiementUpdatePage {
  pageTitle = element(by.id('jhi-paiement-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  datePaiementInput = element(by.id('field_datePaiement'));
  montantPaiementInput = element(by.id('field_montantPaiement'));
  factureSelect = element(by.id('field_facture'));
  clientSelect = element(by.id('field_client'));
  employeSelect = element(by.id('field_employe'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDatePaiementInput(datePaiement: string): Promise<void> {
    await this.datePaiementInput.sendKeys(datePaiement);
  }

  async getDatePaiementInput(): Promise<string> {
    return await this.datePaiementInput.getAttribute('value');
  }

  async setMontantPaiementInput(montantPaiement: string): Promise<void> {
    await this.montantPaiementInput.sendKeys(montantPaiement);
  }

  async getMontantPaiementInput(): Promise<string> {
    return await this.montantPaiementInput.getAttribute('value');
  }

  async factureSelectLastOption(): Promise<void> {
    await this.factureSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async factureSelectOption(option: string): Promise<void> {
    await this.factureSelect.sendKeys(option);
  }

  getFactureSelect(): ElementFinder {
    return this.factureSelect;
  }

  async getFactureSelectedOption(): Promise<string> {
    return await this.factureSelect.element(by.css('option:checked')).getText();
  }

  async clientSelectLastOption(): Promise<void> {
    await this.clientSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async clientSelectOption(option: string): Promise<void> {
    await this.clientSelect.sendKeys(option);
  }

  getClientSelect(): ElementFinder {
    return this.clientSelect;
  }

  async getClientSelectedOption(): Promise<string> {
    return await this.clientSelect.element(by.css('option:checked')).getText();
  }

  async employeSelectLastOption(): Promise<void> {
    await this.employeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async employeSelectOption(option: string): Promise<void> {
    await this.employeSelect.sendKeys(option);
  }

  getEmployeSelect(): ElementFinder {
    return this.employeSelect;
  }

  async getEmployeSelectedOption(): Promise<string> {
    return await this.employeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class PaiementDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-paiement-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-paiement'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
