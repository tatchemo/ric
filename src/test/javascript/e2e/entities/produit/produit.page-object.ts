import { element, by, ElementFinder } from 'protractor';

export class ProduitComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-produit div table .btn-danger'));
  title = element.all(by.css('jhi-produit div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ProduitUpdatePage {
  pageTitle = element(by.id('jhi-produit-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  designationInput = element(by.id('field_designation'));
  quantiteStockInput = element(by.id('field_quantiteStock'));
  quantiteMinimaleInput = element(by.id('field_quantiteMinimale'));
  categorieSelect = element(by.id('field_categorie'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDesignationInput(designation: string): Promise<void> {
    await this.designationInput.sendKeys(designation);
  }

  async getDesignationInput(): Promise<string> {
    return await this.designationInput.getAttribute('value');
  }

  async setQuantiteStockInput(quantiteStock: string): Promise<void> {
    await this.quantiteStockInput.sendKeys(quantiteStock);
  }

  async getQuantiteStockInput(): Promise<string> {
    return await this.quantiteStockInput.getAttribute('value');
  }

  async setQuantiteMinimaleInput(quantiteMinimale: string): Promise<void> {
    await this.quantiteMinimaleInput.sendKeys(quantiteMinimale);
  }

  async getQuantiteMinimaleInput(): Promise<string> {
    return await this.quantiteMinimaleInput.getAttribute('value');
  }

  async categorieSelectLastOption(): Promise<void> {
    await this.categorieSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async categorieSelectOption(option: string): Promise<void> {
    await this.categorieSelect.sendKeys(option);
  }

  getCategorieSelect(): ElementFinder {
    return this.categorieSelect;
  }

  async getCategorieSelectedOption(): Promise<string> {
    return await this.categorieSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ProduitDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-produit-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-produit'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
