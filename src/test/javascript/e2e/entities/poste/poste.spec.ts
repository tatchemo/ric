import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PosteComponentsPage, PosteDeleteDialog, PosteUpdatePage } from './poste.page-object';

const expect = chai.expect;

describe('Poste e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let posteComponentsPage: PosteComponentsPage;
  let posteUpdatePage: PosteUpdatePage;
  let posteDeleteDialog: PosteDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Postes', async () => {
    await navBarPage.goToEntity('poste');
    posteComponentsPage = new PosteComponentsPage();
    await browser.wait(ec.visibilityOf(posteComponentsPage.title), 5000);
    expect(await posteComponentsPage.getTitle()).to.eq('ricApp.poste.home.title');
  });

  it('should load create Poste page', async () => {
    await posteComponentsPage.clickOnCreateButton();
    posteUpdatePage = new PosteUpdatePage();
    expect(await posteUpdatePage.getPageTitle()).to.eq('ricApp.poste.home.createOrEditLabel');
    await posteUpdatePage.cancel();
  });

  it('should create and save Postes', async () => {
    const nbButtonsBeforeCreate = await posteComponentsPage.countDeleteButtons();

    await posteComponentsPage.clickOnCreateButton();
    await promise.all([posteUpdatePage.setLibelleInput('libelle')]);
    expect(await posteUpdatePage.getLibelleInput()).to.eq('libelle', 'Expected Libelle value to be equals to libelle');
    await posteUpdatePage.save();
    expect(await posteUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await posteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Poste', async () => {
    const nbButtonsBeforeDelete = await posteComponentsPage.countDeleteButtons();
    await posteComponentsPage.clickOnLastDeleteButton();

    posteDeleteDialog = new PosteDeleteDialog();
    expect(await posteDeleteDialog.getDialogTitle()).to.eq('ricApp.poste.delete.question');
    await posteDeleteDialog.clickOnConfirmButton();

    expect(await posteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
