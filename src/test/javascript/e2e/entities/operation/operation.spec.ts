import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OperationComponentsPage, OperationDeleteDialog, OperationUpdatePage } from './operation.page-object';

const expect = chai.expect;

describe('Operation e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let operationComponentsPage: OperationComponentsPage;
  let operationUpdatePage: OperationUpdatePage;
  let operationDeleteDialog: OperationDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Operations', async () => {
    await navBarPage.goToEntity('operation');
    operationComponentsPage = new OperationComponentsPage();
    await browser.wait(ec.visibilityOf(operationComponentsPage.title), 5000);
    expect(await operationComponentsPage.getTitle()).to.eq('ricApp.operation.home.title');
  });

  it('should load create Operation page', async () => {
    await operationComponentsPage.clickOnCreateButton();
    operationUpdatePage = new OperationUpdatePage();
    expect(await operationUpdatePage.getPageTitle()).to.eq('ricApp.operation.home.createOrEditLabel');
    await operationUpdatePage.cancel();
  });

  it('should create and save Operations', async () => {
    const nbButtonsBeforeCreate = await operationComponentsPage.countDeleteButtons();

    await operationComponentsPage.clickOnCreateButton();
    await promise.all([
      operationUpdatePage.setNumeroOperationInput('numeroOperation'),
      operationUpdatePage.setDateOperationInput('2000-12-31'),
      operationUpdatePage.setMontantTotalInput('5'),
      operationUpdatePage.etatSelectLastOption(),
      operationUpdatePage.typeOperationSelectLastOption(),
      operationUpdatePage.clientSelectLastOption(),
      operationUpdatePage.employeSelectLastOption()
    ]);
    expect(await operationUpdatePage.getNumeroOperationInput()).to.eq(
      'numeroOperation',
      'Expected NumeroOperation value to be equals to numeroOperation'
    );
    expect(await operationUpdatePage.getDateOperationInput()).to.eq(
      '2000-12-31',
      'Expected dateOperation value to be equals to 2000-12-31'
    );
    expect(await operationUpdatePage.getMontantTotalInput()).to.eq('5', 'Expected montantTotal value to be equals to 5');
    const selectedDeleted = operationUpdatePage.getDeletedInput();
    if (await selectedDeleted.isSelected()) {
      await operationUpdatePage.getDeletedInput().click();
      expect(await operationUpdatePage.getDeletedInput().isSelected(), 'Expected deleted not to be selected').to.be.false;
    } else {
      await operationUpdatePage.getDeletedInput().click();
      expect(await operationUpdatePage.getDeletedInput().isSelected(), 'Expected deleted to be selected').to.be.true;
    }
    const selectedReturned = operationUpdatePage.getReturnedInput();
    if (await selectedReturned.isSelected()) {
      await operationUpdatePage.getReturnedInput().click();
      expect(await operationUpdatePage.getReturnedInput().isSelected(), 'Expected returned not to be selected').to.be.false;
    } else {
      await operationUpdatePage.getReturnedInput().click();
      expect(await operationUpdatePage.getReturnedInput().isSelected(), 'Expected returned to be selected').to.be.true;
    }
    await operationUpdatePage.save();
    expect(await operationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await operationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Operation', async () => {
    const nbButtonsBeforeDelete = await operationComponentsPage.countDeleteButtons();
    await operationComponentsPage.clickOnLastDeleteButton();

    operationDeleteDialog = new OperationDeleteDialog();
    expect(await operationDeleteDialog.getDialogTitle()).to.eq('ricApp.operation.delete.question');
    await operationDeleteDialog.clickOnConfirmButton();

    expect(await operationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
