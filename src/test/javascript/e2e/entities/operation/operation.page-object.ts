import { element, by, ElementFinder } from 'protractor';

export class OperationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-operation div table .btn-danger'));
  title = element.all(by.css('jhi-operation div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class OperationUpdatePage {
  pageTitle = element(by.id('jhi-operation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  numeroOperationInput = element(by.id('field_numeroOperation'));
  dateOperationInput = element(by.id('field_dateOperation'));
  montantTotalInput = element(by.id('field_montantTotal'));
  deletedInput = element(by.id('field_deleted'));
  returnedInput = element(by.id('field_returned'));
  etatSelect = element(by.id('field_etat'));
  typeOperationSelect = element(by.id('field_typeOperation'));
  clientSelect = element(by.id('field_client'));
  employeSelect = element(by.id('field_employe'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumeroOperationInput(numeroOperation: string): Promise<void> {
    await this.numeroOperationInput.sendKeys(numeroOperation);
  }

  async getNumeroOperationInput(): Promise<string> {
    return await this.numeroOperationInput.getAttribute('value');
  }

  async setDateOperationInput(dateOperation: string): Promise<void> {
    await this.dateOperationInput.sendKeys(dateOperation);
  }

  async getDateOperationInput(): Promise<string> {
    return await this.dateOperationInput.getAttribute('value');
  }

  async setMontantTotalInput(montantTotal: string): Promise<void> {
    await this.montantTotalInput.sendKeys(montantTotal);
  }

  async getMontantTotalInput(): Promise<string> {
    return await this.montantTotalInput.getAttribute('value');
  }

  getDeletedInput(): ElementFinder {
    return this.deletedInput;
  }
  getReturnedInput(): ElementFinder {
    return this.returnedInput;
  }
  async setEtatSelect(etat: string): Promise<void> {
    await this.etatSelect.sendKeys(etat);
  }

  async getEtatSelect(): Promise<string> {
    return await this.etatSelect.element(by.css('option:checked')).getText();
  }

  async etatSelectLastOption(): Promise<void> {
    await this.etatSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async typeOperationSelectLastOption(): Promise<void> {
    await this.typeOperationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async typeOperationSelectOption(option: string): Promise<void> {
    await this.typeOperationSelect.sendKeys(option);
  }

  getTypeOperationSelect(): ElementFinder {
    return this.typeOperationSelect;
  }

  async getTypeOperationSelectedOption(): Promise<string> {
    return await this.typeOperationSelect.element(by.css('option:checked')).getText();
  }

  async clientSelectLastOption(): Promise<void> {
    await this.clientSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async clientSelectOption(option: string): Promise<void> {
    await this.clientSelect.sendKeys(option);
  }

  getClientSelect(): ElementFinder {
    return this.clientSelect;
  }

  async getClientSelectedOption(): Promise<string> {
    return await this.clientSelect.element(by.css('option:checked')).getText();
  }

  async employeSelectLastOption(): Promise<void> {
    await this.employeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async employeSelectOption(option: string): Promise<void> {
    await this.employeSelect.sendKeys(option);
  }

  getEmployeSelect(): ElementFinder {
    return this.employeSelect;
  }

  async getEmployeSelectedOption(): Promise<string> {
    return await this.employeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OperationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-operation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-operation'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
