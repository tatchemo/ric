import { element, by, ElementFinder } from 'protractor';

export class BordereauComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-bordereau div table .btn-danger'));
  title = element.all(by.css('jhi-bordereau div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BordereauUpdatePage {
  pageTitle = element(by.id('jhi-bordereau-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  numeroBordereauInput = element(by.id('field_numeroBordereau'));
  dateBordereauInput = element(by.id('field_dateBordereau'));
  typeSelect = element(by.id('field_type'));
  operationSelect = element(by.id('field_operation'));
  clientSelect = element(by.id('field_client'));
  employeSelect = element(by.id('field_employe'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumeroBordereauInput(numeroBordereau: string): Promise<void> {
    await this.numeroBordereauInput.sendKeys(numeroBordereau);
  }

  async getNumeroBordereauInput(): Promise<string> {
    return await this.numeroBordereauInput.getAttribute('value');
  }

  async setDateBordereauInput(dateBordereau: string): Promise<void> {
    await this.dateBordereauInput.sendKeys(dateBordereau);
  }

  async getDateBordereauInput(): Promise<string> {
    return await this.dateBordereauInput.getAttribute('value');
  }

  async setTypeSelect(type: string): Promise<void> {
    await this.typeSelect.sendKeys(type);
  }

  async getTypeSelect(): Promise<string> {
    return await this.typeSelect.element(by.css('option:checked')).getText();
  }

  async typeSelectLastOption(): Promise<void> {
    await this.typeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async operationSelectLastOption(): Promise<void> {
    await this.operationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async operationSelectOption(option: string): Promise<void> {
    await this.operationSelect.sendKeys(option);
  }

  getOperationSelect(): ElementFinder {
    return this.operationSelect;
  }

  async getOperationSelectedOption(): Promise<string> {
    return await this.operationSelect.element(by.css('option:checked')).getText();
  }

  async clientSelectLastOption(): Promise<void> {
    await this.clientSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async clientSelectOption(option: string): Promise<void> {
    await this.clientSelect.sendKeys(option);
  }

  getClientSelect(): ElementFinder {
    return this.clientSelect;
  }

  async getClientSelectedOption(): Promise<string> {
    return await this.clientSelect.element(by.css('option:checked')).getText();
  }

  async employeSelectLastOption(): Promise<void> {
    await this.employeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async employeSelectOption(option: string): Promise<void> {
    await this.employeSelect.sendKeys(option);
  }

  getEmployeSelect(): ElementFinder {
    return this.employeSelect;
  }

  async getEmployeSelectedOption(): Promise<string> {
    return await this.employeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BordereauDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-bordereau-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-bordereau'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
