import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BordereauComponentsPage, BordereauDeleteDialog, BordereauUpdatePage } from './bordereau.page-object';

const expect = chai.expect;

describe('Bordereau e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bordereauComponentsPage: BordereauComponentsPage;
  let bordereauUpdatePage: BordereauUpdatePage;
  let bordereauDeleteDialog: BordereauDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Bordereaus', async () => {
    await navBarPage.goToEntity('bordereau');
    bordereauComponentsPage = new BordereauComponentsPage();
    await browser.wait(ec.visibilityOf(bordereauComponentsPage.title), 5000);
    expect(await bordereauComponentsPage.getTitle()).to.eq('ricApp.bordereau.home.title');
  });

  it('should load create Bordereau page', async () => {
    await bordereauComponentsPage.clickOnCreateButton();
    bordereauUpdatePage = new BordereauUpdatePage();
    expect(await bordereauUpdatePage.getPageTitle()).to.eq('ricApp.bordereau.home.createOrEditLabel');
    await bordereauUpdatePage.cancel();
  });

  it('should create and save Bordereaus', async () => {
    const nbButtonsBeforeCreate = await bordereauComponentsPage.countDeleteButtons();

    await bordereauComponentsPage.clickOnCreateButton();
    await promise.all([
      bordereauUpdatePage.setNumeroBordereauInput('numeroBordereau'),
      bordereauUpdatePage.setDateBordereauInput('2000-12-31'),
      bordereauUpdatePage.typeSelectLastOption(),
      bordereauUpdatePage.operationSelectLastOption(),
      bordereauUpdatePage.clientSelectLastOption(),
      bordereauUpdatePage.employeSelectLastOption()
    ]);
    expect(await bordereauUpdatePage.getNumeroBordereauInput()).to.eq(
      'numeroBordereau',
      'Expected NumeroBordereau value to be equals to numeroBordereau'
    );
    expect(await bordereauUpdatePage.getDateBordereauInput()).to.eq(
      '2000-12-31',
      'Expected dateBordereau value to be equals to 2000-12-31'
    );
    await bordereauUpdatePage.save();
    expect(await bordereauUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await bordereauComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Bordereau', async () => {
    const nbButtonsBeforeDelete = await bordereauComponentsPage.countDeleteButtons();
    await bordereauComponentsPage.clickOnLastDeleteButton();

    bordereauDeleteDialog = new BordereauDeleteDialog();
    expect(await bordereauDeleteDialog.getDialogTitle()).to.eq('ricApp.bordereau.delete.question');
    await bordereauDeleteDialog.clickOnConfirmButton();

    expect(await bordereauComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
