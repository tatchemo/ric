import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ReturnsComponentsPage, ReturnsDeleteDialog, ReturnsUpdatePage } from './returns.page-object';

const expect = chai.expect;

describe('Returns e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let returnsComponentsPage: ReturnsComponentsPage;
  let returnsUpdatePage: ReturnsUpdatePage;
  let returnsDeleteDialog: ReturnsDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Returns', async () => {
    await navBarPage.goToEntity('returns');
    returnsComponentsPage = new ReturnsComponentsPage();
    await browser.wait(ec.visibilityOf(returnsComponentsPage.title), 5000);
    expect(await returnsComponentsPage.getTitle()).to.eq('ricApp.returns.home.title');
  });

  it('should load create Returns page', async () => {
    await returnsComponentsPage.clickOnCreateButton();
    returnsUpdatePage = new ReturnsUpdatePage();
    expect(await returnsUpdatePage.getPageTitle()).to.eq('ricApp.returns.home.createOrEditLabel');
    await returnsUpdatePage.cancel();
  });

  it('should create and save Returns', async () => {
    const nbButtonsBeforeCreate = await returnsComponentsPage.countDeleteButtons();

    await returnsComponentsPage.clickOnCreateButton();
    await promise.all([
      returnsUpdatePage.setNumeroRetourInput('numeroRetour'),
      returnsUpdatePage.setMotifInput('motif'),
      returnsUpdatePage.setCommentaireInput('commentaire'),
      returnsUpdatePage.operationSelectLastOption()
    ]);
    expect(await returnsUpdatePage.getNumeroRetourInput()).to.eq(
      'numeroRetour',
      'Expected NumeroRetour value to be equals to numeroRetour'
    );
    expect(await returnsUpdatePage.getMotifInput()).to.eq('motif', 'Expected Motif value to be equals to motif');
    expect(await returnsUpdatePage.getCommentaireInput()).to.eq('commentaire', 'Expected Commentaire value to be equals to commentaire');
    await returnsUpdatePage.save();
    expect(await returnsUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await returnsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Returns', async () => {
    const nbButtonsBeforeDelete = await returnsComponentsPage.countDeleteButtons();
    await returnsComponentsPage.clickOnLastDeleteButton();

    returnsDeleteDialog = new ReturnsDeleteDialog();
    expect(await returnsDeleteDialog.getDialogTitle()).to.eq('ricApp.returns.delete.question');
    await returnsDeleteDialog.clickOnConfirmButton();

    expect(await returnsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
