import { element, by, ElementFinder } from 'protractor';

export class ReturnsComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-returns div table .btn-danger'));
  title = element.all(by.css('jhi-returns div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ReturnsUpdatePage {
  pageTitle = element(by.id('jhi-returns-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  numeroRetourInput = element(by.id('field_numeroRetour'));
  motifInput = element(by.id('field_motif'));
  commentaireInput = element(by.id('field_commentaire'));
  operationSelect = element(by.id('field_operation'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumeroRetourInput(numeroRetour: string): Promise<void> {
    await this.numeroRetourInput.sendKeys(numeroRetour);
  }

  async getNumeroRetourInput(): Promise<string> {
    return await this.numeroRetourInput.getAttribute('value');
  }

  async setMotifInput(motif: string): Promise<void> {
    await this.motifInput.sendKeys(motif);
  }

  async getMotifInput(): Promise<string> {
    return await this.motifInput.getAttribute('value');
  }

  async setCommentaireInput(commentaire: string): Promise<void> {
    await this.commentaireInput.sendKeys(commentaire);
  }

  async getCommentaireInput(): Promise<string> {
    return await this.commentaireInput.getAttribute('value');
  }

  async operationSelectLastOption(): Promise<void> {
    await this.operationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async operationSelectOption(option: string): Promise<void> {
    await this.operationSelect.sendKeys(option);
  }

  getOperationSelect(): ElementFinder {
    return this.operationSelect;
  }

  async getOperationSelectedOption(): Promise<string> {
    return await this.operationSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ReturnsDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-returns-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-returns'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
