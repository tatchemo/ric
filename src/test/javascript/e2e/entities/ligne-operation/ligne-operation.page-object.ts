import { element, by, ElementFinder } from 'protractor';

export class LigneOperationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-ligne-operation div table .btn-danger'));
  title = element.all(by.css('jhi-ligne-operation div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class LigneOperationUpdatePage {
  pageTitle = element(by.id('jhi-ligne-operation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  quantiteInput = element(by.id('field_quantite'));
  pUInput = element(by.id('field_pU'));
  montantLigneOperationInput = element(by.id('field_montantLigneOperation'));
  deletedInput = element(by.id('field_deleted'));
  produitSelect = element(by.id('field_produit'));
  operationSelect = element(by.id('field_operation'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setQuantiteInput(quantite: string): Promise<void> {
    await this.quantiteInput.sendKeys(quantite);
  }

  async getQuantiteInput(): Promise<string> {
    return await this.quantiteInput.getAttribute('value');
  }

  async setPUInput(pU: string): Promise<void> {
    await this.pUInput.sendKeys(pU);
  }

  async getPUInput(): Promise<string> {
    return await this.pUInput.getAttribute('value');
  }

  async setMontantLigneOperationInput(montantLigneOperation: string): Promise<void> {
    await this.montantLigneOperationInput.sendKeys(montantLigneOperation);
  }

  async getMontantLigneOperationInput(): Promise<string> {
    return await this.montantLigneOperationInput.getAttribute('value');
  }

  getDeletedInput(): ElementFinder {
    return this.deletedInput;
  }

  async produitSelectLastOption(): Promise<void> {
    await this.produitSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async produitSelectOption(option: string): Promise<void> {
    await this.produitSelect.sendKeys(option);
  }

  getProduitSelect(): ElementFinder {
    return this.produitSelect;
  }

  async getProduitSelectedOption(): Promise<string> {
    return await this.produitSelect.element(by.css('option:checked')).getText();
  }

  async operationSelectLastOption(): Promise<void> {
    await this.operationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async operationSelectOption(option: string): Promise<void> {
    await this.operationSelect.sendKeys(option);
  }

  getOperationSelect(): ElementFinder {
    return this.operationSelect;
  }

  async getOperationSelectedOption(): Promise<string> {
    return await this.operationSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class LigneOperationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-ligneOperation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-ligneOperation'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
