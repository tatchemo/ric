import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { LigneOperationComponentsPage, LigneOperationDeleteDialog, LigneOperationUpdatePage } from './ligne-operation.page-object';

const expect = chai.expect;

describe('LigneOperation e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let ligneOperationComponentsPage: LigneOperationComponentsPage;
  let ligneOperationUpdatePage: LigneOperationUpdatePage;
  let ligneOperationDeleteDialog: LigneOperationDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load LigneOperations', async () => {
    await navBarPage.goToEntity('ligne-operation');
    ligneOperationComponentsPage = new LigneOperationComponentsPage();
    await browser.wait(ec.visibilityOf(ligneOperationComponentsPage.title), 5000);
    expect(await ligneOperationComponentsPage.getTitle()).to.eq('ricApp.ligneOperation.home.title');
  });

  it('should load create LigneOperation page', async () => {
    await ligneOperationComponentsPage.clickOnCreateButton();
    ligneOperationUpdatePage = new LigneOperationUpdatePage();
    expect(await ligneOperationUpdatePage.getPageTitle()).to.eq('ricApp.ligneOperation.home.createOrEditLabel');
    await ligneOperationUpdatePage.cancel();
  });

  it('should create and save LigneOperations', async () => {
    const nbButtonsBeforeCreate = await ligneOperationComponentsPage.countDeleteButtons();

    await ligneOperationComponentsPage.clickOnCreateButton();
    await promise.all([
      ligneOperationUpdatePage.setQuantiteInput('5'),
      ligneOperationUpdatePage.setPUInput('5'),
      ligneOperationUpdatePage.setMontantLigneOperationInput('5'),
      ligneOperationUpdatePage.produitSelectLastOption(),
      ligneOperationUpdatePage.operationSelectLastOption()
    ]);
    expect(await ligneOperationUpdatePage.getQuantiteInput()).to.eq('5', 'Expected quantite value to be equals to 5');
    expect(await ligneOperationUpdatePage.getPUInput()).to.eq('5', 'Expected pU value to be equals to 5');
    expect(await ligneOperationUpdatePage.getMontantLigneOperationInput()).to.eq(
      '5',
      'Expected montantLigneOperation value to be equals to 5'
    );
    const selectedDeleted = ligneOperationUpdatePage.getDeletedInput();
    if (await selectedDeleted.isSelected()) {
      await ligneOperationUpdatePage.getDeletedInput().click();
      expect(await ligneOperationUpdatePage.getDeletedInput().isSelected(), 'Expected deleted not to be selected').to.be.false;
    } else {
      await ligneOperationUpdatePage.getDeletedInput().click();
      expect(await ligneOperationUpdatePage.getDeletedInput().isSelected(), 'Expected deleted to be selected').to.be.true;
    }
    await ligneOperationUpdatePage.save();
    expect(await ligneOperationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await ligneOperationComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last LigneOperation', async () => {
    const nbButtonsBeforeDelete = await ligneOperationComponentsPage.countDeleteButtons();
    await ligneOperationComponentsPage.clickOnLastDeleteButton();

    ligneOperationDeleteDialog = new LigneOperationDeleteDialog();
    expect(await ligneOperationDeleteDialog.getDialogTitle()).to.eq('ricApp.ligneOperation.delete.question');
    await ligneOperationDeleteDialog.clickOnConfirmButton();

    expect(await ligneOperationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
