import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FactureComponentsPage, FactureDeleteDialog, FactureUpdatePage } from './facture.page-object';

const expect = chai.expect;

describe('Facture e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let factureComponentsPage: FactureComponentsPage;
  let factureUpdatePage: FactureUpdatePage;
  let factureDeleteDialog: FactureDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Factures', async () => {
    await navBarPage.goToEntity('facture');
    factureComponentsPage = new FactureComponentsPage();
    await browser.wait(ec.visibilityOf(factureComponentsPage.title), 5000);
    expect(await factureComponentsPage.getTitle()).to.eq('ricApp.facture.home.title');
  });

  it('should load create Facture page', async () => {
    await factureComponentsPage.clickOnCreateButton();
    factureUpdatePage = new FactureUpdatePage();
    expect(await factureUpdatePage.getPageTitle()).to.eq('ricApp.facture.home.createOrEditLabel');
    await factureUpdatePage.cancel();
  });

  it('should create and save Factures', async () => {
    const nbButtonsBeforeCreate = await factureComponentsPage.countDeleteButtons();

    await factureComponentsPage.clickOnCreateButton();
    await promise.all([
      factureUpdatePage.setNumeroFactureInput('numeroFacture'),
      factureUpdatePage.setDateFacturationInput('2000-12-31'),
      factureUpdatePage.setMontantFacureInput('5'),
      factureUpdatePage.setNetAPayerInput('5'),
      factureUpdatePage.etatSelectLastOption(),
      factureUpdatePage.operationSelectLastOption()
    ]);
    expect(await factureUpdatePage.getNumeroFactureInput()).to.eq(
      'numeroFacture',
      'Expected NumeroFacture value to be equals to numeroFacture'
    );
    expect(await factureUpdatePage.getDateFacturationInput()).to.eq(
      '2000-12-31',
      'Expected dateFacturation value to be equals to 2000-12-31'
    );
    expect(await factureUpdatePage.getMontantFacureInput()).to.eq('5', 'Expected montantFacure value to be equals to 5');
    expect(await factureUpdatePage.getNetAPayerInput()).to.eq('5', 'Expected netAPayer value to be equals to 5');
    await factureUpdatePage.save();
    expect(await factureUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await factureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Facture', async () => {
    const nbButtonsBeforeDelete = await factureComponentsPage.countDeleteButtons();
    await factureComponentsPage.clickOnLastDeleteButton();

    factureDeleteDialog = new FactureDeleteDialog();
    expect(await factureDeleteDialog.getDialogTitle()).to.eq('ricApp.facture.delete.question');
    await factureDeleteDialog.clickOnConfirmButton();

    expect(await factureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
