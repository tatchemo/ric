import { element, by, ElementFinder } from 'protractor';

export class FactureComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-facture div table .btn-danger'));
  title = element.all(by.css('jhi-facture div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FactureUpdatePage {
  pageTitle = element(by.id('jhi-facture-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  numeroFactureInput = element(by.id('field_numeroFacture'));
  dateFacturationInput = element(by.id('field_dateFacturation'));
  montantFacureInput = element(by.id('field_montantFacure'));
  netAPayerInput = element(by.id('field_netAPayer'));
  etatSelect = element(by.id('field_etat'));
  operationSelect = element(by.id('field_operation'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumeroFactureInput(numeroFacture: string): Promise<void> {
    await this.numeroFactureInput.sendKeys(numeroFacture);
  }

  async getNumeroFactureInput(): Promise<string> {
    return await this.numeroFactureInput.getAttribute('value');
  }

  async setDateFacturationInput(dateFacturation: string): Promise<void> {
    await this.dateFacturationInput.sendKeys(dateFacturation);
  }

  async getDateFacturationInput(): Promise<string> {
    return await this.dateFacturationInput.getAttribute('value');
  }

  async setMontantFacureInput(montantFacure: string): Promise<void> {
    await this.montantFacureInput.sendKeys(montantFacure);
  }

  async getMontantFacureInput(): Promise<string> {
    return await this.montantFacureInput.getAttribute('value');
  }

  async setNetAPayerInput(netAPayer: string): Promise<void> {
    await this.netAPayerInput.sendKeys(netAPayer);
  }

  async getNetAPayerInput(): Promise<string> {
    return await this.netAPayerInput.getAttribute('value');
  }

  async setEtatSelect(etat: string): Promise<void> {
    await this.etatSelect.sendKeys(etat);
  }

  async getEtatSelect(): Promise<string> {
    return await this.etatSelect.element(by.css('option:checked')).getText();
  }

  async etatSelectLastOption(): Promise<void> {
    await this.etatSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async operationSelectLastOption(): Promise<void> {
    await this.operationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async operationSelectOption(option: string): Promise<void> {
    await this.operationSelect.sendKeys(option);
  }

  getOperationSelect(): ElementFinder {
    return this.operationSelect;
  }

  async getOperationSelectedOption(): Promise<string> {
    return await this.operationSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FactureDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-facture-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-facture'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
