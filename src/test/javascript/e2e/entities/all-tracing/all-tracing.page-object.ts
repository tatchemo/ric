import { element, by, ElementFinder } from 'protractor';

export class AllTracingComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-all-tracing div table .btn-danger'));
  title = element.all(by.css('jhi-all-tracing div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AllTracingUpdatePage {
  pageTitle = element(by.id('jhi-all-tracing-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  logDescriptionInput = element(by.id('field_logDescription'));
  entityLoggedInput = element(by.id('field_entityLogged'));
  logDateInput = element(by.id('field_logDate'));
  logGenerateByInput = element(by.id('field_logGenerateBy'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLogDescriptionInput(logDescription: string): Promise<void> {
    await this.logDescriptionInput.sendKeys(logDescription);
  }

  async getLogDescriptionInput(): Promise<string> {
    return await this.logDescriptionInput.getAttribute('value');
  }

  async setEntityLoggedInput(entityLogged: string): Promise<void> {
    await this.entityLoggedInput.sendKeys(entityLogged);
  }

  async getEntityLoggedInput(): Promise<string> {
    return await this.entityLoggedInput.getAttribute('value');
  }

  async setLogDateInput(logDate: string): Promise<void> {
    await this.logDateInput.sendKeys(logDate);
  }

  async getLogDateInput(): Promise<string> {
    return await this.logDateInput.getAttribute('value');
  }

  async setLogGenerateByInput(logGenerateBy: string): Promise<void> {
    await this.logGenerateByInput.sendKeys(logGenerateBy);
  }

  async getLogGenerateByInput(): Promise<string> {
    return await this.logGenerateByInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AllTracingDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-allTracing-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-allTracing'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
