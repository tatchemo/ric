import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AllTracingComponentsPage, AllTracingDeleteDialog, AllTracingUpdatePage } from './all-tracing.page-object';

const expect = chai.expect;

describe('AllTracing e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let allTracingComponentsPage: AllTracingComponentsPage;
  let allTracingUpdatePage: AllTracingUpdatePage;
  let allTracingDeleteDialog: AllTracingDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AllTracings', async () => {
    await navBarPage.goToEntity('all-tracing');
    allTracingComponentsPage = new AllTracingComponentsPage();
    await browser.wait(ec.visibilityOf(allTracingComponentsPage.title), 5000);
    expect(await allTracingComponentsPage.getTitle()).to.eq('ricApp.allTracing.home.title');
  });

  it('should load create AllTracing page', async () => {
    await allTracingComponentsPage.clickOnCreateButton();
    allTracingUpdatePage = new AllTracingUpdatePage();
    expect(await allTracingUpdatePage.getPageTitle()).to.eq('ricApp.allTracing.home.createOrEditLabel');
    await allTracingUpdatePage.cancel();
  });

  it('should create and save AllTracings', async () => {
    const nbButtonsBeforeCreate = await allTracingComponentsPage.countDeleteButtons();

    await allTracingComponentsPage.clickOnCreateButton();
    await promise.all([
      allTracingUpdatePage.setLogDescriptionInput('logDescription'),
      allTracingUpdatePage.setEntityLoggedInput('entityLogged'),
      allTracingUpdatePage.setLogDateInput('2000-12-31'),
      allTracingUpdatePage.setLogGenerateByInput('logGenerateBy')
    ]);
    expect(await allTracingUpdatePage.getLogDescriptionInput()).to.eq(
      'logDescription',
      'Expected LogDescription value to be equals to logDescription'
    );
    expect(await allTracingUpdatePage.getEntityLoggedInput()).to.eq(
      'entityLogged',
      'Expected EntityLogged value to be equals to entityLogged'
    );
    expect(await allTracingUpdatePage.getLogDateInput()).to.eq('2000-12-31', 'Expected logDate value to be equals to 2000-12-31');
    expect(await allTracingUpdatePage.getLogGenerateByInput()).to.eq(
      'logGenerateBy',
      'Expected LogGenerateBy value to be equals to logGenerateBy'
    );
    await allTracingUpdatePage.save();
    expect(await allTracingUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await allTracingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last AllTracing', async () => {
    const nbButtonsBeforeDelete = await allTracingComponentsPage.countDeleteButtons();
    await allTracingComponentsPage.clickOnLastDeleteButton();

    allTracingDeleteDialog = new AllTracingDeleteDialog();
    expect(await allTracingDeleteDialog.getDialogTitle()).to.eq('ricApp.allTracing.delete.question');
    await allTracingDeleteDialog.clickOnConfirmButton();

    expect(await allTracingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
