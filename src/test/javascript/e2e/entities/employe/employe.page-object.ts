import { element, by, ElementFinder } from 'protractor';

export class EmployeComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-employe div table .btn-danger'));
  title = element.all(by.css('jhi-employe div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EmployeUpdatePage {
  pageTitle = element(by.id('jhi-employe-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nomInput = element(by.id('field_nom'));
  prenomInput = element(by.id('field_prenom'));
  telephoneInput = element(by.id('field_telephone'));
  cniInput = element(by.id('field_cni'));
  deletedInput = element(by.id('field_deleted'));
  userSelect = element(by.id('field_user'));
  posteSelect = element(by.id('field_poste'));
  agenceSelect = element(by.id('field_agence'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNomInput(nom: string): Promise<void> {
    await this.nomInput.sendKeys(nom);
  }

  async getNomInput(): Promise<string> {
    return await this.nomInput.getAttribute('value');
  }

  async setPrenomInput(prenom: string): Promise<void> {
    await this.prenomInput.sendKeys(prenom);
  }

  async getPrenomInput(): Promise<string> {
    return await this.prenomInput.getAttribute('value');
  }

  async setTelephoneInput(telephone: string): Promise<void> {
    await this.telephoneInput.sendKeys(telephone);
  }

  async getTelephoneInput(): Promise<string> {
    return await this.telephoneInput.getAttribute('value');
  }

  async setCniInput(cni: string): Promise<void> {
    await this.cniInput.sendKeys(cni);
  }

  async getCniInput(): Promise<string> {
    return await this.cniInput.getAttribute('value');
  }

  getDeletedInput(): ElementFinder {
    return this.deletedInput;
  }

  async userSelectLastOption(): Promise<void> {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option: string): Promise<void> {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption(): Promise<string> {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async posteSelectLastOption(): Promise<void> {
    await this.posteSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async posteSelectOption(option: string): Promise<void> {
    await this.posteSelect.sendKeys(option);
  }

  getPosteSelect(): ElementFinder {
    return this.posteSelect;
  }

  async getPosteSelectedOption(): Promise<string> {
    return await this.posteSelect.element(by.css('option:checked')).getText();
  }

  async agenceSelectLastOption(): Promise<void> {
    await this.agenceSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async agenceSelectOption(option: string): Promise<void> {
    await this.agenceSelect.sendKeys(option);
  }

  getAgenceSelect(): ElementFinder {
    return this.agenceSelect;
  }

  async getAgenceSelectedOption(): Promise<string> {
    return await this.agenceSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EmployeDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-employe-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-employe'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
