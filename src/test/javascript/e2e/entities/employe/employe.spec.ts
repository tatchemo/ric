import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EmployeComponentsPage, EmployeDeleteDialog, EmployeUpdatePage } from './employe.page-object';

const expect = chai.expect;

describe('Employe e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let employeComponentsPage: EmployeComponentsPage;
  let employeUpdatePage: EmployeUpdatePage;
  let employeDeleteDialog: EmployeDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Employes', async () => {
    await navBarPage.goToEntity('employe');
    employeComponentsPage = new EmployeComponentsPage();
    await browser.wait(ec.visibilityOf(employeComponentsPage.title), 5000);
    expect(await employeComponentsPage.getTitle()).to.eq('ricApp.employe.home.title');
  });

  it('should load create Employe page', async () => {
    await employeComponentsPage.clickOnCreateButton();
    employeUpdatePage = new EmployeUpdatePage();
    expect(await employeUpdatePage.getPageTitle()).to.eq('ricApp.employe.home.createOrEditLabel');
    await employeUpdatePage.cancel();
  });

  it('should create and save Employes', async () => {
    const nbButtonsBeforeCreate = await employeComponentsPage.countDeleteButtons();

    await employeComponentsPage.clickOnCreateButton();
    await promise.all([
      employeUpdatePage.setNomInput('nom'),
      employeUpdatePage.setPrenomInput('prenom'),
      employeUpdatePage.setTelephoneInput('telephone'),
      employeUpdatePage.setCniInput('cni'),
      employeUpdatePage.userSelectLastOption(),
      employeUpdatePage.posteSelectLastOption(),
      employeUpdatePage.agenceSelectLastOption()
    ]);
    expect(await employeUpdatePage.getNomInput()).to.eq('nom', 'Expected Nom value to be equals to nom');
    expect(await employeUpdatePage.getPrenomInput()).to.eq('prenom', 'Expected Prenom value to be equals to prenom');
    expect(await employeUpdatePage.getTelephoneInput()).to.eq('telephone', 'Expected Telephone value to be equals to telephone');
    expect(await employeUpdatePage.getCniInput()).to.eq('cni', 'Expected Cni value to be equals to cni');
    const selectedDeleted = employeUpdatePage.getDeletedInput();
    if (await selectedDeleted.isSelected()) {
      await employeUpdatePage.getDeletedInput().click();
      expect(await employeUpdatePage.getDeletedInput().isSelected(), 'Expected deleted not to be selected').to.be.false;
    } else {
      await employeUpdatePage.getDeletedInput().click();
      expect(await employeUpdatePage.getDeletedInput().isSelected(), 'Expected deleted to be selected').to.be.true;
    }
    await employeUpdatePage.save();
    expect(await employeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await employeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Employe', async () => {
    const nbButtonsBeforeDelete = await employeComponentsPage.countDeleteButtons();
    await employeComponentsPage.clickOnLastDeleteButton();

    employeDeleteDialog = new EmployeDeleteDialog();
    expect(await employeDeleteDialog.getDialogTitle()).to.eq('ricApp.employe.delete.question');
    await employeDeleteDialog.clickOnConfirmButton();

    expect(await employeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
