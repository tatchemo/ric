package com.urservices.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.urservices.myapp.web.rest.TestUtil;

public class AllTracingTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AllTracing.class);
        AllTracing allTracing1 = new AllTracing();
        allTracing1.setId(1L);
        AllTracing allTracing2 = new AllTracing();
        allTracing2.setId(allTracing1.getId());
        assertThat(allTracing1).isEqualTo(allTracing2);
        allTracing2.setId(2L);
        assertThat(allTracing1).isNotEqualTo(allTracing2);
        allTracing1.setId(null);
        assertThat(allTracing1).isNotEqualTo(allTracing2);
    }
}
