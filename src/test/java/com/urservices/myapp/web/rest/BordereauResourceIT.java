package com.urservices.myapp.web.rest;

import com.urservices.myapp.RicApp;
import com.urservices.myapp.domain.Bordereau;
import com.urservices.myapp.repository.BordereauRepository;
import com.urservices.myapp.service.BordereauService;
import com.urservices.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.urservices.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.urservices.myapp.domain.enumeration.TypeBordereau;
/**
 * Integration tests for the {@link BordereauResource} REST controller.
 */
@SpringBootTest(classes = RicApp.class)
public class BordereauResourceIT {

    private static final String DEFAULT_NUMERO_BORDEREAU = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_BORDEREAU = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_BORDEREAU = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_BORDEREAU = LocalDate.now(ZoneId.systemDefault());

    private static final TypeBordereau DEFAULT_TYPE = TypeBordereau.Approvionnement;
    private static final TypeBordereau UPDATED_TYPE = TypeBordereau.Livraison;

    @Autowired
    private BordereauRepository bordereauRepository;

    @Autowired
    private BordereauService bordereauService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBordereauMockMvc;

    private Bordereau bordereau;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BordereauResource bordereauResource = new BordereauResource(bordereauService);
        this.restBordereauMockMvc = MockMvcBuilders.standaloneSetup(bordereauResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bordereau createEntity(EntityManager em) {
        Bordereau bordereau = new Bordereau()
            .numeroBordereau(DEFAULT_NUMERO_BORDEREAU)
            .dateBordereau(DEFAULT_DATE_BORDEREAU)
            .type(DEFAULT_TYPE);
        return bordereau;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bordereau createUpdatedEntity(EntityManager em) {
        Bordereau bordereau = new Bordereau()
            .numeroBordereau(UPDATED_NUMERO_BORDEREAU)
            .dateBordereau(UPDATED_DATE_BORDEREAU)
            .type(UPDATED_TYPE);
        return bordereau;
    }

    @BeforeEach
    public void initTest() {
        bordereau = createEntity(em);
    }

    @Test
    @Transactional
    public void createBordereau() throws Exception {
        int databaseSizeBeforeCreate = bordereauRepository.findAll().size();

        // Create the Bordereau
        restBordereauMockMvc.perform(post("/api/bordereaus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bordereau)))
            .andExpect(status().isCreated());

        // Validate the Bordereau in the database
        List<Bordereau> bordereauList = bordereauRepository.findAll();
        assertThat(bordereauList).hasSize(databaseSizeBeforeCreate + 1);
        Bordereau testBordereau = bordereauList.get(bordereauList.size() - 1);
        assertThat(testBordereau.getNumeroBordereau()).isEqualTo(DEFAULT_NUMERO_BORDEREAU);
        assertThat(testBordereau.getDateBordereau()).isEqualTo(DEFAULT_DATE_BORDEREAU);
        assertThat(testBordereau.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createBordereauWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bordereauRepository.findAll().size();

        // Create the Bordereau with an existing ID
        bordereau.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBordereauMockMvc.perform(post("/api/bordereaus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bordereau)))
            .andExpect(status().isBadRequest());

        // Validate the Bordereau in the database
        List<Bordereau> bordereauList = bordereauRepository.findAll();
        assertThat(bordereauList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDateBordereauIsRequired() throws Exception {
        int databaseSizeBeforeTest = bordereauRepository.findAll().size();
        // set the field null
        bordereau.setDateBordereau(null);

        // Create the Bordereau, which fails.

        restBordereauMockMvc.perform(post("/api/bordereaus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bordereau)))
            .andExpect(status().isBadRequest());

        List<Bordereau> bordereauList = bordereauRepository.findAll();
        assertThat(bordereauList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBordereaus() throws Exception {
        // Initialize the database
        bordereauRepository.saveAndFlush(bordereau);

        // Get all the bordereauList
        restBordereauMockMvc.perform(get("/api/bordereaus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bordereau.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroBordereau").value(hasItem(DEFAULT_NUMERO_BORDEREAU)))
            .andExpect(jsonPath("$.[*].dateBordereau").value(hasItem(DEFAULT_DATE_BORDEREAU.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getBordereau() throws Exception {
        // Initialize the database
        bordereauRepository.saveAndFlush(bordereau);

        // Get the bordereau
        restBordereauMockMvc.perform(get("/api/bordereaus/{id}", bordereau.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bordereau.getId().intValue()))
            .andExpect(jsonPath("$.numeroBordereau").value(DEFAULT_NUMERO_BORDEREAU))
            .andExpect(jsonPath("$.dateBordereau").value(DEFAULT_DATE_BORDEREAU.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBordereau() throws Exception {
        // Get the bordereau
        restBordereauMockMvc.perform(get("/api/bordereaus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBordereau() throws Exception {
        // Initialize the database
        bordereauRepository.saveAndFlush(bordereau);

        int databaseSizeBeforeUpdate = bordereauRepository.findAll().size();

        // Update the bordereau
        Bordereau updatedBordereau = bordereauRepository.findById(bordereau.getId()).get();
        // Disconnect from session so that the updates on updatedBordereau are not directly saved in db
        em.detach(updatedBordereau);
        updatedBordereau
            .numeroBordereau(UPDATED_NUMERO_BORDEREAU)
            .dateBordereau(UPDATED_DATE_BORDEREAU)
            .type(UPDATED_TYPE);

        restBordereauMockMvc.perform(put("/api/bordereaus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bordereau)))
            .andExpect(status().isOk());

        // Validate the Bordereau in the database
        List<Bordereau> bordereauList = bordereauRepository.findAll();
        assertThat(bordereauList).hasSize(databaseSizeBeforeUpdate);
        Bordereau testBordereau = bordereauList.get(bordereauList.size() - 1);
        assertThat(testBordereau.getNumeroBordereau()).isEqualTo(UPDATED_NUMERO_BORDEREAU);
        assertThat(testBordereau.getDateBordereau()).isEqualTo(UPDATED_DATE_BORDEREAU);
        assertThat(testBordereau.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingBordereau() throws Exception {
        int databaseSizeBeforeUpdate = bordereauRepository.findAll().size();

        // Create the Bordereau

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBordereauMockMvc.perform(put("/api/bordereaus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bordereau)))
            .andExpect(status().isBadRequest());

        // Validate the Bordereau in the database
        List<Bordereau> bordereauList = bordereauRepository.findAll();
        assertThat(bordereauList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBordereau() throws Exception {
        // Initialize the database
        bordereauRepository.saveAndFlush(bordereau);

        int databaseSizeBeforeDelete = bordereauRepository.findAll().size();

        // Delete the bordereau
        restBordereauMockMvc.perform(delete("/api/bordereaus/{id}", bordereau.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Bordereau> bordereauList = bordereauRepository.findAll();
        assertThat(bordereauList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
