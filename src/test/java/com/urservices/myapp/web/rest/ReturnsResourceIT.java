package com.urservices.myapp.web.rest;

import com.urservices.myapp.RicApp;
import com.urservices.myapp.domain.Returns;
import com.urservices.myapp.repository.ReturnsRepository;
import com.urservices.myapp.service.ReturnsService;
import com.urservices.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.urservices.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ReturnsResource} REST controller.
 */
@SpringBootTest(classes = RicApp.class)
public class ReturnsResourceIT {

    private static final String DEFAULT_NUMERO_RETOUR = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_RETOUR = "BBBBBBBBBB";

    private static final String DEFAULT_MOTIF = "AAAAAAAAAA";
    private static final String UPDATED_MOTIF = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTAIRE = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTAIRE = "BBBBBBBBBB";

    @Autowired
    private ReturnsRepository returnsRepository;

    @Autowired
    private ReturnsService returnsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restReturnsMockMvc;

    private Returns returns;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReturnsResource returnsResource = new ReturnsResource(returnsService);
        this.restReturnsMockMvc = MockMvcBuilders.standaloneSetup(returnsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Returns createEntity(EntityManager em) {
        Returns returns = new Returns()
            .numeroRetour(DEFAULT_NUMERO_RETOUR)
            .motif(DEFAULT_MOTIF)
            .commentaire(DEFAULT_COMMENTAIRE);
        return returns;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Returns createUpdatedEntity(EntityManager em) {
        Returns returns = new Returns()
            .numeroRetour(UPDATED_NUMERO_RETOUR)
            .motif(UPDATED_MOTIF)
            .commentaire(UPDATED_COMMENTAIRE);
        return returns;
    }

    @BeforeEach
    public void initTest() {
        returns = createEntity(em);
    }

    @Test
    @Transactional
    public void createReturns() throws Exception {
        int databaseSizeBeforeCreate = returnsRepository.findAll().size();

        // Create the Returns
        restReturnsMockMvc.perform(post("/api/returns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returns)))
            .andExpect(status().isCreated());

        // Validate the Returns in the database
        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeCreate + 1);
        Returns testReturns = returnsList.get(returnsList.size() - 1);
        assertThat(testReturns.getNumeroRetour()).isEqualTo(DEFAULT_NUMERO_RETOUR);
        assertThat(testReturns.getMotif()).isEqualTo(DEFAULT_MOTIF);
        assertThat(testReturns.getCommentaire()).isEqualTo(DEFAULT_COMMENTAIRE);
    }

    @Test
    @Transactional
    public void createReturnsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = returnsRepository.findAll().size();

        // Create the Returns with an existing ID
        returns.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReturnsMockMvc.perform(post("/api/returns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returns)))
            .andExpect(status().isBadRequest());

        // Validate the Returns in the database
        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNumeroRetourIsRequired() throws Exception {
        int databaseSizeBeforeTest = returnsRepository.findAll().size();
        // set the field null
        returns.setNumeroRetour(null);

        // Create the Returns, which fails.

        restReturnsMockMvc.perform(post("/api/returns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returns)))
            .andExpect(status().isBadRequest());

        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMotifIsRequired() throws Exception {
        int databaseSizeBeforeTest = returnsRepository.findAll().size();
        // set the field null
        returns.setMotif(null);

        // Create the Returns, which fails.

        restReturnsMockMvc.perform(post("/api/returns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returns)))
            .andExpect(status().isBadRequest());

        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllReturns() throws Exception {
        // Initialize the database
        returnsRepository.saveAndFlush(returns);

        // Get all the returnsList
        restReturnsMockMvc.perform(get("/api/returns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(returns.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroRetour").value(hasItem(DEFAULT_NUMERO_RETOUR)))
            .andExpect(jsonPath("$.[*].motif").value(hasItem(DEFAULT_MOTIF)))
            .andExpect(jsonPath("$.[*].commentaire").value(hasItem(DEFAULT_COMMENTAIRE)));
    }

    @Test
    @Transactional
    public void getReturns() throws Exception {
        // Initialize the database
        returnsRepository.saveAndFlush(returns);

        // Get the returns
        restReturnsMockMvc.perform(get("/api/returns/{id}", returns.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(returns.getId().intValue()))
            .andExpect(jsonPath("$.numeroRetour").value(DEFAULT_NUMERO_RETOUR))
            .andExpect(jsonPath("$.motif").value(DEFAULT_MOTIF))
            .andExpect(jsonPath("$.commentaire").value(DEFAULT_COMMENTAIRE));
    }

    @Test
    @Transactional
    public void getNonExistingReturns() throws Exception {
        // Get the returns
        restReturnsMockMvc.perform(get("/api/returns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReturns() throws Exception {
        // Initialize the database
        returnsRepository.saveAndFlush(returns);

        int databaseSizeBeforeUpdate = returnsRepository.findAll().size();

        // Update the returns
        Returns updatedReturns = returnsRepository.findById(returns.getId()).get();
        // Disconnect from session so that the updates on updatedReturns are not directly saved in db
        em.detach(updatedReturns);
        updatedReturns
            .numeroRetour(UPDATED_NUMERO_RETOUR)
            .motif(UPDATED_MOTIF)
            .commentaire(UPDATED_COMMENTAIRE);

        restReturnsMockMvc.perform(put("/api/returns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returns)))
            .andExpect(status().isOk());

        // Validate the Returns in the database
        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeUpdate);
        Returns testReturns = returnsList.get(returnsList.size() - 1);
        assertThat(testReturns.getNumeroRetour()).isEqualTo(UPDATED_NUMERO_RETOUR);
        assertThat(testReturns.getMotif()).isEqualTo(UPDATED_MOTIF);
        assertThat(testReturns.getCommentaire()).isEqualTo(UPDATED_COMMENTAIRE);
    }

    @Test
    @Transactional
    public void updateNonExistingReturns() throws Exception {
        int databaseSizeBeforeUpdate = returnsRepository.findAll().size();

        // Create the Returns
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReturnsMockMvc.perform(put("/api/returns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returns)))
            .andExpect(status().isBadRequest());

        // Validate the Returns in the database
        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReturns() throws Exception {
        // Initialize the database
        returnsRepository.saveAndFlush(returns);

        int databaseSizeBeforeDelete = returnsRepository.findAll().size();

        // Delete the returns
        restReturnsMockMvc.perform(delete("/api/returns/{id}", returns.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Returns> returnsList = returnsRepository.findAll();
        assertThat(returnsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
