package com.urservices.myapp.web.rest;

import com.urservices.myapp.RicApp;
import com.urservices.myapp.domain.LigneOperation;
import com.urservices.myapp.repository.LigneOperationRepository;
import com.urservices.myapp.service.LigneOperationService;
import com.urservices.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.urservices.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LigneOperationResource} REST controller.
 */
@SpringBootTest(classes = RicApp.class)
public class LigneOperationResourceIT {

    private static final Long DEFAULT_QUANTITE = 1L;
    private static final Long UPDATED_QUANTITE = 2L;

    private static final Long DEFAULT_P_U = 1L;
    private static final Long UPDATED_P_U = 2L;

    private static final Long DEFAULT_MONTANT_LIGNE_OPERATION = 1L;
    private static final Long UPDATED_MONTANT_LIGNE_OPERATION = 2L;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    @Autowired
    private LigneOperationRepository ligneOperationRepository;

    @Autowired
    private LigneOperationService ligneOperationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLigneOperationMockMvc;

    private LigneOperation ligneOperation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LigneOperationResource ligneOperationResource = new LigneOperationResource(ligneOperationService);
        this.restLigneOperationMockMvc = MockMvcBuilders.standaloneSetup(ligneOperationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LigneOperation createEntity(EntityManager em) {
        LigneOperation ligneOperation = new LigneOperation()
            .quantite(DEFAULT_QUANTITE)
            .pU(DEFAULT_P_U)
            .montantLigneOperation(DEFAULT_MONTANT_LIGNE_OPERATION)
            .deleted(DEFAULT_DELETED);
        return ligneOperation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LigneOperation createUpdatedEntity(EntityManager em) {
        LigneOperation ligneOperation = new LigneOperation()
            .quantite(UPDATED_QUANTITE)
            .pU(UPDATED_P_U)
            .montantLigneOperation(UPDATED_MONTANT_LIGNE_OPERATION)
            .deleted(UPDATED_DELETED);
        return ligneOperation;
    }

    @BeforeEach
    public void initTest() {
        ligneOperation = createEntity(em);
    }

    @Test
    @Transactional
    public void createLigneOperation() throws Exception {
        int databaseSizeBeforeCreate = ligneOperationRepository.findAll().size();

        // Create the LigneOperation
        restLigneOperationMockMvc.perform(post("/api/ligne-operations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneOperation)))
            .andExpect(status().isCreated());

        // Validate the LigneOperation in the database
        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeCreate + 1);
        LigneOperation testLigneOperation = ligneOperationList.get(ligneOperationList.size() - 1);
        assertThat(testLigneOperation.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testLigneOperation.getpU()).isEqualTo(DEFAULT_P_U);
        assertThat(testLigneOperation.getMontantLigneOperation()).isEqualTo(DEFAULT_MONTANT_LIGNE_OPERATION);
        assertThat(testLigneOperation.isDeleted()).isEqualTo(DEFAULT_DELETED);
    }

    @Test
    @Transactional
    public void createLigneOperationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ligneOperationRepository.findAll().size();

        // Create the LigneOperation with an existing ID
        ligneOperation.setId(1L);
        // An entity with an existing ID cannot be created, so this API call must fail
        restLigneOperationMockMvc.perform(post("/api/ligne-operations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneOperation)))
            .andExpect(status().isBadRequest());

        // Validate the LigneOperation in the database
        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkQuantiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = ligneOperationRepository.findAll().size();
        // set the field null
        ligneOperation.setQuantite(null);

        // Create the LigneOperation, which fails.
        restLigneOperationMockMvc.perform(post("/api/ligne-operations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneOperation)))
            .andExpect(status().isBadRequest());

        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkpUIsRequired() throws Exception {
        int databaseSizeBeforeTest = ligneOperationRepository.findAll().size();
        // set the field null
        ligneOperation.setpU(null);

        // Create the LigneOperation, which fails.
        restLigneOperationMockMvc.perform(post("/api/ligne-operations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneOperation)))
            .andExpect(status().isBadRequest());

        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLigneOperations() throws Exception {
        // Initialize the database
        ligneOperationRepository.saveAndFlush(ligneOperation);

        // Get all the ligneOperationList
        restLigneOperationMockMvc.perform(get("/api/ligne-operations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ligneOperation.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE.intValue())))
            .andExpect(jsonPath("$.[*].pU").value(hasItem(DEFAULT_P_U.intValue())))
            .andExpect(jsonPath("$.[*].montantLigneOperation").value(hasItem(DEFAULT_MONTANT_LIGNE_OPERATION.intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getLigneOperation() throws Exception {
        // Initialize the database
        ligneOperationRepository.saveAndFlush(ligneOperation);

        // Get the ligneOperation
        restLigneOperationMockMvc.perform(get("/api/ligne-operations/{id}", ligneOperation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ligneOperation.getId().intValue()))
            .andExpect(jsonPath("$.quantite").value(DEFAULT_QUANTITE.intValue()))
            .andExpect(jsonPath("$.pU").value(DEFAULT_P_U.intValue()))
            .andExpect(jsonPath("$.montantLigneOperation").value(DEFAULT_MONTANT_LIGNE_OPERATION.intValue()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLigneOperation() throws Exception {
        // Get the ligneOperation
        restLigneOperationMockMvc.perform(get("/api/ligne-operations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLigneOperation() throws Exception {
        // Initialize the database
        ligneOperationRepository.saveAndFlush(ligneOperation);

        int databaseSizeBeforeUpdate = ligneOperationRepository.findAll().size();

        // Update the ligneOperation
        LigneOperation updatedLigneOperation = ligneOperationRepository.findById(ligneOperation.getId()).get();
        // Disconnect from session so that the updates on updatedLigneOperation are not directly saved in db
        em.detach(updatedLigneOperation);
        updatedLigneOperation
            .quantite(UPDATED_QUANTITE)
            .pU(UPDATED_P_U)
            .montantLigneOperation(UPDATED_MONTANT_LIGNE_OPERATION)
            .deleted(UPDATED_DELETED);

        restLigneOperationMockMvc.perform(put("/api/ligne-operations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneOperation)))
            .andExpect(status().isOk());

        // Validate the LigneOperation in the database
        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeUpdate);
        LigneOperation testLigneOperation = ligneOperationList.get(ligneOperationList.size() - 1);
        assertThat(testLigneOperation.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testLigneOperation.getpU()).isEqualTo(UPDATED_P_U);
        assertThat(testLigneOperation.getMontantLigneOperation()).isEqualTo(UPDATED_MONTANT_LIGNE_OPERATION);
        assertThat(testLigneOperation.isDeleted()).isEqualTo(UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingLigneOperation() throws Exception {
        int databaseSizeBeforeUpdate = ligneOperationRepository.findAll().size();

        // Create the LigneOperation
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLigneOperationMockMvc.perform(put("/api/ligne-operations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneOperation)))
            .andExpect(status().isBadRequest());

        // Validate the LigneOperation in the database
        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLigneOperation() throws Exception {
        // Initialize the database
        ligneOperationRepository.saveAndFlush(ligneOperation);

        int databaseSizeBeforeDelete = ligneOperationRepository.findAll().size();

        // Delete the ligneOperation
        restLigneOperationMockMvc.perform(delete("/api/ligne-operations/{id}", ligneOperation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LigneOperation> ligneOperationList = ligneOperationRepository.findAll();
        assertThat(ligneOperationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
