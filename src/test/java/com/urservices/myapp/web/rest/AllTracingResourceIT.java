package com.urservices.myapp.web.rest;

import com.urservices.myapp.RicApp;
import com.urservices.myapp.domain.AllTracing;
import com.urservices.myapp.repository.AllTracingRepository;
import com.urservices.myapp.service.AllTracingService;
import com.urservices.myapp.service.dto.AllTracingDTO;
import com.urservices.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.urservices.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AllTracingResource} REST controller.
 */
@SpringBootTest(classes = RicApp.class)
public class AllTracingResourceIT {

    private static final String DEFAULT_LOG_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_LOG_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ENTITY_LOGGED = "AAAAAAAAAA";
    private static final String UPDATED_ENTITY_LOGGED = "BBBBBBBBBB";

    private static final Instant DEFAULT_INSTANT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_INSTANT = Instant.now(Clock.systemDefaultZone());

    private static final String DEFAULT_LOG_GENERATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_LOG_GENERATE_BY = "BBBBBBBBBB";

    @Autowired
    private AllTracingRepository allTracingRepository;

    @Autowired
    private AllTracingService allTracingService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAllTracingMockMvc;

    private AllTracing allTracing;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AllTracingResource allTracingResource = new AllTracingResource(allTracingService);
        this.restAllTracingMockMvc = MockMvcBuilders.standaloneSetup(allTracingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AllTracing createEntity(EntityManager em) {
        AllTracing allTracing = new AllTracing()
            .logDescription(DEFAULT_LOG_DESCRIPTION)
            .entityLogged(DEFAULT_ENTITY_LOGGED)
            .logDate(DEFAULT_INSTANT)
            .logGenerateBy(DEFAULT_LOG_GENERATE_BY);
        return allTracing;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AllTracing createUpdatedEntity(EntityManager em) {
        AllTracing allTracing = new AllTracing()
            .logDescription(UPDATED_LOG_DESCRIPTION)
            .entityLogged(UPDATED_ENTITY_LOGGED)
            .logDate(UPDATED_INSTANT)
            .logGenerateBy(UPDATED_LOG_GENERATE_BY);
        return allTracing;
    }

    @BeforeEach
    public void initTest() {
        allTracing = createEntity(em);
    }

    @Test
    @Transactional
    public void createAllTracing() throws Exception {
        int databaseSizeBeforeCreate = allTracingRepository.findAll().size();

        // Create the AllTracing
        restAllTracingMockMvc.perform(post("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isCreated());

        // Validate the AllTracing in the database
        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeCreate + 1);
        AllTracing testAllTracing = allTracingList.get(allTracingList.size() - 1);
        assertThat(testAllTracing.getLogDescription()).isEqualTo(DEFAULT_LOG_DESCRIPTION);
        assertThat(testAllTracing.getEntityLogged()).isEqualTo(DEFAULT_ENTITY_LOGGED);
        assertThat(testAllTracing.getLogDate()).isEqualTo(DEFAULT_INSTANT);
        assertThat(testAllTracing.getLogGenerateBy()).isEqualTo(DEFAULT_LOG_GENERATE_BY);
    }

    @Test
    @Transactional
    public void createAllTracingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = allTracingRepository.findAll().size();

        // Create the AllTracing with an existing ID
        allTracing.setId(1L);
        // An entity with an existing ID cannot be created, so this API call must fail
        restAllTracingMockMvc.perform(post("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isBadRequest());

        // Validate the AllTracing in the database
        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEntityLoggedIsRequired() throws Exception {
        int databaseSizeBeforeTest = allTracingRepository.findAll().size();
        // set the field null
        allTracing.setEntityLogged(null);

        // Create the AllTracing, which fails.

        restAllTracingMockMvc.perform(post("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isBadRequest());

        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLogDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = allTracingRepository.findAll().size();
        // set the field null
        allTracing.setLogDate(null);

        // Create the AllTracing, which fails.

        restAllTracingMockMvc.perform(post("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isBadRequest());

        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLogGenerateByIsRequired() throws Exception {
        int databaseSizeBeforeTest = allTracingRepository.findAll().size();
        // set the field null
        allTracing.setLogGenerateBy(null);

        // Create the AllTracing, which fails.

        restAllTracingMockMvc.perform(post("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isBadRequest());

        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAllTracings() throws Exception {
        // Initialize the database
        allTracingRepository.saveAndFlush(allTracing);

        // Get all the allTracingList
        restAllTracingMockMvc.perform(get("/api/all-tracings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(allTracing.getId().intValue())))
            .andExpect(jsonPath("$.[*].logDescription").value(hasItem(DEFAULT_LOG_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].entityLogged").value(hasItem(DEFAULT_ENTITY_LOGGED)))
            .andExpect(jsonPath("$.[*].logDate").value(hasItem(DEFAULT_INSTANT.toString())))
            .andExpect(jsonPath("$.[*].logGenerateBy").value(hasItem(DEFAULT_LOG_GENERATE_BY)));
    }

    @Test
    @Transactional
    public void getAllTracing() throws Exception {
        // Initialize the database
        allTracingRepository.saveAndFlush(allTracing);

        // Get the allTracing
        restAllTracingMockMvc.perform(get("/api/all-tracings/{id}", allTracing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(allTracing.getId().intValue()))
            .andExpect(jsonPath("$.logDescription").value(DEFAULT_LOG_DESCRIPTION))
            .andExpect(jsonPath("$.entityLogged").value(DEFAULT_ENTITY_LOGGED))
            .andExpect(jsonPath("$.logDate").value(DEFAULT_INSTANT.toString()))
            .andExpect(jsonPath("$.logGenerateBy").value(DEFAULT_LOG_GENERATE_BY));
    }

    @Test
    @Transactional
    public void getNonExistingAllTracing() throws Exception {
        // Get the allTracing
        restAllTracingMockMvc.perform(get("/api/all-tracings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAllTracing() throws Exception {
        // Initialize the database
        allTracingRepository.saveAndFlush(allTracing);

        int databaseSizeBeforeUpdate = allTracingRepository.findAll().size();

        // Update the allTracing
        AllTracing updatedAllTracing = allTracingRepository.findById(allTracing.getId()).get();
        // Disconnect from session so that the updates on updatedAllTracing are not directly saved in db
        em.detach(updatedAllTracing);
        updatedAllTracing
            .logDescription(UPDATED_LOG_DESCRIPTION)
            .entityLogged(UPDATED_ENTITY_LOGGED)
            .logDate(UPDATED_INSTANT)
            .logGenerateBy(UPDATED_LOG_GENERATE_BY);
        restAllTracingMockMvc.perform(put("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isOk());

        // Validate the AllTracing in the database
        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeUpdate);
        AllTracing testAllTracing = allTracingList.get(allTracingList.size() - 1);
        assertThat(testAllTracing.getLogDescription()).isEqualTo(UPDATED_LOG_DESCRIPTION);
        assertThat(testAllTracing.getEntityLogged()).isEqualTo(UPDATED_ENTITY_LOGGED);
        assertThat(testAllTracing.getLogDate()).isEqualTo(UPDATED_INSTANT);
        assertThat(testAllTracing.getLogGenerateBy()).isEqualTo(UPDATED_LOG_GENERATE_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingAllTracing() throws Exception {
        int databaseSizeBeforeUpdate = allTracingRepository.findAll().size();

        // Create the AllTracing
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAllTracingMockMvc.perform(put("/api/all-tracings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allTracing)))
            .andExpect(status().isBadRequest());

        // Validate the AllTracing in the database
        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAllTracing() throws Exception {
        // Initialize the database
        allTracingRepository.saveAndFlush(allTracing);

        int databaseSizeBeforeDelete = allTracingRepository.findAll().size();

        // Delete the allTracing
        restAllTracingMockMvc.perform(delete("/api/all-tracings/{id}", allTracing.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AllTracing> allTracingList = allTracingRepository.findAll();
        assertThat(allTracingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
