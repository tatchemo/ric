package com.urservices.myapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.urservices.myapp.web.rest.TestUtil;

public class AllTracingDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AllTracingDTO.class);
        AllTracingDTO allTracingDTO1 = new AllTracingDTO();
        allTracingDTO1.setId(1L);
        AllTracingDTO allTracingDTO2 = new AllTracingDTO();
        assertThat(allTracingDTO1).isNotEqualTo(allTracingDTO2);
        allTracingDTO2.setId(allTracingDTO1.getId());
        assertThat(allTracingDTO1).isEqualTo(allTracingDTO2);
        allTracingDTO2.setId(2L);
        assertThat(allTracingDTO1).isNotEqualTo(allTracingDTO2);
        allTracingDTO1.setId(null);
        assertThat(allTracingDTO1).isNotEqualTo(allTracingDTO2);
    }
}
