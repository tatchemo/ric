package com.urservices.myapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.urservices.myapp.web.rest.TestUtil;

public class ReturnsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReturnsDTO.class);
        ReturnsDTO returnsDTO1 = new ReturnsDTO();
        returnsDTO1.setId(1L);
        ReturnsDTO returnsDTO2 = new ReturnsDTO();
        assertThat(returnsDTO1).isNotEqualTo(returnsDTO2);
        returnsDTO2.setId(returnsDTO1.getId());
        assertThat(returnsDTO1).isEqualTo(returnsDTO2);
        returnsDTO2.setId(2L);
        assertThat(returnsDTO1).isNotEqualTo(returnsDTO2);
        returnsDTO1.setId(null);
        assertThat(returnsDTO1).isNotEqualTo(returnsDTO2);
    }
}
