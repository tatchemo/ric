package com.urservices.myapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.urservices.myapp.web.rest.TestUtil;

public class LigneOperationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LigneOperationDTO.class);
        LigneOperationDTO ligneOperationDTO1 = new LigneOperationDTO();
        ligneOperationDTO1.setId(1L);
        LigneOperationDTO ligneOperationDTO2 = new LigneOperationDTO();
        assertThat(ligneOperationDTO1).isNotEqualTo(ligneOperationDTO2);
        ligneOperationDTO2.setId(ligneOperationDTO1.getId());
        assertThat(ligneOperationDTO1).isEqualTo(ligneOperationDTO2);
        ligneOperationDTO2.setId(2L);
        assertThat(ligneOperationDTO1).isNotEqualTo(ligneOperationDTO2);
        ligneOperationDTO1.setId(null);
        assertThat(ligneOperationDTO1).isNotEqualTo(ligneOperationDTO2);
    }
}
