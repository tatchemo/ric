package com.urservices.myapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.urservices.myapp.web.rest.TestUtil;

public class TypeOperationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeOperationDTO.class);
        TypeOperationDTO typeOperationDTO1 = new TypeOperationDTO();
        typeOperationDTO1.setId(1L);
        TypeOperationDTO typeOperationDTO2 = new TypeOperationDTO();
        assertThat(typeOperationDTO1).isNotEqualTo(typeOperationDTO2);
        typeOperationDTO2.setId(typeOperationDTO1.getId());
        assertThat(typeOperationDTO1).isEqualTo(typeOperationDTO2);
        typeOperationDTO2.setId(2L);
        assertThat(typeOperationDTO1).isNotEqualTo(typeOperationDTO2);
        typeOperationDTO1.setId(null);
        assertThat(typeOperationDTO1).isNotEqualTo(typeOperationDTO2);
    }
}
